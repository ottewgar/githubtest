<?php

require_once 'vendor/deployer/deployer/recipe/common.php';
require 'vendor/deployphp/recipes/recipes/rsync.php';
require 'vendor/deployer/deployer/recipe/composer.php';
require 'vendor/deployer/deployer/recipe/symfony.php';

/**
 * Environment Setup
 */
$username = getenv('USERNAME');
$host = getenv('HOST');
$stage = getenv('STAGE');
$deploymentPath = getenv('DEPLOYMENT_PATH');
$environment = getenv('ENVIRONMENT');
$privateKey = getenv('PRIVATE_KEY');
$publicKey = getenv('PUBLIC_KEY');

server('web', $host)
    ->user($username)
    ->stage($stage)
    ->env('deploy_path', $deploymentPath)
    ->env('env', $environment)
    ->identityFile($publicKey, $privateKey);

//make web/upload a shared folder as well as logs
set('shared_dirs', ['app/logs', 'web/uploads', 'web/media/cache']);

set('writable_use_sudo', false);

$includeDevDependencies = in_array($environment, array('dev', 'test'), true);
if($includeDevDependencies) {
    env('composer_options', 'install --verbose --prefer-dist --no-progress --no-interaction');
} else {
    env('composer_options', 'install --no-dev --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction');
}

env('rsync_src', __DIR__);
env('rsync_dest', '{{release_path}}');

// Environment vars
env('env_vars', 'SYMFONY_ENV=' . $environment);
env('env', $environment);

// remove parameters.yml from shared files
set('shared_files', []);

// run doctrine migrations
after('deploy:vendors', 'database:migrate');

/**
 * Main task
 */
task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'rsync',
    'deploy:vendors',
    'deploy:create_cache_dir',
    'deploy:shared',
    'deploy:assets',
    'deploy:cache:warmup',
    'deploy:writable',
    'deploy:symlink',
    'cleanup',
])->desc('Deploy project');

after('deploy', 'success');
