<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;

use FOS\RestBundle\Controller\Annotations\View;

use AppBundle\Entity\User;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;

use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Noxlogic\RateLimitBundle\Annotation\RateLimit;

/**
 * Class ApiController
 * @package AppBundle\Controller\Api
 */

/**
 * @Route("/api/v1/user")
 * @RateLimit(methods={"GET","POST"}, limit=100, period=3600)
 */

class UserApiController extends FOSRestController {

    /**
     * After a user has successfully logged in via OAuth and received an access_token, their user details can be retrieved
     * using this call.
     *
     *
     * @Get("")
     * @View(
     * serializerGroups={"basic_details"}
     * )
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Authenticated users",
     *  description="Get user details based on OAuth access_token",
     *  output={"class"="AppBundle\Entity\User", "groups"={"basic_details"}},
     *  statusCodes={
     *      200="Returned when user is logged in and has active account",
     *      401={
     *          "Returned when the user is not authenticated",
     *          "Returned when the user account is not active",
     *          "Returned when the user account is locked",
     *          "Returned when the user hasn't set a password"
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *    }
     * )
     * )
     */

    public function userDetailsAction()
    {
        $user = $this->getUser();

        return $user;
    }

    /**
     * After a user has successfully logged in via OAuth and received an access_token, their user details can be retrieved
     * using this call.
     *
     *
     * @Post("")
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Authenticated users",
     *  description="Update user details",
     *  input={
     *      "class"="AppBundle\Form\Type\Api\UserProfileFormType",
     *      "name" = "user",
     *      "groups"={"api_user_profile"}
     *  },
     *  statusCodes={
     *      200="Returned when user details updated successfully",
     *      401={
     *          "Returned when the user is not authenticated",
     *          "Returned when the user account is not active",
     *          "Returned when the user account is locked",
     *          "Returned when the user hasn't set a password"
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *    }
     * )
     * )
     */

    public function updateUserProfileAction(Request $request)
    {
        return $this->get('nandos.api.form_handler.user_profile')
            ->handle($this->getUser(), $request);
    }

    /**
     * After a user has successfully logged in via OAuth and received an access_token, their password can be changed
     * using this call.
     *
     *
     * @Post("/change-password")
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Authenticated users",
     *  description="Change the currently logged in users password",
     *  input={
     *      "class"="AppBundle\Form\Type\Api\ChangePasswordFormType",
     *      "name" = ""
     *  },
     *  statusCodes={
     *      200="Returned when user password successfully updated",
     *      400={
     *          "Returned when the password information provided fails data validation",
     *      },
     *      401={
     *          "Returned when the user is not authenticated",
     *          "Returned when the user account is not active",
     *          "Returned when the user account is locked",
     *          "Returned when the user hasn't set a password"
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *    }
     * )
     * )
     */

    public function changePasswordAction(Request $request)
    {
        return $this->get('nandos.api.form_handler.change_password')
            ->handle($this->getUser(), $request);
    }

    /**
     * After a user has successfully logged in via OAuth and received an access_token, they can request to merge an old
     * takeaway account into their own.
     *
     *
     * @Post("/merge-old-takeaway-account")
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Authenticated users",
     *  description="Request to merge an old takeaway account",
     *  input={
     *      "class"="AppBundle\Form\Type\Api\EmailFormType",
     *      "name" = ""
     *  },
     *  statusCodes={
     *      200="Returned when merge account request is valid",
     *      400={
     *          "Returned when the email address provided fails data validation",
     *      },
     *      401={
     *          "Returned when the user is not authenticated",
     *          "Returned when the user account is not active",
     *          "Returned when the user account is locked",
     *          "Returned when the user hasn't set a password"
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *    }
     * )
     * )
     */

    public function mergeOldTakeawayAccountAction(Request $request)
    {
        return $this->get('nandos.api.form_handler.request_merge_old_takeaway_account')
            ->handle($this->getUser(), $request);
    }

    /**
     * After a user has successfully logged in via OAuth and received an access_token, they can request to merge an old
     * takeaway account into their own.
     *
     *
     * @Post("/merge-old-takeaway-account/confirm")
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Authenticated users",
     *  description="Merge an old takeaway account",
     *  input={
     *      "class"="AppBundle\Form\Type\Api\SecondaryEmailVerificationTokenFormType",
     *      "name" = ""
     *  },
     *  statusCodes={
     *      200="Returned when the merge account was successful",
     *      400={
     *          "Returned when the verification token provided fails data validation",
     *      },
     *      401={
     *          "Returned when the user is not authenticated",
     *          "Returned when the user account is not active",
     *          "Returned when the user account is locked",
     *          "Returned when the user hasn't set a password"
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *    }
     * )
     * )
     */

    public function confirmMergeOldTakeawayAccountAction(Request $request)
    {

        return $this->get('nandos.api.merge_old_takeaway_account')
            ->handle($this->getUser(), $request);
    }

    /**
     * After a user has successfully logged in via OAuth and received an access_token, they can get information about a
     * specific secondary email operation verification token
     *
     * @Get("/secondary-email-operation/{verificationToken}", requirements={"verificationToken" = ".+"})
     * @View(
     * serializerGroups={"basic_details"}
     * )
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  section="Authenticated users",
     *  description="Get information about a secondary email operation verification token",
     *  input={
     *      "class"="AppBundle\Form\Type\Api\SecondaryEmailVerificationTokenFormType",
     *      "name" = ""
     *  },
     *  output={"class"="AppBundle\Entity\SecondaryEmailVerification", "groups"={"basic_details"}},
     *  statusCodes={
     *      200="Returned when the secondary email operation verification token exists",
     *      400={
     *          "Returned when the verification token provided fails data validation",
     *      },
     *      401={
     *          "Returned when the user is not authenticated",
     *          "Returned when the user account is not active",
     *          "Returned when the user account is locked",
     *          "Returned when the user hasn't set a password"
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *    }
     * )
     * )
     */

    public function secondaryEmailOperationDetailsAction(Request $request, $verificationToken)
    {
        return $this->get('nandos.api.handler.secondary_email_operation_details')
            ->getSecondaryEmailOperationDetailsResponse($this->getUser(), $verificationToken);
    }

}