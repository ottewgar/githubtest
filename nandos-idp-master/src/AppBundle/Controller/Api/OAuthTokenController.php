<?php

namespace AppBundle\Controller\Api;

use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Log\Traits\LoggerTrait;
use FOS\OAuthServerBundle\Controller\TokenController;

use FOS\OAuthServerBundle\Model\TokenInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use OAuth2\OAuth2ServerException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Noxlogic\RateLimitBundle\Annotation\RateLimit;

/**
 * Class OAuthTokenController
 * @package AppBundle\Controller\Api
 *
 * {@inheritdoc}
 * @Route("/oauth/v2", service="nandos.api.oauth_server.controller.token")
 */

class OAuthTokenController extends TokenController
{
    use LoggerTrait;

    protected $userManagerContainer;

    public function setUserManagerContainer(CardUserManagerContainer $userManagerContainer)
    {
        $this->userManagerContainer = $userManagerContainer;
    }

    /**
     * Get an OAuth access token.
     *
     * the Nandos IDP uses the "password" grant type for user login.
     * @param Request $request
     * @return TokenInterface
     *
     * @Route("/token", name="fos_oauth_server_token")
     * @Method({"GET","POST"})
     *
     * @ApiDoc(
     *  section="OAuth",
     *  output="FOS\OAuthServerBundle\Model\TokenInterface",
     *  requirements={
     *      { "name"="client_id", "dataType"="string", "description"="The client application's identifier"},
     *      { "name"="client_secret", "dataType"="string", "description"="The client application's secret"},
     *      { "name"="grant_type", "dataType"="string", "requirement"="refresh_token|authorization_code|password|client_credentials", "description"="Grant type"},
     *  },
     *  parameters={
     *      { "name"="username", "dataType"="string", "required"=false, "description"="User name (for `password` grant type)"},
     *      { "name"="password", "dataType"="string", "required"=false, "description"="User password (for `password` grant type)"},
     *      { "name"="refresh_token", "dataType"="string", "required"=false, "description"="The authorization code received by the authorization server(for `refresh_token` grant type`"},
     *      { "name"="code", "dataType"="string", "required"=false, "description"="The authorization code received by the authorization server (For `authorization_code` grant type)"},
     *      { "name"="scope", "dataType"="string", "required"=false, "description"="If the `redirect_uri` parameter was included in the authorization request, and their values MUST be identical"},
     *      { "name"="redirect_uri", "dataType"="string", "required"=false, "description"="If the `redirect_uri` parameter was included in the authorization request, and their values MUST be identical"},
     *  }
     * )
     */
    public function tokenAction(Request $request)
    {
        try {
            return $this->server->grantAccessToken($request);
        } catch (OAuth2ServerException $e) {
            return $e->getHttpResponse();
        }
    }

}