<?php

namespace AppBundle\Controller\Api;

use AppBundle\Form\Type\Api\ConfirmEmailFormType;
use FOS\RestBundle\Controller\FOSRestController;

use FOS\RestBundle\Controller\Annotations\View;

use AppBundle\Form\Type\Api\UserRegisterFormType;
use AppBundle\Entity\User;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Noxlogic\RateLimitBundle\Annotation\RateLimit;

/**
 * Class ApiController
 * @package AppBundle\Controller\Api
 */

/**
 * @Route("/api/v1")
 * @RateLimit(methods={"GET","POST"}, limit=1000, period=600)
 */

class RegisterApiController extends FOSRestController {

    /**
     * Attempt to register a new user. Errors will occur if the user exists already in the IDP or exists in
     * NCR Radiant.
     *
     * Example JSON request:
     *
     * "user": {
     *   "firstname": "Joe",
     *   "lastname": "Bloggs",
     *   "email": "joebloggs@example.com",
     *   "password": "secure unencrypted password"
     *  }
     *
     *
     * @Post("/register")
     * @QueryParam(
     *     name="api_test_key",
     *     requirements="\w+",
     *     description="Used by API testing. Not enabled on production"
     * )
     * @ApiDoc(
     *  section="Public API",
     *  resource=true,
     *  description="Register a new Nandos user",
     *  input={
     *      "class"="AppBundle\Form\Type\Api\UserRegisterFormType",
     *      "name"="user",
     *      "groups"={"api_register"}
     *  },
     *  statusCodes={
     *      201="Returned when user successfully registered. Response will contain 'required_action' = 'check_confirmation_email'",
     *      400={
     *          "Returned when the user details are invalid (e.g. weak password / missing required fields etc.)",
     *          "Returned when the user already exists in the IDP. Depending on the status of the user, one of the following 'required_action' values will be present: TBC",
     *          "Returned when the user already exists in NCR Radiant."
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *    }
     * )
     */

    public function registerAction(Request $request)
    {
        return $this->get('nandos.api.form_handler.user_register')->handle($request);
    }


    /**
     * Confirm a users account (email address) using the confirmation token emailed to them.
     *
     * The result of this API call will not reveal which email address was confirmed.
     *
     * No POST body is required, as the confirmation token is provided in the url
     *
     * @Post("/token/{confirmationToken}/confirm", requirements={"confirmationToken" = "[a-zA-Z0-9_\-]+"})
     * @ApiDoc(
     *  section="Public API",
     *  resource=true,
     *  description="Active account via confirmation token",
     *  statusCodes={
     *      200="Returned when user successfully activated using confirmation token",
     *      404={
     *          "Returned if no user could be found with the token. They may have confirmed already",
     *          "Returned if the confirmation token does not consist of a-z A-Z 0-9 _ and -",
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *  }
     * )
     */

    public function confirmUserAccount(Request $request, $confirmationToken)
    {
        return $this->get('nandos.api.handler.confirm_user_account_by_token')->handle($confirmationToken);
    }


    /**
     * Change a users password (it may be expired or they forgot it) using the confirmation token emailed to them.
     *
     * The result of this API call will not reveal which account password was updated.
     *
     *
     * @Post("/token/{confirmationToken}/reset-password", requirements={"confirmationToken" = "[a-zA-Z0-9_\-]+"})
     * @ApiDoc(
     *  section="Public API",
     *  resource=true,
     *  description="Change password via confirmation token",
     *  input={
     *      "class"="AppBundle\Form\Type\Api\ResetPasswordFormType",
     *      "name" = "",
     *      "groups"={"api_change_password"}
     *  },
     *  statusCodes={
     *      200="Returned when user successfully updated password using confirmation token",
     *      400={
     *          "Returned when the new password is invalid",
     *      },
     *      404={
     *          "Returned if no user could be found with the token. They may have confirmed already",
     *          "Returned if the confirmation token does not consist of a-z A-Z 0-9 _ and -",
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *  }
     * )
     */

    public function resetPassword(Request $request, $confirmationToken)
    {
        return $this->get('nandos.api.form_handler.reset_password_by_token')->handle($confirmationToken, $request);
    }

     /**
     * Resend a users confirmation email (either confirm account or reset password email, based on user account status)
     *
     * @Post("/resend-confirmation-email")
     * @ApiDoc(
     *  section="Public API",
     *  resource=true,
     *  description="Resend appropriate confirmation email containing token",
     *  input={
     *     "class"="AppBundle\Form\Type\Api\ResendConfirmationEmailFormType",
     *      "name" = "",
     *      "groups"={"api_resend_confirmation_email"}
     *     },
     *  statusCodes={
     *      200="Returned when confirmation email successfully sent.",
     *      400={
     *          "Returned when the email address provided fails data validation",
     *      },
     *      404={
     *          "Returned if no user could be found with the provided email address"
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *  }
     * )
     */

    public function resendConfirmationEmail(Request $request)
    {
        return $this->get('nandos.api.form_handler.resend_confirmation_email')->handle($request);
    }

    /**
     * Resend a users password reset email
     *
     * @Post("/send-password-reset-email")
     * @ApiDoc(
     *  section="Public API",
     *  resource=true,
     *  description="Send password reset email containing token",
     *  input={
     *     "class"="AppBundle\Form\Type\Api\SendPasswordResetEmailFormType",
     *      "name" = "",
     *      "groups"={"api_send_password_reset_email"}
     *     },
     *  statusCodes={
     *      200="Returned when confirmation email successfully sent.",
     *      400={
     *          "Returned when the email address provided fails data validation",
     *      },
     *      404={
     *          "Returned if no user could be found with the provided email address"
     *      },
     *      500="Unexpected server error e.g. database error. See 'message' in response for exception description."
     *  }
     * )
     */

    public function sendPasswordResetEmail(Request $request)
    {
        return $this->get('nandos.api.form_handler.send_password_reset_email')->handle($request);
    }

}