<?php

namespace AppBundle\Controller;

use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserAdminController extends CRUDController
{

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->injectServices();
    }

    /**
     *
     */
    protected function injectServices()
    {
        $this->admin->setEncryptor($this->container->get('ambta_doctrine_encrypt.encryptor'));
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function testPaytronixAction($id)
    {
        $user = $this->getUserById($id);

        $paytronixUserManager = $this->container->get('nandos.paytronix.user_manager');

        $paytronixUser = $paytronixUserManager->getPaytronixProfile($user->getUsername());

        if ($paytronixUser['result'] !== "success") {

            $return = [
                'result' => "failed",
            ];

        } else {
            $fields = $paytronixUser['fields'];

            $return = [
                'result' => $paytronixUser['result'],
                'user' => [
                    'username' => $fields['username'],
                    'password' => $fields['custom2'],
                    'firstName' => $fields['firstName'],
                    'lastName' => $fields['lastName'],
                    'cardNumber' => $paytronixUser['primaryCardNumbers'],
                ]
            ];

        }


        return new JsonResponse($return);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function testNcrAction($id)
    {
        $user = $this->getUserById($id);

        $ncrUserManager = $this->container->get('nandos.ncr_radiant.user_manager');

        $profile = $ncrUserManager->getProfileByEmail($user->getUsername());

        if ($profile instanceof MemberProfile) {
            $return = [
                'result' => 'success',
                'user' => [
                    'username' => $profile->emailAddress,
                    'password' => $profile->password,
                    'firstName' => $profile->firstName,
                    'lastName' => $profile->lastName,
                    'cardNumber' => $profile->cardNumber,
                ]
            ];
        } else {
            $return = [
                'result' => 'failed',
            ];
        }

        return new JsonResponse($return);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserById($id)
    {
        $userService = $this->container->get('user_service');

        $userManager = $userService->getUserManager();

        return $userManager->findUserBy(['id' => $id]);
    }
}
