<?php

namespace AppBundle\Controller;


use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Form\Type\UserRegisterFormType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $ncrConfig = $this->get('nandos.ncr_radiant.config');
        $paytronixConfig = $this->get('nandos.paytronix.config');
        $generalApiConfig = $this->get('nandos.general_api.config');

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir'              => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'ncr_radiant_endpoint'  => $ncrConfig->getEndpoint(),
            'paytronix_endpoint'    => $paytronixConfig->getEndpoint(),
            'api_test_mode'         => ($generalApiConfig->getConfig('api_test_mode')) ? "On" : "Off",
            'database_name'         => $this->getParameter('database_name')
        ));
    }
}
