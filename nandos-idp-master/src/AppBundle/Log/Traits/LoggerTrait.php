<?php

namespace AppBundle\Log\Traits;

use Psr\Log\LoggerTrait as BaseLoggerTrait;
use Psr\Log\LoggerAwareTrait;

trait LoggerTrait
{
    use BaseLoggerTrait, LoggerAwareTrait;

    public function log($level, $message, array $context = array()) {

        if ($this->logger) {
            $this->logger->log($level, $message, $context);
        }

    }

}
