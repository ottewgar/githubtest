<?php

namespace AppBundle\Listener;

use AppBundle\Api\ApiService;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CheckRouteAllowedListener
{
    /**
     * @var ApiService
     */
    private $apiService;

    /**
     * CheckRouteAllowedListener constructor.
     * @param ApiService $apiService
     */
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param FilterControllerEvent $event
     * @return bool
     */
    public function onKernelController(FilterControllerEvent $event)
    {

        $controllerRoute = $event->getRequest()->get('_route');

        if (!$this->apiService->checkIfApiRoute($controllerRoute)) {
            return false;
        }

        $client = $this->apiService->getClient();

        if (!$client) {
            return false;
        } else {
            $allowedRoutes = $client->getAllowedRoutes();

            if ($allowedRoutes !== "" && $allowedRoutes !== null) {
                $routes = explode("\r\n", strip_tags($allowedRoutes));

                if (!in_array($controllerRoute, $routes)) {
                    throw new AccessDeniedException('Access Denied');
                }
            }

        }

    }

}
