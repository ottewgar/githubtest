<?php

namespace AppBundle\Listener;

use Noxlogic\RateLimitBundle\Events\GenerateKeyEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RateLimitGenerateKeyListener
{
    /**
     * @param GenerateKeyEvent $event
     */
    public function onGenerateKey(GenerateKeyEvent $event)
    {
        $request = Request::createFromGlobals();

        $username = "";
        if ($request->request->get('username')) {
            $username = $request->request->get('username');
        } elseif ($request->query->get('username')) {
            $username = $request->query->get('username');
        }

        $event->addToKey($username);
    }
}
