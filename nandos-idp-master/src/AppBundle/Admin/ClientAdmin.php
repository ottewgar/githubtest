<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Client;
use OAuth2\OAuth2;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ClientAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('randomId')
            ->add('redirectUris')
            ->add('secret')
            ->add('allowedGrantTypes')
            ->add('id')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('randomId')
            ->add('redirectUris')
            ->add('secret')
            ->add('allowedGrantTypes')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $grantTypes = [
            OAuth2::GRANT_TYPE_AUTH_CODE          => OAuth2::GRANT_TYPE_AUTH_CODE,
            OAuth2::GRANT_TYPE_CLIENT_CREDENTIALS => OAuth2::GRANT_TYPE_CLIENT_CREDENTIALS,
            OAuth2::GRANT_TYPE_USER_CREDENTIALS   => OAuth2::GRANT_TYPE_USER_CREDENTIALS,
            OAuth2::GRANT_TYPE_REFRESH_TOKEN      => OAuth2::GRANT_TYPE_REFRESH_TOKEN
        ];

        $formMapper
            ->add('name')
            ->add('randomId')
            ->add('redirectUris')
            ->add('secret')
            ->add('allowedGrantTypes', 'choice', ['multiple' => true, 'choices' => $grantTypes ])
            ->add('allowedRoutes', 'textarea', [
                'required' => false,
            ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('randomId')
            ->add('redirectUris')
            ->add('secret')
            ->add('allowedGrantTypes')
        ;
    }
}
