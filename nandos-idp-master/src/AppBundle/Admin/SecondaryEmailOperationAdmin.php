<?php

namespace AppBundle\Admin;

use AppBundle\Entity\SecondaryEmailOperation;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class SecondaryEmailOperationAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('primaryEmailAddress', null, ['advanced_filter' => false])
            ->add('secondaryEmailAddress', null, ['advanced_filter' => false])
            ->add('requestedOperation', 'doctrine_orm_string', [], 'choice', ['choices' => SecondaryEmailOperation::getRequestedOperations()])
            ->add('createdAt')
            ->add('operationAttemptedAt')
            ->add('operationSuccess')
            ->add('operationLog')
            ->add('verificationToken')
        ;

    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('verificationToken')
            ->add('expiresAt')
            ->add('primaryEmailAddress')
            ->add('secondaryEmailAddress')
            ->add('requestedOperation')
            ->add('createdAt')
            ->add('operationAttemptedAt')
            ->add('operationSuccess')
            ->add('operationLog')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('verificationToken')
            ->add('expiresAt')
            ->add('primaryEmailAddress')
            ->add('secondaryEmailAddress')
            ->add('requestedOperation', 'choice', ['choices' => SecondaryEmailOperation::getRequestedOperations()])
            ->add('createdAt')
            ->add('operationAttemptedAt')
            ->add('operationSuccess')
            ->add('operationLog')
        ;
    }

    /**
     * Don't allow admins to add a secondary email operation
     *
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->remove('create');
    }


    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('verificationToken')
            ->add('expiresAt')
            ->add('primaryEmailAddress')
            ->add('secondaryEmailAddress')
            ->add('requestedOperation')
            ->add('createdAt')
            ->add('operationAttemptedAt')
            ->add('operationSuccess')
            ->add('operationLog')
        ;
    }
}
