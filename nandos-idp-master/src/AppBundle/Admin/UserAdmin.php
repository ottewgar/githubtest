<?php

namespace AppBundle\Admin;

use AppBundle\Entity\User;
use AppBundle\Form\Type\ApiTestType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends BaseUserAdmin
{
    protected $encryptor;

    public function setEncryptor($encryptor)
    {
        $this->encryptor = $encryptor;
    }

    public function encryptValue($value)
    {
        return $this->encryptor->encrypt($value);
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('id')
            ->add('email', 'custom_filter', [
            ])
            ->add('registerSource', 'custom_filter', [], 'choice', ['choices' => User::getRegisterSources()])
            ->add('locked')
            ->add('enabled')
            ->add('credentialsExpired')
            ->add('groups', 'custom_filter')
        ;

    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->formTheme[] = 'admin/type/testApiType.html.twig';

        $formMapper
            ->with('Test Card Accounts', ['class' => 'col-md-12'])
                ->add('apitest_formtype', 'apitest_formtype', [
                    'mapped' => false,
                    'label' => 'Card System',
                    'help' => 'Test for account in card system',
                ])
            ->end()
            ->with('Account', ['class' => 'col-md-6'])
    //            ->add('username')
                ->add('firstname', null, array('required' => false, 'label' => 'First name'))
                ->add('lastname', null, array('required' => false, 'label' => 'Last name'))
                ->add('email', null, array('label' => 'Email address'))
                ->add('plainPassword', 'text', array(
                    'required' => (!$this->getSubject() || is_null($this->getSubject()->getId()))
                ))
            ->add('registerSource', 'choice', array('label' => 'Register source', 'choices' => User::getRegisterSources()))
            ->end()
            ->with('Extended profile',  ['class' => 'col-md-6 clear-left'])
                ->add('dateOfBirth', 'birthday', array('required' => false))
    //            ->add('website', 'url', array('required' => false))
    //            ->add('biography', 'text', array('required' => false))
                ->add('gender', 'sonata_user_gender', array(
                    'required' => true,
                    'translation_domain' => $this->getTranslationDomain()
                ))
                ->add('locale', 'locale', array('required' => false))
                ->add('timezone', 'timezone', array('required' => false))
                ->add('phone', null, array('required' => false))
            ->end()
            ->with("Nando's accounts", ['class' => 'col-md-6'])
//            ->add('nandosCardNumber', null,
//                ['label' => 'Nando\'s Card Number',
//                'attr' => ['readonly' => 'readonly'],
//                    'help' => 'Fetched from NCR on each user profile API request'
//                ]
//            )
            ->add('takeawayCustomerId', null, array('label' => 'Takeaway Customer ID'))
            ->end()
            ->with('Groups', ['class' => 'col-md-6 clear-left'])
            ->add('groups', 'sonata_type_model', array(
                'required' => false,
                'expanded' => true,
                'multiple' => true
            ))
            ->end()
//            ->with('Social', ['class' => 'col-md-6'])
//            ->add('facebookUid', null, array('required' => false))
//            ->add('facebookName', null, array('required' => false))
//            ->add('twitterUid', null, array('required' => false))
//            ->add('twitterName', null, array('required' => false))
//            ->add('gplusUid', null, array('required' => false))
//            ->add('gplusName', null, array('required' => false))
//            ->end()
        ;

        if ($this->getSubject() && !$this->getSubject()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->with('User management', ['class' => 'col-md-6'])
//                ->add('realRoles', 'sonata_security_roles', array(
//                    'label'    => 'form.label_roles',
//                    'expanded' => true,
//                    'multiple' => true,
//                    'required' => false
//                ))
                ->add('locked', null, array('required' => false, 'help' => 'Use only this field to disable user login (do not use "enabled" as it is solely used to identify unconfirmed email address)'))
//                ->add('expired', null, array('required' => false))
                ->add('enabled', null, array('required' => false, 'help' => 'Not enabled means the email address needs to be verified'))
                ->add('credentialsExpired', null, array('required' => false, 'help' => 'Credentials expired means password must be reset. New users begin in this state, until they confirm account.'))
                ->end()
            ;
        }

        $formMapper
            ->with('Security', ['class' => 'col-md-6'])
            ->add('confirmation_token', 'text', array('required' => false, 'help' => 'Email confirmation/reset password token', 'label' => 'Confirmation token'))
            //->add('token', null, array('required' => false)) // what is this token?
            //->add('twoStepVerificationCode', null, array('required' => false))
            ->add('confirmationExpiry', 'datetime', ['label' => 'Confirmation Expiry', 'required' => false])
            ->end()
        ;

        $formMapper
            ->with('Additional Data', ['class' => 'clear-left'])
            ->add('metadatas', 'sonata_type_collection',
                array(
                'required' => false,
                'label' => 'User Metadata',
                'btn_add' => false,
                'type_options' => array(
                    // Prevents the "Delete" option from being displayed
                    'delete' => false,
                )),
                array('edit' => 'inline',
                      'inline' => 'table',
                      'allow_delete' => false)
                )
            ->end();

    }

    /**
     * For security, disable user export
     *
     * @return array
     */

    public function getExportFormats()
    {
       return [];
    }

    /**
     * Disable batch delete of users
     *
     * @return array
     */

    public function getBatchActions()
    {
        return [];
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('testpaytronix', $this->getRouterIdParameter().'/testpaytronix');
        $collection->add('testncr', $this->getRouterIdParameter().'/testncr');
    }
}