<?php

namespace AppBundle\Admin;

use AppBundle\Entity\UserMetadata;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserMetadataAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('userId')
            ->add('metadataKey')
            ->add('metadataFormat')
            ->add('metadataValue')
            ->add('metadataEncryptedValue')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('metadataKey')
            ->add('user')
            ->add('metadataFormat')
            ->add('metadataValue')
            ->add('metadataEncryptedValue')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('userId', null, ['help' => 'The user id can be determined by looking at the edit url of a user'])
            ->add('metadataKey')
            ->add('metadataFormat')
            ->add('metadataValue')
            ->add('metadataEncryptedValue')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('user')
            ->add('metadataKey')
            ->add('metadataFormat')
            ->add('metadataValue')
            ->add('metadataEncryptedValue')
        ;
    }

    /**
     * As we only ask for a user id, use it to load the actual user
     * @param UserMetadata $object
     */

    public function prePersist($object)
    {

        /** @var UserMetadata $object */
        if ($userId = $object->getUserId()) {
            $object->setUser($this->getModelManager()->getEntityManager($object)->getReference('AppBundle\Entity\User', $userId));
        }
    }

    /**
     * Set the user id back into the form for update
     * @param mixed $id
     * @return mixed|object
     */

    public function getObject($id)
    {
        $object = parent::getObject($id);
        if ($object && $user = $object->getUser()) {
            $object->setUserId($user->getId());
        }
        return $object;

    }

}
