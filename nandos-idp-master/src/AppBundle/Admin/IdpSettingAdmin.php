<?php

namespace AppBundle\Admin;

use AppBundle\Entity\IdpSetting;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class IdpSettingAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('settingKey')
            ->add('settingValue')
            ->add('settingType')
            ->add('settingHelp')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('settingKey')
            ->add('settingValue')
            ->add('settingType')
            ->add('settingHelp')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('settingKey')
            ->add('settingValue')
            ->add('settingType', 'choice', array(
                'choices' => IdpSetting::getTypes(),
            ))
            ->add('settingHelp')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('settingKey')
            ->add('settingValue')
            ->add('settingType')
            ->add('settingHelp')
        ;
    }
}
