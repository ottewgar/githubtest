<?php

namespace AppBundle\Email;

use AppBundle\Entity\SecondaryEmailOperation;
use AppBundle\Log\Traits\LoggerTrait;
use AppBundle\Util\Sanitizer;
use FOS\UserBundle\Entity\User;
use Swift_Mailer;
use Lexik\Bundle\MailerBundle\Message\MessageFactory;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class NandosMailer
{
    use LoggerTrait;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Lexik\Bundle\MailerBundle\Message\MessageFactory
     */
    private $messageFactory;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var string
     */
    private $emailRecipients;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    private $router;

    /**
     * @param Swift_Mailer $mailer
     * @param MessageFactory $messageFactory
     * @param string $locale
     * @param array $emailRecipients
     * @param Router $router
     */

    public function __construct(Swift_Mailer $mailer, MessageFactory $messageFactory, $locale, $emailRecipients, $router)
    {
        $this->mailer          = $mailer;
        $this->messageFactory  = $messageFactory;
        $this->locale          = $locale;
        $this->emailRecipients = $emailRecipients;
        $this->router          = $router;
    }

    protected function logEmail($user, $template)
    {
        $sanitisedEmail = Sanitizer::sanitiseEmail($user->getEmail());
        $this->info("Send email {$template} for user {$sanitisedEmail}");
    }

    /**
     * @param User $user
     */
    public function sendUserRegisterEmail(User $user, $emailTemplate)
    {
        $message = $this->getUserRegisterEmailMessage($user, $emailTemplate);
        $this->logEmail($user, $emailTemplate);
        $this->mailer->send($message);
    }

    /**
     * @param User $user
     */

    public function sendResendConfirmationEmail(User $user, $emailTemplate)
    {
        $message = $this->getResendConfirmationEmailMessage($user, $emailTemplate);
        $this->logEmail($user, $emailTemplate);
        $this->mailer->send($message);
    }

      /**
     * @param User $user
     */

    public function sendEmailAddressChangedEmail(User $user)
    {
        $message = $this->getEmailAddressChangedEmailMessage($user);
        $this->logEmail($user, 'email_address_changed');
        $this->mailer->send($message);
    }

    /**
     * @param SecondaryEmailOperation $secondaryEmailOperation
     */
    public function sendConfirmMergeOldTakeawayAccountEmail(SecondaryEmailOperation $secondaryEmailOperation)
    {
        $message = $this->getConfirmMergeOldTakeawayAccountEmailMessage($secondaryEmailOperation);
        $this->mailer->send($message);
    }

    /**
     * @param User $user
     */

    public function sendPasswordResetEmail(User $user, $emailTemplate)
    {
        $message = $this->getPasswordResetEmailMessage($user, $emailTemplate);
        $this->logEmail($user, $emailTemplate);
        $this->mailer->send($message);
    }

    /**
     * @param User $user
     * @return \Swift_Message
     */
    public function getUserRegisterEmailMessage(User $user, $emailTemplate)
    {
        if ($emailTemplate == "") {
            $emailTemplate = 'user_register';
        }

        return $this->getMessageForTemplate($emailTemplate, $user, [
            'user'               => $user,
            'confirmation_token' => $user->getConfirmationToken(),
        ]);
    }

    /**
     * @param User $user
     * @return \Swift_Message
     */
    public function getResendConfirmationEmailMessage(User $user, $emailTemplate)
    {
        if ($emailTemplate == "") {
            $emailTemplate = 'resend_confirmation';
        }

        return $this->getMessageForTemplate($emailTemplate, $user, [
            'user'               => $user,
            'confirmation_token' => $user->getConfirmationToken(),
        ]);
    }

    /**
     * @param User $user
     * @return \Swift_Message
     */
    public function getPasswordResetEmailMessage(User $user, $emailTemplate)
    {
        if ($emailTemplate == "") {
            $emailTemplate = 'password_reset';
        }

        return $this->getMessageForTemplate($emailTemplate, $user, [
            'user'               => $user,
            'confirmation_token' => $user->getConfirmationToken(),
        ]);
    }

    /**
     * @param User $user
     * @return \Swift_Message
     */
    public function getEmailAddressChangedEmailMessage(User $user)
    {
        return $this->getMessageForTemplate('email_address_changed', $user, [
            'user'               => $user,
            'confirmation_token' => $user->getConfirmationToken()
        ]);
    }

    /**
     * @param User $user
     * @return \Swift_Message
     */
    public function getConfirmMergeOldTakeawayAccountEmailMessage(SecondaryEmailOperation $secondaryEmailOperation)
    {
        $user = $secondaryEmailOperation->getPrimaryUser();

        return $this->getMessageForTemplate('confirm_merge_old_takeaway_account', $user, [
            'user'                    => $user,
            'secondary_email_address' => $secondaryEmailOperation->getSecondaryEmailAddress(),
            'verification_token'      => $secondaryEmailOperation->getVerificationToken()
        ]);
    }

    /**
     * @param $templateReference
     * @param $to
     * @param array $params
     * @return \Swift_Message
     */
    private function getMessageForTemplate($templateReference, $to, $params = []) {
        return $this->messageFactory->get($templateReference, $to, $params, $this->locale);
    }

}