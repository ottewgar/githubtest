<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Api\Manager\CardUserManagerContainer;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class EmailNotInNandosCardSystemValidator extends ConstraintValidator
{
    /**
     * @var CardUserManagerContainer
     */
    private $cardUserManagerContainer;

    function __construct(CardUserManagerContainer $cardUserManagerContainer)
    {
        $this->cardUserManagerContainer = $cardUserManagerContainer;
    }

    /**
     * @param $entity
     * @param Constraint $constraint
     */
    public function validate($entity, Constraint $constraint)
    {

        /**
         * @var EmailNotInNandosCardSystem
         */
        if (!$constraint instanceof EmailNotInNandosCardSystem) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\UniqueEntity');
        }

        $accessor = PropertyAccess::createPropertyAccessor();

        /**
         * look at the property identified by the "field" option configured in the constraint
         */
        $emailToCheck = strtolower(trim($accessor->getValue($entity, $constraint->field)));

        // NOTE: This will be called even if we had an earlier error e.g. email exists in IDP. We could look in
        // the context to see if the field at errorPath already has an error as skip this step to avoid an API hit?
        // take a look at "group Sequences" - need a way of specifying group sequence at form validation time.
        // Assume it will be similar to validation_group option e.g. api_register, api_register_3rd_party_check

        if (strlen($emailToCheck)) {
            if ($system = $this->cardUserManagerContainer->emailExistsInCardSystem($emailToCheck)) {
                $message = "message" . $system;
                $this->context->buildViolation($constraint->$message)
                    ->atPath($constraint->errorPath)
                    ->addViolation();
            }
        }
    }

}