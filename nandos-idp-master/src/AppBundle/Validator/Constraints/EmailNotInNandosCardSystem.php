<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\User;
use Symfony\Component\Validator\Constraint;

/**
 * This is a class constraint so we can map the error to another field
 * (we need to use the email canonical, but forms typically populate email)
 *
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class EmailNotInNandosCardSystem extends Constraint
{

    public $field = 'emailCanonical';
    public $errorPath = 'email';
    public $service = 'nandos.constraint_validator.email_not_in_nandos_card_system';
    public $message = User::ERROR_EMAIL_EXISTS_IN_NANDOS_CARD_SYSTEM;
    public $messagencr = User::ERROR_EMAIL_EXISTS_IN_NCR;
    public $messagepaytronix = User::ERROR_EMAIL_EXISTS_IN_PAYTRONIX;

    /**
     * The validator must be defined as a service with this name.
     *
     * @return string
     */
    public function validatedBy()
    {
        return $this->service;
    }

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}