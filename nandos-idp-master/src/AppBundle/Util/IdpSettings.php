<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 04/08/16
 * Time: 11:47
 */

namespace AppBundle\Util;


use AppBundle\Entity\IdpSetting;
use Doctrine\ORM\EntityManager;

/**
 * Class IdpSettings
 * @package AppBundle\Util
 */
class IdpSettings
{

    const SETTING_REPOSITORY = "AppBundle:IdpSetting";
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * IdpSettings constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->getRepository();
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository(self::SETTING_REPOSITORY);
    }

    /**
     * @param $key
     */
    public function getSetting($key)
    {
        return $this->repository->findOneBy(['settingKey' => $key]);
    }

    public function setSetting($key, $value, $type)
    {
        $setting = $this->getSetting($key);

        if ($setting instanceof IdpSetting) {
            $setting->setSettingValue($value);
            $this->em->persist($setting);
            $this->em->flush();
        } else {
            $setting = new IdpSetting();
            $setting->setSettingKey($key);
            $setting->getSettingValue($value);
            $setting->setSettingType($type);

            $this->em->persist($setting);
            $this->em->flush();
        }

    }
}