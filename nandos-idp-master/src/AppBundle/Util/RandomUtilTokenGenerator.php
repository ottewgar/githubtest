<?php

namespace AppBundle\Util;

use FOS\UserBundle\Util\TokenGeneratorInterface;
use RandomLib\Generator;
use RandomLib\Factory as RandomLibFactory;
use SecurityLib\Strength;

class RandomUtilTokenGenerator implements TokenGeneratorInterface
{

    /**
     * @var Generator
     */

    private $generator = null;

    /**
     * @var int (see RandomLib\Strength constants)
     */

    private $generatorStrength;

    /**
     * @var int
     */
    private $tokenLength;

    /**
     * Bitmask (see RandomLib\Generator constants) or string of allowed characters
     * @var int|string
     */
    private $tokenCharacters;

    /**
     * The length, strength and characters should NOT be changed at runtime. Use an additional service for a
     * differently configured token generator
     *
     * RandomUtilTokenGenerator constructor.
     * @param $generatorStrength
     * @param $tokenLength
     * @param int|string $tokenCharacters
     */
    public function __construct($tokenLength = 30, $tokenCharacters = Generator::CHAR_BASE64, $generatorStrength = Strength::MEDIUM)
    {
        $this->generatorStrength = $generatorStrength;
        $this->tokenLength       = $tokenLength;
        $this->tokenCharacters   = $tokenCharacters;
    }

    /**
     * Initialize on first use
     *
     * @return Generator
     */
    private function getGenerator()
    {
        if (null === $this->generator) {
            $this->generator = (new RandomLibFactory())->getGenerator(new Strength($this->generatorStrength));
        }

        return $this->generator;
    }

    /**
     * @return string
     */
    public function generateToken()
    {
        return $this->getGenerator()->generateString($this->tokenLength, $this->tokenCharacters);
    }

}