<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 02/09/16
 * Time: 15:41
 */

namespace AppBundle\Util;


class Sanitizer
{
    static public function sanitiseEmail($email)
    {
        $segs = explode('@', $email);

        if (count($segs) > 1) {
            $name = static::insertStars($segs[0]);
            return $name . "@" . $segs[1];
        } else {
            return static::insertStars($email);
        }
    }

    static public function insertStars($name)
    {
        $length = strlen($name);

        return substr_replace($name, static::getStars($length),1, $length -2);
    }

    static public function getStars($length)
    {
        if ($length < 4) {
            return "****";
        }

        $stars = "";

        for($i = 2; $i < $length; $i++) {
            $stars .= "*";
        }

        return $stars;
    }
}