<?php

namespace AppBundle\Util\ApiTest;


use Symfony\Component\HttpFoundation\RequestStack;

class ApiTestRequestValidator
{

    /**
     * The secret that can be set in the url to allow additional information to be returned in the response e.g.
     * confirmation code needed to verify account. This will never be present on the live site.
     *
     * @var string
     */

    private $apiTestSecret;

    /**
     * The name of the query string field that will contain the secret
     * @var string
     */

    private $queryStringField;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * ApiTestRequestValidator constructor.
     * @param string $apiTestSecret
     * @param RequestStack $requestStack
     */
    public function __construct($apiTestSecret, $queryStringField, RequestStack $requestStack)
    {
        $this->apiTestSecret    = trim($apiTestSecret);
        $this->queryStringField = $queryStringField;
        $this->requestStack     = $requestStack;
    }

    public function isValidatedApiTestRequest()
    {

        // if we don't have a secret configured (e.g. in production), this can not be a valid test request
        if (false === $this->apiTestSecret || !strlen($this->apiTestSecret)) {
            return false;
        }

        $request = $this->requestStack->getCurrentRequest();

        if ($request->request->has($this->queryStringField)) {
            $requestApiTestSecret = trim($request->request->get($this->queryStringField));
        } else {
            return false;
        }

        return $requestApiTestSecret == $this->apiTestSecret;

    }

}