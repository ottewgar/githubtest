<?php

namespace AppBundle\Util;


use AppBundle\Api\GeneralApiConfig;
use AppBundle\Api\NcrRadiant\NcrRadiantApi;
use AppBundle\Api\NcrRadiant\NcrRadiantConfig;
use AppBundle\Api\Paytronix\PaytronixApi;
use AppBundle\Api\Paytronix\PaytronixConfig;

class NandosCardConfig
{
    /**
     * @var PaytronixApi
     */
    private $paytronixApi;

    /**
     * @var NcrRadiantApi
     */
    private $ncrApi;

    /**
     * @var IdpSettings
     */
    private $settings;

    /**
     * @var PaytronixConfig
     */
    private $paytronixConfig;

    /**
     * @var NcrRadiantConfig
     */
    private $ncrRadiantConfig;

    private $generalApiConfig;

    /**
     *
     */
    const CARD_PATRONIX = "paytronix";
    /**
     *
     */
    const CARD_NCR = "ncr";

    /**
     * NandosCardConfig constructor.
     * @param PaytronixApi $paytronixApi
     * @param NcrRadiantApi $ncrApi
     * @param IdpSettings $settings
     * @param PaytronixConfig $paytronixConfig
     * @param NcrRadiantConfig $ncrRadiantConfig
     * @param $apiTestMode
     */
    public function __construct(PaytronixApi $paytronixApi, NcrRadiantApi $ncrApi, IdpSettings $settings, PaytronixConfig $paytronixConfig, NcrRadiantConfig $ncrRadiantConfig, GeneralApiConfig $generalApiConfig)
    {
        $this->paytronixApi = $paytronixApi;
        $this->ncrApi = $ncrApi;
        $this->settings = $settings;
        $this->paytronixConfig = $paytronixConfig;
        $this->ncrRadiantConfig = $ncrRadiantConfig;
        $this->generalApiConfig = $generalApiConfig;
    }


    /**
     * Checks either API to see if the email exists
     *
     * Not sure if this is the right place for this
     *
     * @param $cardType
     * @param $email
     * @return mixed
     */
    public function emailExistsInApi($cardType, $email)
    {
        $apiName = $this->getApi($cardType);

        return $this->$apiName->getEmailExistsAll($email);
    }

    /**
     * @param $cardType
     * @return string
     */
    private function getApi($cardType)
    {
        return $cardType."Api";
    }

    /**
     * @return mixed
     */
    public function ncrRadiantEnabled()
    {
        return $this->settings->getSetting('ncr_radiant_enabled')->getSettingValue();
    }

    /**
     * @return mixed
     */
    public function paytronixEnabled()
    {
        return $this->settings->getSetting('paytronix_enabled')->getSettingValue();
    }

    /**
     * @return string
     */
    public function getPaytronixEndpoint()
    {
        return $this->paytronixConfig->getEndpoint();
    }

    /**
     * @return string
     */
    public function getNcrRadiantEndpoint()
    {
        return $this->ncrRadiantConfig->getEndpoint();
    }

    /**
     * @return mixed
     */
    public function getApiTestMode()
    {
        return $this->generalApiConfig->getConfig('api_test_mode');
    }
}