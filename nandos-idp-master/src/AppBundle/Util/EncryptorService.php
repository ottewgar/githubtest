<?php

namespace AppBundle\Util;

use Ambta\DoctrineEncryptBundle\Subscribers\DoctrineEncryptSubscriber;

class EncryptorService
{
    /**
     * @var
     */
    protected $reader;

    /**
     * @var
     */
    protected $encryptor;

    /**
     * EncryptorService constructor.
     * @param $annotationReader
     * @param $encryptor
     */
    public function __construct($annotationReader, $encryptor)
    {
        $this->reader = $annotationReader;
        $this->encryptor = $encryptor;
    }

    /**
     * @param $className
     * @return array
     */
    public function getClassProperties($className)
    {

        $reflectionClass = new \ReflectionClass($className);

        $properties = $reflectionClass->getProperties();
        $propertiesArray = array();

        foreach($properties as $property) {
            $propertyName = $property->getName();
            $propertiesArray[$propertyName] = $property;
        }

        // removed following code as it didn't work with admin filter
        // also that is the reason why this code also exists in EncryptedEntityRepository

//        if($parentClass = $reflectionClass->getParentClass()) {
//            $parentPropertiesArray = $this->getClassProperties($parentClass->getName());
//            if(count($parentPropertiesArray) > 0) {
//                $propertiesArray = array_merge($parentPropertiesArray, $propertiesArray);
//            }
//        }

        $encryptedFields = [];
        foreach ($propertiesArray as $refProperty) {
            if ($this->reader->getPropertyAnnotation($refProperty, DoctrineEncryptSubscriber::ENCRYPTED_ANN_NAME)) {
                $encryptedFields[] = $refProperty->getName();
            }
        }

        return $encryptedFields;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function encrypt($value)
    {
        return $this->encryptor->encrypt($value);
    }
}