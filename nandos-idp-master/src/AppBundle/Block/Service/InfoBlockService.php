<?php

/*
 * This file is part of the Sonata project.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Block\Service;

use AppBundle\Api\ApiService;
use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Util\NandosCardConfig;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Templating\EngineInterface;

/**
 * @author     Thomas Rabaix <thomas.rabaix@sonata-project.org>
 */
class InfoBlockService extends BaseBlockService
{
    protected $cardUserManagerContainer;

    protected $cardConfig;

    public function __construct($name, EngineInterface $templating, CardUserManagerContainer $cardUserManagerContainer, NandosCardConfig $cardConfig)
    {
        $this->name       = $name;
        $this->templating = $templating;
        $this->cardUserManagerContainer = $cardUserManagerContainer;
        $this->cardConfig = $cardConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $info = [
            'ncrEnabled'        => $this->cardUserManagerContainer->getNcrEnabled(),
            'ncrEndpoint'       => $this->cardConfig->getNcrRadiantEndpoint(),
            'paytronixEnabled'  => $this->cardUserManagerContainer->getPaytronixEnabled(),
            'paytronixEndpoint' => $this->cardConfig->getPaytronixEndpoint(),
            'apiTestMode'       => ($this->cardConfig->getApiTestMode()) ? "On" : "Off",
        ];

        return $this->renderResponse($blockContext->getTemplate(), array(
            'block'     => $blockContext->getBlock(),
            'settings'  => $blockContext->getSettings(),
            'info'      => $info,
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        // TODO: Implement validateBlock() method.
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('content', 'textarea', array()),
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Text (core)';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'content'  => 'Insert your custom content here',
            'template' => 'AppBundle:Block:block_core_info.html.twig',
        ));
    }
}
