<?php

namespace AppBundle\Api\NcrRadiant\Response;

use AppBundle\Api\NcrRadiant\Result\GetCardStatusResult;

class GetCardStatusResponse extends BaseResponse
{

    /**
     * @var GetCardStatusResult
     */

    public $getCardStatusResult;

    /**
     * @return GetCardStatusResult
     */
    function getResult()
    {
        return $this->getCardStatusResult;
    }

}