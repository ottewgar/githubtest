<?php

namespace AppBundle\Api\NcrRadiant\Response;

use AppBundle\Api\NcrRadiant\Result\GetCardNumberByEmailResult;

class GetCardNumberByEmailResponse extends BaseResponse
{

    /**
     * @var GetCardNumberByEmailResult
     */

    public $GetCardNumberByEmailResult;

    /**
     * @return GetCardNumberByEmailResult
     */
    function getResult()
    {
        return $this->GetCardNumberByEmailResult;
    }

}