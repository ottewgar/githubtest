<?php

namespace AppBundle\Api\NcrRadiant\Response;

use AppBundle\Api\NcrRadiant\Result\EmailExistsResult;

class EmailExistsResponse extends BaseResponse
{

    /**
     * @var EmailExistsResult
     */

    public $eMailExistsResult;

    /**
     * @return EmailExistsResult
     */
    function getResult()
    {
        return $this->eMailExistsResult;
    }

}