<?php

namespace AppBundle\Api\NcrRadiant\Response;

use AppBundle\Api\NcrRadiant\Result\GetCardStatusResult;

class AddMemberProfileResponse extends BaseResponse
{

    /**
     * @var GetMemberProfileResult
     */

    public $addMemberProfileResult;

    /**
     * @return GetMemberProfileResult
     */
    function getResult()
    {
        return $this->addMemberProfileResult;
    }

}