<?php

namespace AppBundle\Api\NcrRadiant\Response;

use AppBundle\Api\NcrRadiant\Result\GetBonusPlanHistoryResult;

class GetBonusPlanHistoryResponse extends BaseResponse
{

    /**
     * @var GetBonusPlanHistoryResult
     */
    public $GetBonusPlanHistoryResult;

    function getResult()
    {
        return $this->GetBonusPlanHistoryResult;
    }


}