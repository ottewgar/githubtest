<?php

namespace AppBundle\Api\NcrRadiant\Response;

use AppBundle\Api\NcrRadiant\Result\BaseResult;

abstract class BaseResponse
{
    /**
     * Should be implemented and used to return the appropriate Result object
     * @return BaseResult
     */
    abstract function getResult();

    /**
     * The desired result of the response. By default this is the Result object, but may be a scalar, and array
     * or other object.
     *
     * this will fail if getResult() does not return
     *
     * @return mixed
     */

    public function getReturnValue()
    {
        $result = $this->getResult();

        if ($result instanceof BaseResult) {
            return $result->getReturnValue();
        } else {
            throw new \Exception('Either ensure getResult() returns a BaseResult in ' . static::class . ' or implement a custom getReturnValue()');
        }

    }
}