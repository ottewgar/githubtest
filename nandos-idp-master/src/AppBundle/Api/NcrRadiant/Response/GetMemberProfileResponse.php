<?php

namespace AppBundle\Api\NcrRadiant\Response;

use AppBundle\Api\NcrRadiant\Result\GetCardStatusResult;

class GetMemberProfileResponse extends BaseResponse
{

    /**
     * @var GetMemberProfileResult
     */

    public $getMemberProfileResult;

    /**
     * @return GetMemberProfileResult
     */
    function getResult()
    {
        return $this->getMemberProfileResult;
    }

}