<?php

namespace AppBundle\Api\NcrRadiant\Response;

use AppBundle\Api\NcrRadiant\Result\EmailExistsAlResult;

class EmailExistsAllResponse extends BaseResponse
{

    /**
     * @var EmailExistsAllResult
     */

    public $eMailExistsAllResult;

    /**
     * @return EmailExistsAllResult
     */
    function getResult()
    {
        return $this->eMailExistsAllResult;
    }

}