<?php


namespace AppBundle\Api\NcrRadiant;

use AppBundle\Log\Traits\LoggerTrait;
use \DOMDocument;

use Wse\WSSESoap;

class NcrRadiantSoapClient extends \SoapClient
{
    use LoggerTrait;

    private $wsseUsername;

    private $wssePassword;

    /**
     * @var array default classmap - uncomment as and when classes are implemented
     */

    private static $classmap = array(

        // Value objects within results
        'MemberProfile'       => 'AppBundle\Api\NcrRadiant\Result\MemberProfile',
        'VanityDate'          => 'AppBundle\Api\NcrRadiant\Result\VanityDate',
        'assignment'          => 'AppBundle\Api\NcrRadiant\Result\Assignment',
        'rewardProgramDetail' => 'AppBundle\Api\NcrRadiant\Result\RewardProgramDetail',
        'Reward'              => 'AppBundle\Api\NcrRadiant\Result\Reward',
        'bonusPlanType'       => 'AppBundle\Api\NcrRadiant\Result\BonusPlanType',
        'checkItem'           => 'AppBundle\Api\NcrRadiant\Result\CheckItem',

        // Namespaced Request / Response / Result calls

        'AddMemberProfileRequest'  => 'AppBundle\Api\NcrRadiant\Request\AddMemberProfileRequest',
        'addMemberProfileResponse' => 'AppBundle\Api\NcrRadiant\Response\AddMemberProfileResponse',
        'AddMemberProfileResult'   => 'AppBundle\Api\NcrRadiant\Result\AddMemberProfileResult',

        'eMailExistsRequest'  => 'AppBundle\Api\NcrRadiant\Request\EmailExistsRequest',
        'eMailExistsResponse' => 'AppBundle\Api\NcrRadiant\Response\EmailExistsResponse',
        'EMailExistsResult'   => 'AppBundle\Api\NcrRadiant\Result\EmailExistsResult',

        'eMailExistsAllRequest'  => 'AppBundle\Api\NcrRadiant\Request\EmailExistsAllRequest',
        'eMailExistsAllResponse' => 'AppBundle\Api\NcrRadiant\Response\EmailExistsAllResponse',
        'EMailExistsAllResult'   => 'AppBundle\Api\NcrRadiant\Result\EmailExistsAllResult',

        'GetCardNumberByEmailRequest'  => 'AppBundle\Api\NcrRadiant\Request\GetCardNumberByEmailRequest',
        'getCardNumberByEmailResponse' => 'AppBundle\Api\NcrRadiant\Response\GetCardNumberByEmailResponse',
        'GetCardNumberByEmailResult'   => 'AppBundle\Api\NcrRadiant\Result\GetCardNumberByEmailResult',

        'GetCardStatusRequest'  => 'AppBundle\Api\NcrRadiant\Request\GetCardStatusRequest',
        'getCardStatusResponse' => 'AppBundle\Api\NcrRadiant\Response\GetCardStatusResponse',
        'GetCardStatusResult'   => 'AppBundle\Api\NcrRadiant\Result\GetCardStatusResult',

        'GetMemberProfileRequest'  => 'AppBundle\Api\NcrRadiant\Request\GetMemberProfileRequest',
        'getMemberProfileResponse' => 'AppBundle\Api\NcrRadiant\Response\GetMemberProfileResponse',
        'GetMemberProfileResult'   => 'AppBundle\Api\NcrRadiant\Result\GetMemberProfileResult',

        'GetBonusPlanHistoryRequest'  => 'AppBundle\Api\NcrRadiant\Request\GetBonusPlanHistoryRequest',
        'getBonusPlanHistoryResponse' => 'AppBundle\Api\NcrRadiant\Response\GetBonusPlanHistoryResponse',
        'GetBonusPlanHistoryResult'   => 'AppBundle\Api\NcrRadiant\Result\GetBonusPlanHistoryResult',

//        'getCheckDetail' => 'getCheckDetail',
//        'GetCheckDetailRequest' => 'GetCheckDetailRequest',
//        'Request' => 'Request',
//        'getCheckDetailResponse' => 'getCheckDetailResponse',
//        'GetCheckDetailResult' => 'GetCheckDetailResult',
//        'Response' => 'Response',
//        'unassignedTransactionData' => 'unassignedTransactionData',
//        'checkDetails' => 'checkDetails',
//        'checkItemDetail' => 'checkItemDetail',

//        'checkNetwork' => 'checkNetwork',
//        'CheckNetworkRequest' => 'CheckNetworkRequest',
//        'checkNetworkResponse' => 'checkNetworkResponse',
//        'CheckNetworkResult' => 'CheckNetworkResult',
//        'optOut' => 'optOut',
//        'OptOutRequest' => 'OptOutRequest',
//        'optOutResponse' => 'optOutResponse',
//        'OptOutResult' => 'OptOutResult',
//        'getCardBalance' => 'getCardBalance',
//        'GetCardBalanceRequest' => 'GetCardBalanceRequest',
//        'getCardBalanceResponse' => 'getCardBalanceResponse',
//        'GetCardBalanceResult' => 'GetCardBalanceResult',
//        'getNewCardBatchID' => 'getNewCardBatchID',
//        'GetNewCardBatchIDRequest' => 'GetNewCardBatchIDRequest',
//        'getNewCardBatchIDResponse' => 'getNewCardBatchIDResponse',
//        'GetNewCardBatchIDResult' => 'GetNewCardBatchIDResult',
//        'getCardStatus' => 'getCardStatus',

//        'changeCardNumber' => 'changeCardNumber',
//        'ChangeCardNumberRequest' => 'ChangeCardNumberRequest',
//        'changeCardNumberResponse' => 'changeCardNumberResponse',
//        'ChangeCardNumberResult' => 'ChangeCardNumberResult',
//        'getBonusPlanHistory' => 'getBonusPlanHistory',

//        'optIn' => 'optIn',
//        'OptInRequest' => 'OptInRequest',
//        'optInResponse' => 'optInResponse',
//        'OptInResult' => 'OptInResult',
//        'getClaimCount' => 'getClaimCount',
//        'GetClaimCountRequest' => 'GetClaimCountRequest',
//        'getClaimCountResponse' => 'getClaimCountResponse',
//        'GetClaimCountResult' => 'GetClaimCountResult',
//        'processDynamicCompDeposit' => 'processDynamicCompDeposit',
//        'ProcessDynamicCompDepositRequest' => 'ProcessDynamicCompDepositRequest',
//        'processDynamicCompDepositResponse' => 'processDynamicCompDepositResponse',
//        'ProcessDynamicCompDepositResult' => 'ProcessDynamicCompDepositResult',
//        'getBonusPlanStandings' => 'getBonusPlanStandings',
//        'GetBonusPlanStandingsRequest' => 'GetBonusPlanStandingsRequest',
//        'getBonusPlanStandingsResponse' => 'getBonusPlanStandingsResponse',
//        'GetBonusPlanStandingsResult' => 'GetBonusPlanStandingsResult',
//        'BonusPlanStanding' => 'BonusPlanStanding',
//        'categoryItem' => 'categoryItem',
//        'availableSmartReward' => 'availableSmartReward',
//        'QueuedReward' => 'QueuedReward',
//        'eMailExists' => 'eMailExists',
//        'validatePassCode' => 'validatePassCode',
//        'ValidatePassCodeRequest' => 'ValidatePassCodeRequest',
//        'validatePassCodeResponse' => 'validatePassCodeResponse',
//        'ValidatePassCodeResult' => 'ValidatePassCodeResult',
//        'eMailExistsAll' => 'eMailExistsAll',
//        'addMemberProfile' => 'addMemberProfile',
//        'getMemberProfile' => 'getMemberProfile',
//        'GetMemberProfileRequest' => 'GetMemberProfileRequest',
//        'getMemberProfileResponse' => 'getMemberProfileResponse',
//        'GetMemberProfileResult' => 'GetMemberProfileResult',
//        'combineAccount' => 'combineAccount',
//        'CombineAccountRequest' => 'CombineAccountRequest',
//        'combineAccountResponse' => 'combineAccountResponse',
//        'CombineAccountResult' => 'CombineAccountResult',
//        'assignForgottenCard' => 'assignForgottenCard',
//        'AssignForgottenCardRequest' => 'AssignForgottenCardRequest',
//        'assignForgottenCardResponse' => 'assignForgottenCardResponse',
//        'AssignForgottenCardResult' => 'AssignForgottenCardResult',
//        'phoneNumberExists' => 'phoneNumberExists',
//        'PhoneNumberExistsRequest' => 'PhoneNumberExistsRequest',
//        'phoneNumberExistsResponse' => 'phoneNumberExistsResponse',
//        'PhoneNumberExistsResult' => 'PhoneNumberExistsResult',
//        'getCreateNewCardStatus' => 'getCreateNewCardStatus',
//        'GetCreateNewCardStatusRequest' => 'GetCreateNewCardStatusRequest',
//        'getCreateNewCardStatusResponse' => 'getCreateNewCardStatusResponse',
//        'GetCreateNewCardStatusResult' => 'GetCreateNewCardStatusResult',
//        'CardNumberList' => 'CardNumberList',
//        'FailedCardNumberList' => 'FailedCardNumberList',
//        'getCardNumberByPhone' => 'getCardNumberByPhone',
//        'GetCardNumberByPhoneRequest' => 'GetCardNumberByPhoneRequest',
//        'getCardNumberByPhoneResponse' => 'getCardNumberByPhoneResponse',
//        'GetCardNumberByPhoneResult' => 'GetCardNumberByPhoneResult',
//        'phoneNumberExistsAll' => 'phoneNumberExistsAll',
//        'PhoneNumberExistsAllRequest' => 'PhoneNumberExistsAllRequest',
//        'phoneNumberExistsAllResponse' => 'phoneNumberExistsAllResponse',
//        'PhoneNumberExistsAllResult' => 'PhoneNumberExistsAllResult',
//        'getDynamicCompBalance' => 'getDynamicCompBalance',
//        'GetDynamicCompBalanceRequest' => 'GetDynamicCompBalanceRequest',
//        'getDynamicCompBalanceResponse' => 'getDynamicCompBalanceResponse',
//        'GetDynamicCompBalanceResult' => 'GetDynamicCompBalanceResult',
//        'combineAccountCount' => 'combineAccountCount',
//        'CombineAccountCountRequest' => 'CombineAccountCountRequest',
//        'combineAccountCountResponse' => 'combineAccountCountResponse',
//        'CombineAccountCountResult' => 'CombineAccountCountResult',
//        'getNewCardList' => 'getNewCardList',
//        'GetNewCardListRequest' => 'GetNewCardListRequest',
//        'getNewCardListResponse' => 'getNewCardListResponse',
//        'GetNewCardListResult' => 'GetNewCardListResult',
//        'createNewCard' => 'createNewCard',
//        'CreateNewCardRequest' => 'CreateNewCardRequest',
//        'createNewCardResponse' => 'createNewCardResponse',
//        'CreateNewCardResult' => 'CreateNewCardResult',
//        'activateNewCard' => 'activateNewCard',
//        'ActivateNewCardRequest' => 'ActivateNewCardRequest',
//        'activateNewCardResponse' => 'activateNewCardResponse',
//        'ActivateNewCardResult' => 'ActivateNewCardResult',
//        'getDynamicCompConfig' => 'getDynamicCompConfig',
//        'GetDynamicCompConfigRequest' => 'GetDynamicCompConfigRequest',
//        'getDynamicCompConfigResponse' => 'getDynamicCompConfigResponse',
//        'GetDynamicCompConfigResult' => 'GetDynamicCompConfigResult',
//        'DynamicCards' => 'DynamicCards',
//        'dynamicReason' => 'dynamicReason',
//        'getCardNumberByEmail' => 'getCardNumberByEmail',
//        'validateEmailByPassCode' => 'validateEmailByPassCode',
//        'ValidateEmailByPassCodeRequest' => 'ValidateEmailByPassCodeRequest',
//        'validateEmailByPassCodeResponse' => 'validateEmailByPassCodeResponse',
//        'ValidateEmailByPassCodeResult' => 'ValidateEmailByPassCodeResult',
//        'combineAccountGetParent' => 'combineAccountGetParent',
//        'CombineAccountGetParentRequest' => 'CombineAccountGetParentRequest',
//        'combineAccountGetParentResponse' => 'combineAccountGetParentResponse',
//        'CombineAccountGetParentResult' => 'CombineAccountGetParentResult',
//        'setDeferRewardsSetting' => 'setDeferRewardsSetting',
//        'SetDeferRewardsSettingRequest' => 'SetDeferRewardsSettingRequest',
//        'setDeferRewardsSettingResponse' => 'setDeferRewardsSettingResponse',
//        'SetDeferRewardsSettingResult' => 'SetDeferRewardsSettingResult',
//        'adjustCredit' => 'adjustCredit',
//        'AdjustCreditRequest' => 'AdjustCreditRequest',
//        'adjustCreditResponse' => 'adjustCreditResponse',
//        'AdjustCreditResult' => 'AdjustCreditResult',
//        'combineAccountGetChildren' => 'combineAccountGetChildren',
//        'CombineAccountGetChildrenRequest' => 'CombineAccountGetChildrenRequest',
//        'combineAccountGetChildrenResponse' => 'combineAccountGetChildrenResponse',
//        'CombineAccountGetChildrenResult' => 'CombineAccountGetChildrenResult',
//        'deactivateNewCard' => 'deactivateNewCard',
//        'DeactivateNewCardRequest' => 'DeactivateNewCardRequest',
//        'deactivateNewCardResponse' => 'deactivateNewCardResponse',
//        'DeactivateNewCardResult' => 'DeactivateNewCardResult',
//        'unCombineAccount' => 'unCombineAccount',
//        'UnCombineAccountRequest' => 'UnCombineAccountRequest',
//        'unCombineAccountResponse' => 'unCombineAccountResponse',
//        'UnCombineAccountResult' => 'UnCombineAccountResult',
//        'updateMemberProfile' => 'updateMemberProfile',
//        'UpdateMemberProfileRequest' => 'UpdateMemberProfileRequest',
//        'updateMemberProfileResponse' => 'updateMemberProfileResponse',
//        'UpdateMemberProfileResult' => 'UpdateMemberProfileResult',
//        'getSVHistory' => 'getSVHistory',
//        'GetSVHistoryRequest' => 'GetSVHistoryRequest',
//        'getSVHistoryResponse' => 'getSVHistoryResponse',
//        'GetSVHistoryResult' => 'GetSVHistoryResult',
//        'SVTransactionHistory' => 'SVTransactionHistory',
//        'getDeferRewardsSetting' => 'getDeferRewardsSetting',
//        'GetDeferRewardsSettingRequest' => 'GetDeferRewardsSettingRequest',
//        'getDeferRewardsSettingResponse' => 'getDeferRewardsSettingResponse',
//        'GetDeferRewardsSettingResult' => 'GetDeferRewardsSettingResult',

//        'occasionType' => 'occasionType',
//        'accountStatus' => 'accountStatus',
    );

    public function __construct($wsdl, $options = [])
    {
        foreach (static::$classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }

        // Due to a problem with the SSL setup of NCR Radiant, we have to use less strict SSL verification settings

        $options['stream_context'] = stream_context_create(array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        ));

        parent::__construct($wsdl, $options);
    }

    /**
     * @param mixed $wsseUsername
     */
    public function setWsseUsername($wsseUsername)
    {
        $this->wsseUsername = $wsseUsername;
    }

    /**
     * @param mixed $wssePassword
     */
    public function setWssePassword($wssePassword)
    {
        $this->wssePassword = $wssePassword;
    }

    function __doRequest($originalRequest, $location, $action, $version, $oneWay = null)
    {

        // create new XML Document based off the original SOAP request
        $doc = new DOMDocument('1.0');
        $doc->loadXML($originalRequest);

        // pass it to the WSSE utility
        $wsseSoap = new WSSESoap($doc);

        $wsseSoap->addTimestamp();
        $wsseSoap->addUserToken($this->wsseUsername, $this->wssePassword, false);

        // extract modified WSSE request
        $wsseRequest = $wsseSoap->saveXML();

        $result = parent::__doRequest($wsseRequest, $location, $action, $version, $oneWay);

        return $result;

    }

}