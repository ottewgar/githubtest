<?php

namespace AppBundle\Api\NcrRadiant\Result;

class EmailExistsAllResult extends BaseResult {

    /**
     * @var string
     */

    public $eMailAddress;

    /**
     * Note the spelling. The case has to match exactly
     *
     * @var string
     */

    public $EMailAddressExists;

    public function getEmailAddressExists()
    {
        return $this->EMailAddressExists;
    }

    public function getReturnValue()
    {
       return $this->getEmailAddressExists();
    }

}