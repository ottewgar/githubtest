<?php

namespace AppBundle\Api\NcrRadiant\Result;

class GetCardNumberByEmailResult extends BaseResult {

    public $cardNumber;

    public function getReturnValue()
    {
       return $this->cardNumber;
    }

}