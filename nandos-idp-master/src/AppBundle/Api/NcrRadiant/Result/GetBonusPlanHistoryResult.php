<?php
/**
 * Created by PhpStorm.
 * User: alistairburns
 * Date: 10/05/2016
 * Time: 13:41
 */

namespace AppBundle\Api\NcrRadiant\Result;


class GetBonusPlanHistoryResult extends BaseResult
{
    /**
     * @var Assignment[]
     */

    public $assignment;

    /**
     * @var string
     */

    public $cardNumber;
}