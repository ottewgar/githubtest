<?php

namespace AppBundle\Api\NcrRadiant\Result;


class GetMemberProfileResult extends BaseResult
{

    /**
     * @var string
     */

    public $cardNumber;

    /**
     * @var MemberProfile
     */

    public $profile = false;

    /**
     * @return MemberProfile
     */

    public function getReturnValue()
    {

        if ($this->profile instanceof MemberProfile) {
            $this->profile->processResultValues();
        }

        return $this->profile;
    }


}