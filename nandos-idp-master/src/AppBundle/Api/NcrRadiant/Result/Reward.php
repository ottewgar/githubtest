<?php

namespace AppBundle\Api\NcrRadiant\Result;


class Reward
{
    public $name; // string
    public $id; // int
    public $amount; // float
}