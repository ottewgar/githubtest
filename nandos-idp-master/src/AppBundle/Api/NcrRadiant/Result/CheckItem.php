<?php

namespace AppBundle\Api\NcrRadiant\Result;

class CheckItem
{

    /**
     * @var float
     */

    public $basePrice;

    /**
     * @var float
     */

    public $discountPrice;

    /**
     * @var int
     */

    public $id;


    /**
     * @var string
     */

    public $longName;
}