<?php

namespace AppBundle\Api\NcrRadiant\Result;

class GetCardStatusResult extends BaseResult {

    /**
     * @var bool
     */

    public $accountActive;

    /**
     * @var bool
     */
    public $cardExists;

    /**
     * @var string
     */

    public $cardNumber;

    /**
     * @var bool
     */

    public $profileExists;

    /**
     * @var bool
     */

    public $requirePin;

    /**
     * @var bool
     */

    public $seriesActive;

    /**
     * @return boolean
     */
    public function isAccountActive()
    {
        return $this->accountActive;
    }

    /**
     * @return boolean
     */
    public function getcardExists()
    {
        return $this->cardExists;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @return boolean
     */
    public function getprofileExists()
    {
        return $this->profileExists;
    }

    /**
     * @return boolean
     */
    public function getRequirePin()
    {
        return $this->requirePin;
    }

    /**
     * @return boolean
     */
    public function isSeriesActive()
    {
        return $this->seriesActive;
    }

    /**
     * @return GetCardStatusResult
     */

    public function getReturnValue()
    {
       return $this;
    }

}