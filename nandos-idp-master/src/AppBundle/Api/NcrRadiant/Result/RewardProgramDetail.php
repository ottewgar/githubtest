<?php
/**
 * Created by PhpStorm.
 * User: alistairburns
 * Date: 10/05/2016
 * Time: 13:47
 */

namespace AppBundle\Api\NcrRadiant\Result;


class RewardProgramDetail
{

    public $bonusPlanType; // bonusPlanType
    public $creditEarned; // double
    public $id; // int
    public $name; // string
    public $reward; // Reward

}