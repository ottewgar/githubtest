<?php

namespace AppBundle\Api\NcrRadiant\Result;


class AddMemberProfileResult extends BaseResult
{

    /**
     * @var string
     */

    public $cardNumber;

    /**
     * @return MemberProfile
     */

    public function getReturnValue()
    {
        return $this->cardNumber;
    }


}