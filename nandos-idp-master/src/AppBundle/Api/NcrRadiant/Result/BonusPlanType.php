<?php

namespace AppBundle\Api\NcrRadiant\Result;


class BonusPlanType
{
    const UNKNOWN = 'UNKNOWN';
    const ITEM = 'ITEM';
    const CURRENCY = 'CURRENCY';
    const POINT = 'POINT';
    const VISIT = 'VISIT';
    const LOTTERY = 'LOTTERY';
    const GUEST = 'GUEST';
    const EMPLOYEE = 'EMPLOYEE';
}