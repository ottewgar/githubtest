<?php

namespace AppBundle\Api\NcrRadiant\Result;


class BaseResult {

    /**
     * @return BaseResult|mixed
     */

    public function getReturnValue()
    {
        return $this;
    }

}