<?php
/**
 * Created by PhpStorm.
 * User: alistairburns
 * Date: 10/05/2016
 * Time: 13:44
 */

namespace AppBundle\Api\NcrRadiant\Result;


class Assignment
{

    /**
     * @var string (dateTime)
     */

    public $assignmentDate;

    /**
     * @var int
     */

    public $assignmentID;


    /**
     * @var int
     */

    public $checkId;

    /**
     * @var CheckItem[]
     */

    public $checkItem;

    /**
     * @var string
     */

    public $checkTotal;

    /**
     * @var float
     */

    public $nonEligiblePurchaseTotal;

    /**
     * @var float
     */

    public $payment;

    /**
     * @var RewardProgramDetail
     */

    public $rewardProgramDetail;

    /**
     * @var int
     */

    public $storeId;

    /**
     * @var string
     */

    public $storeName; // string

}