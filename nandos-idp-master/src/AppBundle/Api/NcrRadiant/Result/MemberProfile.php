<?php
/**
 * Created by PhpStorm.
 * User: alistairburns
 * Date: 22/02/2016
 * Time: 12:00
 */

namespace AppBundle\Api\NcrRadiant\Result;


class MemberProfile
{

    const NANDOSCARD_BROWSER_FIELD    = 'companyDefined11';
    const NANDOSCARD_GOLD1_FIELD      = 'companyDefined12';
    const NANDOSCARD_GOLD2_FIELD      = 'companyDefined13';
    const NANDOSCARD_GOLD3_FIELD      = 'companyDefined14';
    const NANDOSCARD_GOLD4_FIELD      = 'companyDefined15';
    const NANDOSCARD_SALUTATION_FIELD = 'companyDefined16';
    const NANDOSCARD_OPTIN_NEWS_FIELD = 'companyDefined17';
    const NANDOSCARD_OPTIN_SMS_FIELD  = 'companyDefined19';
    const NANDOSCARD_PASSWORD_FIELD   = 'companyDefined20';
    const NANDOSCARD_PASSWORD_FIELD2  = 'companyDefined21';
    const NANDOSCARD_STUDENT          = 'companyDefined22';
    const NANDOSCARD_GRADUATION       = 'companyDefined23';
    const NANDOSCARD_STUDENT_INST     = 'companyDefined24';

    private $resultValuesProcessed = false;
    private $convertedToRadiant = false;

    public $address1; // string
    public $address2; // string
    public $anniversaryDate; // VanityDate
    public $cardNumber; // string
    public $ccNumber; // string
    public $city; // string
    public $company; // string
    public $country; // string
    public $dateOfBirth; // VanityDate
    public $driversLicense; // string
    public $emailAddress; // string
    public $firstName; // string
    public $lastName; // string
    public $memberAccountId; // int
    public $otherPhoneNumber; // string
    public $phoneNumber; // string
    public $postalCode; // string
    public $profileExists; // boolean
    public $ssNumber; // string
    public $stateProvince; // string

    public $companyDefined1; // string
    public $companyDefined2; // string
    public $companyDefined3; // string
    public $companyDefined4; // string
    public $companyDefined5; // string
    public $companyDefined6; // string
    public $companyDefined7; // string
    public $companyDefined8; // string
    public $companyDefined9; // string
    public $companyDefined10; // string
    public $companyDefined11; // string
    public $companyDefined12; // string
    public $companyDefined13; // string
    public $companyDefined14; // string
    public $companyDefined15; // string
    public $companyDefined16; // string
    public $companyDefined17; // string
    public $companyDefined18; // string
    public $companyDefined19; // string
    public $companyDefined20; // string
    public $companyDefined21; // string
    public $companyDefined22; // string
    public $companyDefined23; // string
    public $companyDefined24; // string
    public $companyDefined25; // string
    public $companyDefined26; // string
    public $companyDefined27; // string
    public $companyDefined28; // string
    public $companyDefined29; // string
    public $companyDefined30; // string

    public $browser;
    public $goldQuestion1;
    public $goldQuestion2;
    public $goldQuestion3;
    public $goldQuestion4;
    public $salutation;
    public $optin;
    public $optinSms;
    public $password;
    public $password2;
    public $student;
    public $studentInst;
    public $graduation;

    private $passwordHash;

    private $companyDefinedFieldmap = array(
        'browser'       => self::NANDOSCARD_BROWSER_FIELD,
        'goldQuestion1' => self::NANDOSCARD_GOLD1_FIELD,
        'goldQuestion2' => self::NANDOSCARD_GOLD2_FIELD,
        'goldQuestion3' => self::NANDOSCARD_GOLD3_FIELD,
        'goldQuestion4' => self::NANDOSCARD_GOLD4_FIELD,
        'salutation'    => self::NANDOSCARD_SALUTATION_FIELD,
        'optin'         => self::NANDOSCARD_OPTIN_NEWS_FIELD,
        'optinSms'      => self::NANDOSCARD_OPTIN_SMS_FIELD,
        'password'      => self::NANDOSCARD_PASSWORD_FIELD,
        'password2'     => self::NANDOSCARD_PASSWORD_FIELD2,
        'student'       => self::NANDOSCARD_STUDENT,
        'studentInst'   => self::NANDOSCARD_STUDENT_INST,
        'graduation'    => self::NANDOSCARD_GRADUATION,
    );

    /**
     * Get a clone of the member profile, with values mapped ready for Ncr
     *
     * @return MemberProfile
     */

    public function getNcrConvertedProfile()
    {

        $profile = clone($this);
        $profile->convertToRadiantRequest();

        return $profile;

    }

    /**
     * Shift our custom properties back to where NCR expects them to be, so we can update the member profile
     */

    public function convertToRadiantRequest()
    {
        if ($this->convertedToRadiant) {
            return;
        }
        $this->remapCompanyDefinedFieldsToRadiant();
        $this->convertFieldDataToRadiant();
        $this->convertedToRadiant = true;

    }

    /**
     * Process Radiant values - move custom company defined fields into their own properties
     * and trim/transform other values
     */
    public function processResultValues()
    {
        // Do only once or data will be blanked
        if ($this->resultValuesProcessed) {
            return;
        }

        $this->remapCompanyDefinedFieldsFromRadiant();
        $this->convertFieldDataFromRadiant();
        $this->resultValuesProcessed = true;

    }

    /**
     * Copy values out from the company defined slots and into their own variables
     */

    private function remapCompanyDefinedFieldsFromRadiant()
    {
        foreach ($this->companyDefinedFieldmap as $localProperty => $radiantField) {
            $this->{$localProperty} = $this->{$radiantField};
            unset($this->{$radiantField});
        }

    }

   private function remapCompanyDefinedFieldsToRadiant()
    {
        foreach ($this->companyDefinedFieldmap as $localProperty => $radiantField) {
            $this->{$radiantField} = $this->{$localProperty};
            unset($this->{$localProperty});
        }
    }

    /**
     * Adapted from ml_get_member_profile() in lib/mlSoap.php
     */

    private function convertFieldDataFromRadiant() {

        foreach (get_object_vars($this) as $property => $value) {
            // pull date from inner object into property
            if ($value instanceof VanityDate) {
                $this->{$property} = $value->date ? $value->date : '';
            } else {
                // trim the value
                if (isset($this->{$property})
                    && !empty($this->{$property})
                    && !is_array($this->{$property})
                    && !is_object($this->{$property})
                ) {
                    $this->{$property} = trim($this->{$property});
                }
            }
        }

        // the password fields are combined as the password hash
        $this->updatePasswordHash();

    }

    private function convertFieldDataToRadiant()
    {
        foreach (['dateOfBirth'] as $dateField) {
            if ($dateString = $this->{$dateField}) {
                $vanityDate = new VanityDate();
                $vanityDate
                    ->setDate($dateString)
                    ->setLocale('en_GB');
                $this->{$dateField} = $vanityDate;
            }
        }

        unset($this->passwordHash);

    }


    public function updatePassword($password)
    {
        $password = trim($password);

        $passwordParts = str_split($password, 50);

        $this->setPassword($passwordParts[0]);
        if (array_key_exists(1, $passwordParts)) {
            $this->setPassword2($passwordParts[1]);
        } else {
            $this->setPassword2('');
        }

        $this->updatePasswordHash();
    }

    private function updatePasswordHash()
    {
        $this->passwordHash = $this->password;
        if (strlen($this->password2)) {
            $this->passwordHash.= $this->password2;
        }
    }

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @return mixed
     */
    public function getAnniversaryDate()
    {
        return $this->anniversaryDate;
    }

    /**
     * @param mixed $anniversaryDate
     */
    public function setAnniversaryDate($anniversaryDate)
    {
        $this->anniversaryDate = $anniversaryDate;
    }

    /**
     * @return mixed
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param mixed $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return mixed
     */
    public function getCcNumber()
    {
        return $this->ccNumber;
    }

    /**
     * @param mixed $ccNumber
     */
    public function setCcNumber($ccNumber)
    {
        $this->ccNumber = $ccNumber;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param mixed $dateOfBirth
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return mixed
     */
    public function getDriversLicense()
    {
        return $this->driversLicense;
    }

    /**
     * @param mixed $driversLicense
     */
    public function setDriversLicense($driversLicense)
    {
        $this->driversLicense = $driversLicense;
    }

    /**
     * @return mixed
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param mixed $emailAddress
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getMemberAccountId()
    {
        return $this->memberAccountId;
    }

    /**
     * @param mixed $memberAccountId
     */
    public function setMemberAccountId($memberAccountId)
    {
        $this->memberAccountId = $memberAccountId;
    }

    /**
     * @return mixed
     */
    public function getOtherPhoneNumber()
    {
        return $this->otherPhoneNumber;
    }

    /**
     * @param mixed $otherPhoneNumber
     */
    public function setOtherPhoneNumber($otherPhoneNumber)
    {
        $this->otherPhoneNumber = $otherPhoneNumber;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return mixed
     */
    public function getProfileExists()
    {
        return $this->profileExists;
    }

    /**
     * @param mixed $profileExists
     */
    public function setProfileExists($profileExists)
    {
        $this->profileExists = $profileExists;
    }

    /**
     * @return mixed
     */
    public function getSsNumber()
    {
        return $this->ssNumber;
    }

    /**
     * @param mixed $ssNumber
     */
    public function setSsNumber($ssNumber)
    {
        $this->ssNumber = $ssNumber;
    }

    /**
     * @return mixed
     */
    public function getStateProvince()
    {
        return $this->stateProvince;
    }

    /**
     * @param mixed $stateProvince
     */
    public function setStateProvince($stateProvince)
    {
        $this->stateProvince = $stateProvince;
    }

    /**
     * @return mixed
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * @param mixed $browser
     */
    public function setBrowser($browser)
    {
        $this->browser = $browser;
    }

    /**
     * @return mixed
     */
    public function getGoldQuestion1()
    {
        return $this->goldQuestion1;
    }

    /**
     * @param mixed $goldQuestion1
     */
    public function setGoldQuestion1($goldQuestion1)
    {
        $this->goldQuestion1 = $goldQuestion1;
    }

    /**
     * @return mixed
     */
    public function getGoldQuestion2()
    {
        return $this->goldQuestion2;
    }

    /**
     * @param mixed $goldQuestion2
     */
    public function setGoldQuestion2($goldQuestion2)
    {
        $this->goldQuestion2 = $goldQuestion2;
    }

    /**
     * @return mixed
     */
    public function getGoldQuestion3()
    {
        return $this->goldQuestion3;
    }

    /**
     * @param mixed $goldQuestion3
     */
    public function setGoldQuestion3($goldQuestion3)
    {
        $this->goldQuestion3 = $goldQuestion3;
    }

    /**
     * @return mixed
     */
    public function getGoldQuestion4()
    {
        return $this->goldQuestion4;
    }

    /**
     * @param mixed $goldQuestion4
     */
    public function setGoldQuestion4($goldQuestion4)
    {
        $this->goldQuestion4 = $goldQuestion4;
    }

    /**
     * @return mixed
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * @param mixed $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * @return mixed
     */
    public function getOptin()
    {
        return $this->optin;
    }

    /**
     * @param mixed $optin
     */
    public function setOptin($optin)
    {
        $this->optin = $optin;
    }

    /**
     * @return mixed
     */
    public function getOptinSms()
    {
        return $this->optinSms;
    }

    /**
     * @param mixed $optinSms
     */
    public function setOptinSms($optinSms)
    {
        $this->optinSms = $optinSms;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
        $this->updatePasswordHash();
    }

    /**
     * @return mixed
     */
    public function getPassword2()
    {
        return $this->password2;
    }

    /**
     * @param mixed $password2
     */
    public function setPassword2($password2)
    {
        $this->password2 = $password2;
        $this->updatePasswordHash();
    }

    /**
     * @return mixed
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * @param mixed $student
     */
    public function setStudent($student)
    {
        $this->student = $student;
    }

    /**
     * @return mixed
     */
    public function getStudentInst()
    {
        return $this->studentInst;
    }

    /**
     * @param mixed $studentInst
     */
    public function setStudentInst($studentInst)
    {
        $this->studentInst = $studentInst;
    }

    /**
     * @return mixed
     */
    public function getGraduation()
    {
        return $this->graduation;
    }

    /**
     * @param mixed $graduation
     */
    public function setGraduation($graduation)
    {
        $this->graduation = $graduation;
    }

    /**
     * @return array
     */
    public function getCompanyDefinedFieldmap()
    {
        return $this->companyDefinedFieldmap;
    }

    /**
     * @param array $companyDefinedFieldmap
     */
    public function setCompanyDefinedFieldmap($companyDefinedFieldmap)
    {
        $this->companyDefinedFieldmap = $companyDefinedFieldmap;
    }

    public function setData($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

}