<?php

namespace AppBundle\Api\NcrRadiant\Result;


class VanityDate
{

    /**
     * @var string
     */
    public $date;

    /**
     * @var string
     */

    public $locale = 'en_GB';

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return VanityDate
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return VanityDate
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }




}