<?php

namespace AppBundle\Api\NcrRadiant;

class NcrRadiantConfig
{
    /**
     * @var string The endpoint, typically 'staging' or 'prod'
     */
    private $endpoint;

    /**
     * @var array The array of endpoint config, defined in parameters.yml
     */

    private $config;

    public function __construct($endpoint, $config)
    {
        $this->endpoint = $endpoint;
        $this->config = $config;
    }

    /**
     * Get a WSSE security value, where $field is one of 'username' or 'password'
     *
     * @param string $field
     * @return string
     */

    public function getWsseConfig($field)
    {
        return $this->config['wsse'][$field];
    }

    /**
     * Get an endpoint value, where $field is one of 'company_id', 'user_id' or 'password'
     *
     * @param string $field
     * @return string
     */

    public function getEndpointConfig($field)
    {
        return $this->config['endpoints'][$this->endpoint][$field];
    }

    /**
     * Get the currently active endpoint
     * @return string
     */

    public function getEndpoint()
    {
        return $this->endpoint;
    }

}