<?php

namespace AppBundle\Api\NcrRadiant\Request;

class BaseRequest
{

    /**
     * @var null|string
     */
    protected $soapMethod = null;

    /**
     * Leave null to use {$soapMethod}Request
     * @var null
     */

    protected $wrappedRequestPropertyName = null;

    /**
     * @var string
     */
    public $companyID;

    /**
     * @var string
     */
    public $userID;

    /**
     * @var string
     */

    public $password;

    /**
     * @var string
     */

    public $requestTime;

    /**
     * @var bool
     */

    public $stage = true;

    function __construct() {
        $this->requestTime = gmdate('Y-m-d\TH:i:s') . 'Z';
    }

    /**
     * @return string
     */
    public function getCompanyID()
    {
        return $this->companyID;
    }

    /**
     * @param string $companyID
     */
    public function setCompanyID($companyID)
    {
        $this->companyID = $companyID;
    }

    /**
     * @return string
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @param string $userID
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getReqDate()
    {
        return $this->reqDate;
    }

    /**
     * @param string $reqDate
     */
    public function setReqDate($reqDate)
    {
        $this->reqDate = $reqDate;
    }

    /**
     * @return string
     */
    public function getRequestTime()
    {
        return $this->requestTime;
    }

    /**
     * @param string $requestTime
     */
    public function setRequestTime($requestTime)
    {
        $this->requestTime = $requestTime;
    }

    /**
     * @return boolean
     */
    public function isStage()
    {
        return $this->stage;
    }

    /**
     * @param boolean $stage
     */
    public function setStage($stage)
    {
        $this->stage = $stage;
    }

    /**
     * @return null|string
     */
    public function getSoapMethod()
    {
        if (null === $this->soapMethod) {
            throw new \Exception("Please specify the soapMethod attribute in class " . static::class);
        }
        return $this->soapMethod;
    }

    /**
     * All calls are wrapped in an object with a single variable representing the request data
     * but the API is very inconsistent with naming standards. Mostly the method matches the Soap Method
     * exactly, but sometimes the first letter is uppercase.
     * In such exceptions this method should be overridden
     *
     * @return string
     * @throws \Exception
     */

    public function getWrappedRequestPropertyName()
    {
        if (null !== $this->wrappedRequestPropertyName) {
            return $this->wrappedRequestPropertyName;
        }

        return $this->getSoapMethod() . 'Request';
    }

}
