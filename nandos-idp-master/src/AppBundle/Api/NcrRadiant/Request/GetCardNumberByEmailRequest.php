<?php

namespace AppBundle\Api\NcrRadiant\Request;


class GetCardNumberByEmailRequest extends BaseRequest
{

    protected $soapMethod = 'GetCardNumberByEmail';

    /**
     * @var string
     */

    public $emailAddress;

    /**
     * @var string "ACTIVE", "BOTH", "INACTIVE"
     *
     */

    public $status = self::STATUS_BOTH;

    const STATUS_ACTIVE   = 'ACTIVE';
    const STATUS_BOTH     = 'BOTH';
    const STATUS_INACTIVE = 'INACTIVE';

    /**
     * @param string $status
     * @return GetCardNumberByEmailRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param string $emailAddress
     * @return GetCardNumberByEmailRequest
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
        return $this;
    }

}