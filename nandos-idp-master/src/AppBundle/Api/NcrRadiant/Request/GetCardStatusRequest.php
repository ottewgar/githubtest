<?php

namespace AppBundle\Api\NcrRadiant\Request;


class GetCardStatusRequest extends BaseRequest
{

    protected $soapMethod = 'getCardStatus';

    /**
     * @var string
     */

    public $cardNumber;

    /**
     * @param string $cardNumber
     * @return GetCardStatusRequest
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
        return $this;
    }



}