<?php

namespace AppBundle\Api\NcrRadiant\Request;

use Symfony\Component\Validator\Constraints as Assert;

class GetBonusPlanHistoryRequest extends BaseRequest
{

    protected $soapMethod = 'getBonusPlanHistory';

    /**
     * Doesn't confirm to the {$soapMethod}Request standard so is overridden
     * @var string
     */
    protected $wrappedRequestPropertyName = 'GetBonusPlanHistoryRequest';

    /**
     * @var string
     */

    public $cardNumber;

    /**
     * @var string
     */

    public $endDate;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="Please supply an integer"
     * )
     * @var int
     */

    public $numberOfAssignments;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="Please supply an integer"
     * )
     * @var int
     */

    public $numberOfDays;

    /**
     * @var string
     */

    public $startDate;

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     * @return GetBonusPlanHistoryRequest
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param string $endDate
     * @return GetBonusPlanHistoryRequest
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfAssignments()
    {
        return $this->numberOfAssignments;
    }

    /**
     * @param int $numberOfAssignments
     * @return GetBonusPlanHistoryRequest
     */
    public function setNumberOfAssignments($numberOfAssignments)
    {
        $this->numberOfAssignments = $numberOfAssignments;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfDays()
    {
        return $this->numberOfDays;
    }

    /**
     * @param int $numberOfDays
     * @return GetBonusPlanHistoryRequest
     */
    public function setNumberOfDays($numberOfDays)
    {
        $this->numberOfDays = $numberOfDays;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return GetBonusPlanHistoryRequest
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

}