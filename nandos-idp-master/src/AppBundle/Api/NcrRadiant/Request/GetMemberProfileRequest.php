<?php

namespace AppBundle\Api\NcrRadiant\Request;


class GetMemberProfileRequest extends BaseRequest
{

    protected $soapMethod = 'getMemberProfile';

    /**
     * @var string
     */

    public $cardNumber;

    /**
     * @param string $cardNumber
     * @return GetCardStatusRequest
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
        return $this;
    }



}