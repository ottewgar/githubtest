<?php

namespace AppBundle\Api\NcrRadiant\Request;


use AppBundle\Api\NcrRadiant\Result\MemberProfile;

class AddMemberProfileRequest extends BaseRequest
{

    protected $soapMethod = 'addMemberProfile';

    /**
     * @var MemberProfile
     */

    public $profile;

    /**
     * @param MemberProfile $profile
     * @return AddMemberProfileRequest
     */
    public function setProfile($profile)
    {

        // prepare profile for Ncr Radiant
        $this->profile = $profile->getNcrConvertedProfile();
        return $this;
    }



}