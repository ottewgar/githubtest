<?php

namespace AppBundle\Api\NcrRadiant\Request;

class EmailExistsRequest extends BaseRequest {

    protected $soapMethod = 'eMailExists';

    /**
     * Note the spelling. The case has to match exactly
     *
     * @var string
     */
    public $eMailAddress;

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->eMailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress($emailAddress)
    {
        $this->eMailAddress = $emailAddress;
    }


}

