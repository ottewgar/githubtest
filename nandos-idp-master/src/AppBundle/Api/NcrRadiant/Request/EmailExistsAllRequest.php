<?php

namespace AppBundle\Api\NcrRadiant\Request;

class EmailExistsAllRequest extends BaseRequest {

    protected $soapMethod = 'eMailExistsAll';

    /**
     * Note the spelling. The case has to match exactly
     *
     * @var string
     */
    public $EMailAddress;

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->EMailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress($emailAddress)
    {
        $this->EMailAddress = $emailAddress;
    }


}

