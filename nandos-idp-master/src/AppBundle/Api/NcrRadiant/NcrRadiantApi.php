<?php

namespace AppBundle\Api\NcrRadiant;

use AppBundle\Api\NcrRadiant\Request\BaseRequest;
use AppBundle\Api\NcrRadiant\Request\EmailExistsRequest;
use AppBundle\Api\NcrRadiant\Request\EmailExistsAllRequest;
use AppBundle\Api\NcrRadiant\Request\GetCardNumberByEmailRequest;
use AppBundle\Api\NcrRadiant\Request\GetCardStatusRequest;
use AppBundle\Api\NcrRadiant\Response\BaseResponse;
use AppBundle\Api\NcrRadiant\Result\BaseResult;
use AppBundle\Api\NcrRadiant\Result\GetCardStatusResult;
use AppBundle\Api\NcrRadiant\Result\MemberProfile;

/**
 *
 * Higher level methods, should not contain any direct SOAP calls
 */

class NcrRadiantApi extends BaseNcrRadiantApi
{
    /**
     * Cache within same request as it involves several API calls
     *
     * @var array
     */
    private $profileByEmail = [];


    /**
     * Determine if the card number is active or lost
     * Will mostly only make one API call (when card is active)
     *
     * @param $cardNumber
     * @return bool
     */
    public function isCardActiveOrLost($cardNumber)
    {
        return $this->iscardActive($cardNumber) || $this->isCardLost($cardNumber);
    }

    /**
     * @param $cardNumber
     * @return bool
     */

    public function isCardActive($cardNumber)
    {
        $cardStatus = $this->getCardStatus($cardNumber);

        return $cardStatus->isAccountActive();

    }

    /**
     * TODO: Likely an API call to the main Nando's site
     *
     * @param $cardNumber
     * @return bool
     */

    public function isCardLost($cardNumber)
    {

        return false;

//        function ml_hl_nandoscard_marked_as_lost($card_number = FALSE) {
//            module_load_include('php', 'nandoscard_lost', 'inc/nandoscard_lost.db');
//            $out = FALSE;
//
//            if ($card_number) {
//                $db_result = nandoscard_lost_db_get_lost_card($card_number);
//                if (!empty($db_result)) {
//                    $out = TRUE;
//                }
//            }
//
//            return $out;
//        }
    }

    /**
     * make it return a profile object, with exists or not, and error code?
     *
     * Adapted from ml_hl_get_profile_by_email in Nando's lib\mlWrapper.php
     * @param $emailAddress
     * @return false|MemberProfile
     */

    public function getProfileByEmail($emailAddress)
    {

        $emailAddress = strtolower(trim($emailAddress));

        if (!array_key_exists($emailAddress, $this->profileByEmail)) {

            // default to false
            $this->profileByEmail[$emailAddress] = false;

            //        $out = array(
            //            "executionStatus" => 500,
            //            "executionStatusDescription" => "Service unavailable or no nandoscards with this email account."
            //        );

            $emailAddressExists = $this->getEmailExistsAll($emailAddress);

            if (!$emailAddressExists) {

                //            $out = array(
                //                "executionStatus" => 999,
                //                "executionStatusDescription" => "Email doesn't exist"
                //            );
                //
                //            //should be impossible but ncr can do the impossible
                //            $card_number = $this->ml_hl_get_nandoscard_number_by_email($emailAddress);
                //            if ($card_number) {
                //                // ok so we have a card number from an account that doesn't exist..
                //                //thats'd be an ncr error then.
                //                $out = array(
                //                    "executionStatus" => 314,
                //                    "executionStatusDescription" => "Ncr error. Cardnumber from an account that doesn't exist"
                //                );
                //            }

            } else {

                $cardNumber = $this->getNandosCardNumberByEmail($emailAddress);

                $this->profileByEmail[$emailAddress] = $this->getMemberProfile($cardNumber);

            }

        }

        return $this->profileByEmail[$emailAddress];

    }

    /**
     * Return the most appropriate card number (users should have ony one per email address but certain conditions
     * mean they may have older deactivated or lost ones)
     *
     * Adapted from ml_hl_get_nandoscard_number_by_email() in Nando's lib\mlWrapper.php
     * @param $emailAddress
     */

    public function getNandosCardNumberByEmail($emailAddress)
    {

        $result = false;

        // get list of card numbers associated with this email address
        $cardNumbers = $this->getCardNumberByEmail($emailAddress);

        if ($cardNumbers) {

            //make result an array to deal with single card number result
            if (!is_array($cardNumbers)) {
                $cardNumbers = [$cardNumbers];
            }

            // loop through all cards stored against the same email address
            foreach ($cardNumbers as $cardNumber) {

                if ($this->isCardActiveOrLost($cardNumber)) {

                    // get profile
                    $memberProfile = $this->getMemberProfile($cardNumber);

                    // Is it a Nando's card or another type of card?
                    // Do we need to make this call? (can we assume the member profile card number is the same
                    // as the one passed to getMemberProfile?) Maybe we need to call it as it checks getMemberProfile
                    // is a valid response?
                    if ($memberProfile && $this->isNandosCardNumber($cardNumber = $memberProfile->getCardNumber())) {
                        return $cardNumber;
                    }

                }
            }
        }

        return false;

    }

    /**
     * Does the card number begin with 18329?
     * @param $cardNumber
     * @return bool
     */
    public function isNandosCardNumber($cardNumber)
    {
        $cardNumber = trim($cardNumber);
        return '18329' == substr($cardNumber, 0, 5);
    }

}