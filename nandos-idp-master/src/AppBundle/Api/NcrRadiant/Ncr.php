<?php

namespace AppBundle\Api\NcrRadiant;

/**
 *  NCR constants and utilities
 */

class Ncr
{

    const ACCOUNT_STATUS_ACTIVE     = 'active';
    const ACCOUNT_STATUS_NONE       = 'none';
    const ACCOUNT_STATUS_LOST_CARD  = 'lost_card';
    const ACCOUNT_STATUS_CLOSED     = 'closed';

}