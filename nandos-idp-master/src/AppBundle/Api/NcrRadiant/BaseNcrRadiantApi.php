<?php

namespace AppBundle\Api\NcrRadiant;

use AppBundle\Api\NcrRadiant\Request\AddMemberProfileRequest;
use AppBundle\Api\NcrRadiant\Request\BaseRequest;
use AppBundle\Api\NcrRadiant\Request\EmailExistsRequest;
use AppBundle\Api\NcrRadiant\Request\EmailExistsAllRequest;
use AppBundle\Api\NcrRadiant\Request\GetBonusPlanHistoryRequest;
use AppBundle\Api\NcrRadiant\Request\GetCardNumberByEmailRequest;
use AppBundle\Api\NcrRadiant\Request\GetCardStatusRequest;
use AppBundle\Api\NcrRadiant\Request\GetMemberProfileRequest;
use AppBundle\Api\NcrRadiant\Response\BaseResponse;
use AppBundle\Api\NcrRadiant\Result\BaseResult;
use AppBundle\Api\NcrRadiant\Result\GetCardStatusResult;
use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Log\Traits\LoggerTrait;

/**
 * All SOAP calls are set up here. These can be called directly if defined as public.
 * More complex logic involving several API calls should be placed in NcrRadianApi
 */

class BaseNcrRadiantApi
{
    use LoggerTrait;

    /**
     * @var \SoapClient
     */

    private $client;

    /**
     * @var string
     */

    private $companyId;

    /**
     * @var string
     */

    private $userId;

    /**
     * @var
     */

    private $password;

    private $requestCache = [];

    final function __construct(\SoapClient $client, $companyId, $userId, $password)
    {
        $this->client    = $client;
        $this->companyId = $companyId;
        $this->userId    = $userId;
        $this->password  = $password;

    }

    /**
     * Checks if email exists in ACTIVE accounts only
     *
     * @param eMailExists $parameters
     * @return bool
     */
    public function getEmailExists($emailAddress)
    {
        $emailAddress = strtolower(trim($emailAddress));

        $request = new EmailExistsRequest();
        $request->setEmailAddress($emailAddress);

        return $this->makeSoapCall($request);

    }

     /**
     * Checks if email exists in both active & inactive accounts
     *
     * @param eMailExists $parameters
     * @return bool
     */
    public function getEmailExistsAll($emailAddress)
    {

        $emailAddress = strtolower(trim($emailAddress));

        $request = new EmailExistsAllRequest();
        $request->setEmailAddress($emailAddress);

        return $this->makeSoapCall($request);

    }

    /**
     * @param $emailAddress
     * @param string $status
     * @return string|array
     * @throws \Exception
     */

    protected function getCardNumberByEmail($emailAddress, $status = GetCardNumberByEmailRequest::STATUS_BOTH)
    {

        $emailAddress = strtolower(trim($emailAddress));

        $request = new GetCardNumberByEmailRequest();
        $request
            ->setEmailAddress($emailAddress)
            ->setStatus($status)
        ;

        return $this->makeSoapCall($request);

    }

    /**
     * @param $cardNumber
     * @return GetCardStatusResult
     * @throws \Exception
     */

    public function getCardStatus($cardNumber)
    {
        $cardNumber = trim($cardNumber);

        $request = new GetCardStatusRequest();
        $request->setCardNumber($cardNumber);

        return $this->makeSoapCall($request);

    }

    /**
     * Return the members profile object
     *
     * @param $card_number
     * @return MemberProfile
     */
    public function getMemberProfile($cardNumber) {

        $cardNumber = trim($cardNumber);

        // cache as we call it more than once in the same request
        if (!isset($this->requestCache['getMemberProfile'][$cardNumber])) {


            $this->info("Get member profile called - member not available in cache");

            //build request
            $request = new GetMemberProfileRequest();
            $request->setCardNumber($cardNumber);

            $this->requestCache['getMemberProfile'][$cardNumber] = $this->makeSoapCall($request);
        }

        return $this->requestCache['getMemberProfile'][$cardNumber];

    }

    public function addMemberProfile(MemberProfile $profile)
    {

        $request = new AddMemberProfileRequest();
        $request->setProfile($profile);

        return $this->makeSoapCall($request);
    }

    /**
     * @param $cardNumber
     * @param null|string $startDate
     * @param null|string $endDate
     * @param null|int $numberOfAssignments
     * @param null|int $numberOfDays
     * @return BaseResult|bool|mixed
     * @throws \Exception
     */
    public function getBonusPlanHistory($cardNumber, $startDate = null, $endDate = null, $numberOfAssignments = null, $numberOfDays = null)
    {
        $cardNumber = trim($cardNumber);

        $request = new GetBonusPlanHistoryRequest();
        $request
            ->setCardNumber($cardNumber)
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setNumberOfAssignments($numberOfAssignments)
            ->setNumberOfDays($numberOfDays);

        return $this->makeSoapCall($request);

    }

    /**
     * @param GetBonusPlanHistoryRequest $soapRequest
     * @return BaseResult|bool|mixed
     * @throws \Exception
     */
    public function getBonusPlanHistoryFromGetBonusPlanHistoryRequest(GetBonusPlanHistoryRequest $soapRequest)
    {
        return $this->makeSoapCall($soapRequest);
    }

    /**
     * @param $request
     * @param null $method
     * @return BaseResult|bool|mixed
     */
    private function makeSoapCall(BaseRequest $request)
    {

        $method = $request->getSoapMethod();

        // Decorate request with credentials if appropriate

        if ($request instanceof BaseRequest) {
            $request->setCompanyID($this->companyId);
            $request->setUserID($this->userId);
            $request->setPassword($this->password);
        }

        $wrappedRequest = new \stdClass();

        // all calls are wrapped in an object with a single variable representing the request data
        // but the API is very inconsistent with naming standards. Mostly the method matches the Soap Method
        // exactly, but sometimes the first letter is uppercase

        $requestVariable = $request->getWrappedRequestPropertyName();

        $wrappedRequest->{$requestVariable} = $request;

        $this->info("NCR MakeSoapCall to method: $method");

        $response = $this->client->{$method}($wrappedRequest);

        /**
         * If we are have class mapped our own Response class, it knows how to get the result.
         * Otherwise, see if the response contains one of our own Result classes, and assume the first of them is
         * the intended result.
         *
         * The result may even be scalar, an array or object, depending on the type of call
         *
         */

        if ($response instanceof BaseResponse) {
            $returnValue = $response->getReturnValue();
        } else {
            $resultFound = false;
            foreach ((array)$response as $responseAttribute) {
                if (is_object($responseAttribute) && $responseAttribute instanceof BaseResult) {
                    $returnValue = $responseAttribute->getReturnValue();
                    $resultFound = true;
                    break;
                }
            }

            // unable to find a BaseResponse or BaseResult
            if (!$resultFound) {
                throw new \Exception("Unable to determine a BaseResult or BaseResponse class for SOAP call '{$method}'. Check classmap and spelling in " . static::class);
            }

        }

        return $returnValue;

    }


}