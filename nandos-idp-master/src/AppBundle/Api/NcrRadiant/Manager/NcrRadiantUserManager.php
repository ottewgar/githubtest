<?php

namespace AppBundle\Api\NcrRadiant\Manager;

use AppBundle\Api\CardManagerInterface;
use AppBundle\Api\Manager\AbstractUserManager;
use AppBundle\Api\NcrRadiant\Ncr;
use AppBundle\Api\NcrRadiant\NcrRadiantApi;
use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Util\Sanitizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NcrRadiantUserManager extends AbstractUserManager implements CardManagerInterface
{
    protected $cardsystem = "NCR Radiant";
    /**
     * @var NcrRadiantApi
     */
    protected $api;

    /**
     * @var UserManager
     */

    protected $userManager;

    /**
     * NcrRadiantUserManager
     *
     * @param $ncrRadiantApi
     * @param $userManager
     */
    public function __construct(NcrRadiantApi $ncrRadiantApi, UserManager $userManager)
    {
        $this->api         = $ncrRadiantApi;
        $this->userManager = $userManager;
    }

    /**
     * Do they have an NCR account, what state is it in?
     */
    public function getNcrAccountStatus($emailAddress)
    {

        $profile = $this->api->getProfileByEmail($emailAddress);

        $sanitizedEmail = Sanitizer::sanitiseEmail($emailAddress);

        // executionStatus 314 - do we catch exception and say account status unknown?

        if (!$profile) {

            $this->info("Get account status for user {$sanitizedEmail} from card system {$this->cardsystem} shows no account");
            return Ncr::ACCOUNT_STATUS_NONE;

            // Ally: redo to look for inactive profile (Nando's card) and then return status of inactive
            // Do we need to consider the lost cards too? When a card is lost is the account marked as inactive?

//            $out["executionStatus"] = 999;
//            $out["executionStatusDescription"] = "Email doesn't exist";

            //
            //check card active status from radiant
            // BROKEN IN NANDOS END  - this won't return inactive cards
            // $any_card_number = $this->ml_hl_get_nandoscard_number_by_email($email, TRUE);

            //active?
            // NEED TO CONFIRM
            // $awaiting_activation = $this->ml_hl_nandoscard_email_awaiting_activation($email);
//
//            if ($awaiting_activation) { //account is awaiting email confirmation
//                $out["executionStatus"] = 997;
//                $out["executionStatusDescription"] = "Account not active, please check your email.";
//            } else {

            // WE HAVE NO CARD NUMBER (is optional parameter in ml_hl_validate)
            // $card_lost = $this->api->isCardLost($cardnumber);

            //
            //enable for lost card check
//                if ($card_lost) {
//                    $out["executionStatus"] = 1057;
//                    $out["executionStatusDescription"] = "Card marked as lost";
//                } else {
//                    if (!empty($any_card_number)) {
//                        $out["executionStatus"] = 996;
//                        $out["executionStatusDescription"] = "Account closed";
//                    }
//                }
//        }

//        }

        } else {

            // account is active (or LOST - TBC)
            $this->info("Get account status for user {$sanitizedEmail} from card system {$this->cardsystem} shows account active");
            return Ncr::ACCOUNT_STATUS_ACTIVE;

        }

    }

    /**
     * Return an ACTIVE account that has a Nando's card
     * TODO Rename this method and update usages
     *
     * @param $email
     * @return MemberProfile|false
     */

    public function getProfileByEmail($email)
    {
        return $this->api->getProfileByEmail($email);
    }


    public function createUserFromProfile(MemberProfile $profile, $password = null)
    {
        $user = new User();

        $user->setFirstname($profile->getFirstName());
        $user->setLastname($profile->getLastName());
        $user->setEmail($profile->getEmailAddress());
//        $user->setNandosCardNumber($profile->getCardNumber());

        $sanitisedEmail = Sanitizer::sanitiseEmail($profile->getEmailAddress());
        $this->info("create {$this->cardsystem} user from profile {$sanitisedEmail}");
        // add title? (need to add to our DB)

        if (null !== $password) {
            $user->setPlainPassword($password);
        }

        return $user;

    }

    /**
     *
     * Does the password match that stored in the NCR Member profile?
     * If the password matches, and was an old md5 password, update it in NCR
     *
     * @param string $password An unencrypted password
     * @param MemberProfile $profile
     * @param bool $updateLegacyPassword
     * @return bool
     */
    public function passwordMatchesProfilePassword(MemberProfile $profile, $password, $updateLegacyPassword = true)
    {
        $password           = trim($password);
        $ncrPasswordHash    = $profile->getPasswordHash();

        // make sure neither password is blank
        if (!(strlen($password) && strlen($ncrPasswordHash))) {
           return false;
        }

        // lets see if it is a legacy match
        if (md5($password) == $ncrPasswordHash) {

            $sanitizedEmail = Sanitizer::sanitiseEmail($profile->getEmailAddress());
            $this->notice("Password matches MD5 for user {$sanitizedEmail} in {$this->cardsystem}");

            if ($updateLegacyPassword) {
                $this->updateProfilePassword($profile, $password, true);
            }

            // re-validate? (need to bust request cache and re-fetch or will cause infinite loop...)

            return true;

        } else {
            return password_verify($password, $ncrPasswordHash);
        }

    }

    /**
     * We can perhaps optimize to use less API calls in NCR
     *
     * @param $email
     * @param $password
     */
    public function getAuthenticatedUserAndStoreLocally($email, $password)
    {
        $sanitizedEmail = Sanitizer::sanitiseEmail($email);
        $this->info("Check for authenticated user in {$this->cardsystem} for user {$sanitizedEmail}");

        $status = $this->getNcrAccountStatus($email);

        if ($status == Ncr::ACCOUNT_STATUS_ACTIVE) {

            $this->info("User {$sanitizedEmail} found in {$this->cardsystem}");
            $profile = $this->getProfileByEmail($email);

            if ($profile && $this->passwordMatchesProfilePassword($profile, $password)) {

                $this->info("User {$sanitizedEmail} found in {$this->cardsystem} and password matches so will create in IDP");

                $user = $this->userManager->findUserByEmail($email);

                if (!$user) {
                    $user = $this->createEnabledUserFromProfile(User::REGISTER_SOURCE_NCR_RADIANT, $profile, $password);
                } else {
                    $user->setPlainPassword($password);
                }

                $this->userManager->updateUser($user);

                return $user;
            }
        }

        return null;
    }

    /**
     * We can perhaps optimize to use less API calls in NCR
     *
     * @param $email
     * @param $password
     */
    public function getUserAndStoreLocally($email)
    {
        $sanitizedEmail = Sanitizer::sanitiseEmail($email);
        $this->info("Check for user in {$this->cardsystem} for user {$sanitizedEmail} without checking authentication");

        $userProfile = $this->getProfileByEmail($email);

        if ($userProfile) {
            $this->info("User {$sanitizedEmail} found in {$this->cardsystem} so will create in IDP");

            // create a local user so we can attach a password reset token to it
            $user = $this->createEnabledUserFromProfile(User::REGISTER_SOURCE_NCR_RADIANT, $userProfile);

            return $user;

        } else {
            return false;
        }
    }

    /**
     * Change a users password in NCR
     *
     * @param MemberProfile $profile
     * @param $password
     * @return bool
     */
    public function updateProfilePassword($profile, $password, $hashed)
    {
        if (!$profile instanceof MemberProfile) {
            $profile = $this->getProfileByEmail($profile);
        }
        if (!$profile instanceof MemberProfile) {
            return false;
        }

        if (!strlen($password = trim($password))) {
            return false;
        }

        if (!$hashed) {
            $password = $this->hashPassword($password);
        }

        $sanitizedEmail = Sanitizer::sanitiseEmail($profile->getEmailAddress());
        $this->info("Updating password for user {$sanitizedEmail} and card system {$this->cardsystem}");
        $profile->updatePassword($password);

        //save new profile to NCR
        $response = $this->api->addMemberProfile($profile);

        return true;

    }

    public function addMember($profile)
    {
        $this->api->addMemberProfile($profile);
    }

    /**
     * To ensure any changes to card numbers in NCR are captured, we always check and update the local copy on
     * any API query to get user details
     *
     * @param User $user
     */
    public function refreshNandosCardNumberFromNcr(User $user)
    {
        if ($nandosCardNumber = $this->api->getNandosCardNumberByEmail($user->getEmail())) {
            if ($nandosCardNumber != $user->getNandosCardNumber()) {
                $user->setNandosCardNumber($nandosCardNumber);
                $this->userManager->updateUser($user);
            }
        }
    }

}