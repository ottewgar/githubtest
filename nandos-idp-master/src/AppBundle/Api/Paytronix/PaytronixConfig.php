<?php

namespace AppBundle\Api\Paytronix;

class PaytronixConfig
{
    /**
     * @var string The endpoint, typically 'staging' or 'prod'
     */
    private $endpoint;

    /**
     * @var array The array of endpoint config, defined in parameters.yml
     */

    private $config;

    /**
     * PaytronixConfig constructor.
     * @param $endpoint
     * @param $config
     */
    public function __construct($endpoint, $config)
    {

        $this->endpoint = $endpoint;
        $this->config = $config;
    }

    /**
     * Get an endpoint value, where $field is one of 'username' or 'password'
     *
     * @param string $field
     * @return string
     */

    public function getEndpointConfig($field)
    {
        return $this->config['endpoints'][$this->endpoint][$field];
    }

    /**
     * Get the currently active endpoint
     * @return string
     */

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return mixed
     */
    public function getPasswordField()
    {
        return $this->config['password_field'];
    }

}