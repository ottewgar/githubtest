<?php

namespace AppBundle\Api\Paytronix\Manager;

use AppBundle\Api\CardManagerInterface;
use AppBundle\Api\Manager\AbstractUserManager;
use AppBundle\Api\NcrRadiant\Ncr;
use AppBundle\Api\NcrRadiant\NcrRadiantApi;
use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Api\Paytronix\Paytronix;
use AppBundle\Api\Paytronix\PaytronixApi;
use AppBundle\Api\Paytronix\PaytronixConfig;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Util\Sanitizer;

class PaytronixUserManager extends AbstractUserManager implements CardManagerInterface
{
    protected $cardsystem = "Paytronix";

    /**
     * @var PaytronixUserManager
     */
    protected $api;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var PaytronixConfig
     */
    protected $config;

    /**
     * NcrRadiantUserManager
     *
     * @param $ncrRadiantApi
     * @param $userManager
     */
    public function __construct(PaytronixApi $paytronixApi, UserManager $userManager, PaytronixConfig $config)
    {
        $this->api          = $paytronixApi;
        $this->userManager  = $userManager;
        $this->config       = $config;
    }

    /**
     * Do they have a Paytronix account, what state is it in?
     */
    public function getPaytronixAccountStatus($emailAddress)
    {

        $sanitisedEmail = Sanitizer::sanitiseEmail($emailAddress);
        $profile = $this->api->getUserInformation($emailAddress);

        if ($profile['result'] !== "success") {

            $this->info("Check account status for use {$sanitisedEmail} in card system {$this->cardsystem} is not active");
            return Paytronix::ACCOUNT_STATUS_NONE;

        } else {

            $this->info("Check account status for use {$sanitisedEmail} in card system {$this->cardsystem} is active");
            return Paytronix::ACCOUNT_STATUS_ACTIVE;
        }
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getPaytronixProfile($username)
    {
        return $this->api->getUserInformation($username);
    }

    /**
     * @param $profile
     * @param null $password
     * @return User
     */
    public function createUserFromProfile($profile, $password = null)
    {
        $data = $profile['fields'];

        $user = new User();

        $user->setFirstname($data['firstName']);
        $user->setLastname($data['lastName']);
        $user->setEmail($data['username']);

        $sanitisedEmail = Sanitizer::sanitiseEmail($data['username']);
        $this->info("create user {$sanitisedEmail} from profile in card system {$this->cardsystem}");

        // add title? (need to add to our DB)

        if (null !== $password) {
            $user->setPlainPassword($password);
        }

        return $user;

    }

    /**
     *
     * Does the password match that stored in the Paytronix Member profile?
     *
     * @param string $password An unencrypted password
     * @param $username
     * @param bool $updateLegacyPassword
     * @return bool
     */
    public function passwordMatchesProfilePassword($profile, $password, $updateLegacyPassword = true)
    {
        $password = trim($password);

        if ($this->config->getPasswordField() === "password") {
            $this->api->login($profile['fields']['username'], $password);

            return true;
        }

        $passwordHash = $profile['fields'][$this->config->getPasswordField()];

        // make sure neither password is blank
        if (!(strlen($password) && strlen($passwordHash))) {
            return false;
        }

        // lets see if it is a legacy match
        if (md5($password) == $passwordHash) {

            $sanitizedEmail = Sanitizer::sanitiseEmail($profile['fields']['username']);
            $this->notice("Password matches MD5 for user {$sanitizedEmail} in {$this->cardsystem}");

            if ($updateLegacyPassword) {

                $this->updateProfilePassword($profile['fields']['username'], $this->hashPassword($password), true);
            }

            // re-validate? (need to bust request cache and re-fetch or will cause infinite loop...)

            return true;

        } else {
            return password_verify($password, $passwordHash);
        }

        return false;

    }

    /**
     * Change a users password in Paytronix
     *
     * @param $username
     * @param $password
     * @return bool
     */
    public function updateProfilePassword($username, $password, $hashed)
    {
        if (!strlen($password = trim($password))) {
            return false;
        }

        if (!$hashed) {
            $password = $this->hashPassword($password);
        }

        $sanitizedEmail = Sanitizer::sanitiseEmail($username);
        $this->info("Update password for user {$sanitizedEmail} in card system {$this->cardsystem}");

        $response = $this->api->updatePassword($username, $password, $this->config->getPasswordField(), $hashed);

        return $response;
    }

    /**
     * @param $username
     * @param $newUsername
     * @return mixed
     */
    public function updateUsername($username, $newUsername)
    {
        return $this->api->updateUsername($username, $newUsername);
    }

    /**
     * @param $email
     * @param $password
     * @return User|null
     */
    public function getAuthenticatedUserAndStoreLocally($email, $password)
    {
        $sanitizedEmail = Sanitizer::sanitiseEmail($email);
        $this->info("Check for authenticated user {$sanitizedEmail} in card system {$this->cardsystem}");

        $status = $this->getPaytronixAccountStatus($email);

        if ($status == Paytronix::ACCOUNT_STATUS_ACTIVE) {

            $this->info("User {$sanitizedEmail} found in {$this->cardsystem}");
            $profile = $this->getPaytronixProfile($email);

            if ($profile && $this->passwordMatchesProfilePassword($profile, $password)) {

                $this->info("User {$sanitizedEmail} found in {$this->cardsystem} and password matches so will create in IDP");

                $user = $this->userManager->findUserByEmail($email);

                if (!$user) {
                    $user = $this->createEnabledUserFromProfile(User::REGISTER_SOURCE_PAYTRONIX, $profile, $password);
                } else {
                    $user->setPlainPassword($password);
                }

                $this->userManager->updateUser($user);

                return $user;
            }
        }

        return null;
    }

    /**
     * We can perhaps optimize to use less API calls in Paytronix
     *
     * @param $email
     * @param $password
     */
    public function getUserAndStoreLocally($email)
    {
        $sanitizedEmail = Sanitizer::sanitiseEmail($email);
        $this->info("Check for user {$sanitizedEmail} in card system {$this->cardsystem}");

        $userProfile = $this->getPaytronixProfile($email);

        if ($userProfile['result'] === "success") {
            $this->info("User {$sanitizedEmail} found in {$this->cardsystem} so will create in IDP");

            $user = $this->createEnabledUserFromProfile(User::REGISTER_SOURCE_PAYTRONIX, $userProfile);

            return $user;

        } else {
            return false;
        }
    }
}