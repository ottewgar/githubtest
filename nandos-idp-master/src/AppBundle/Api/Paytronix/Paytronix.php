<?php

namespace AppBundle\Api\Paytronix;

/**
 *  Paytronix constants and utilities
 */

class Paytronix
{

    const ACCOUNT_STATUS_ACTIVE     = 'active';
    const ACCOUNT_STATUS_NONE       = 'none';
    const ACCOUNT_STATUS_LOST_CARD  = 'lost_card';
    const ACCOUNT_STATUS_CLOSED     = 'closed';

}