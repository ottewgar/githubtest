<?php

namespace AppBundle\Api\Paytronix;

use AppBundle\Log\Traits\LoggerTrait;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;

class PaytronixApi
{
    use LoggerTrait;

    /**
     * @var
     */
    private $client;
    /**
     * @var
     */
    private $merchantId;

    /**
     * PaytronixApi constructor.
     * @param $client
     * @param $merchantId
     */
    public function __construct($client, $merchantId)
    {
        $this->client = $client;
        $this->merchantId = $merchantId;
    }

    /**
     * @param $path
     * @param $data
     * @return bool|mixed
     */
    protected function get($path, $data)
    {
        $data['merchantId'] = $this->merchantId;

        try {
            $this->info("Paytronix API Call GET method: {$path}");

            $response = $this->client->request('GET', $path, [
                'query' => $data,
            ]);
        } catch (ConnectException $e) {
            return false;
        }

        $this->info("Paytronix API Response Code: {$response->getStatusCode()}");

        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        } else {
            return false;
        }
    }

    /**
     * @param $path
     * @param $data
     * @return bool|mixed
     */
    protected function post($path, $data)
    {
        $data->merchantId = $this->merchantId;

        $encoded = json_encode($data);

        try {
            $this->info("Paytronix API Call POST method: {$path}");

            $response = $this->client->request('POST', $path, [
                'body' => $encoded,
            ]);
        } catch (ConnectException $e) {
            return false;
        }

        $this->info("Paytronix API Response Code: {$response->getStatusCode()}");

        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        } else {
            return false;
        }
    }

    /**
     * @param $username
     * @param $fields
     * @return bool|mixed
     */
    protected function editUser($username, $fields)
    {
        $setUserFields = [
            'style' => 'typed',
        ];

        $setUserFields = (object) array_merge($setUserFields, $fields);

        $data = (object)[
            'username' => $username,
            'setUserFields' => $setUserFields,
        ];

        return $this->post('/rest/15.20/enrollment/editUser.json', $data);
    }

    /**
     * @param $cardNumber
     * @return bool|mixed
     */
    public function getAccountInformation($cardNumber)
    {
        $data = [
            'printedCardNumber' => $cardNumber,
        ];

        return $this->get('/rest/15.20/guest/accountInformation.json', $data);
    }

    /**
     * @param $username
     * @return bool|mixed
     */
    public function getUserInformation($username)
    {
        $data = [
            'username' => $username,
        ];

        return $this->get('/rest/15.20/guest/userInformation.json', $data);
    }

    /**
     * @return bool|mixed
     */
    public function createAndRegister()
    {
        $setUserFields = (object)[
            'style' => 'typed',
            'optin' => true,
            'salutation' => ['Mr.'],
            'firstName' => ['Scott'],
            'lastName' => ['Pringle'],
            'companyName' => ['Quiz'],
            'email' => ['scottp99@gmail.com'],
            'address1' => ['aaa'],
            'address2' => ['bbb'],
            'city' => ['ddd'],
            'stateProvince' => ['MA'],
            'postalCode' => ['02116'],
            'country' => ['US'],
            'phone' => ['2342342333'],
            'fax' => ['2342342333'],
            'mobilePhone' => ['2342342333'],
            'dateOfBirth' => ['1985-01-01'],
            'anniversaryDate' => ['2015-01-01'],
            'passwordHintQuestion' => ['Mother\'s maiden name'],
            'passwordHintAnswer' => ['tyrie'],
            'custom1' => ['11'],
            'custom2' => ['22'],
            'custom3' => ['33'],
            'custom4' => ['44'],
            'custom5' => ['55'],
            'custom6' => ['66'],
            'username' => ['scottp99'],
            'password' => ['password'],
            'textCampaignOptIn' => true,
            'nickname' => ['ggg'],
            'avatarCode' => ['11'],
            'referrerEmail' => ['asdfdsfs@ASDfsdafsd.com'],
            'referralCode' => ['123'],

        ];

        $setAccountFields = (object)[
            "style" => "typed",
            "customerNumber" => ["123456"],
            "perksModifiedDate" => ["2012-05-01"],
            "externalAccounts" => [
                (object)[
                    "integration" => "xhDE_Ea4QhsfC8h3pUtHJnLZ9lRXYgysJVXnR4XNO0",
                    "accountCode" => "123456"
                ]
            ],
            "enrollDate" => ["2012-05-19"]
        ];

        $data = (object)[
            'enforceUniqueFields' => [],
            'cardTemplateCode' => '0',
            'activationStoreCode' => 'corp',
            'setUserFields' => $setUserFields,
            'setAccountFields' => $setAccountFields,
        ];

        return $this->post('/rest/15.20/enrollment/createAndRegister.json', $data);
    }

    /**
     * @return bool|mixed
     */
    public function register()
    {
        $setUserFields = (object)[
            'style' => 'typed',
            'optin' => true,
            'salutation' => ['Mr.'],
            'firstName' => ['Scott'],
            'lastName' => ['Pringle'],
            'companyName' => ['Quiz'],
            'email' => ['scottp99@gmail.com'],
            'address1' => ['aaa'],
            'address2' => ['bbb'],
            'city' => ['ddd'],
            'stateProvince' => ['MA'],
            'postalCode' => ['02116'],
            'country' => ['US'],
            'phone' => ['2342342333'],
            'fax' => ['2342342333'],
            'mobilePhone' => ['2342342333'],
            'dateOfBirth' => ['1985-01-01'],
            'anniversaryDate' => ['2015-01-01'],
            'passwordHintQuestion' => ['Mother\'s maiden name'],
            'passwordHintAnswer' => ['tyrie'],
            'custom1' => ['11'],
            'custom2' => ['22'],
            'custom3' => ['33'],
            'custom4' => ['44'],
            'custom5' => ['55'],
            'custom6' => ['66'],
            'username' => ['scottp99'],
            'password' => ['password'],
            'textCampaignOptIn' => true,
            'nickname' => ['ggg'],
            'avatarCode' => ['11'],
            'referrerEmail' => ['asdfdsfs@ASDfsdafsd.com'],
            'referralCode' => ['123'],

        ];

        $setAccountFields = (object)[
            "style" => "typed",
            "customerNumber" => ["123456"],
            "perksModifiedDate" => ["2012-05-01"],
            "externalAccounts" => [
                (object)[
                    "integration" => "xhDE_Ea4QhsfC8h3pUtHJnLZ9lRXYgysJVXnR4XNO0",
                    "accountCode" => "123456"
                ]
            ],
            "enrollDate" => ["2012-05-19"]
        ];

        $data = (object)[
            'enforceUniqueFields' => [],
            'cardTemplateCode' => '0',
            'activationStoreCode' => 'corp',
            'setUserFields' => $setUserFields,
            'setAccountFields' => $setAccountFields,
        ];

        return $this->post('/rest/15.20/enrollment/register.json', $data);
    }


    /**
     * @param $username
     * @param $password
     * @param string $passwordField
     * @return bool|mixed
     */
    public function updatePassword($username, $password, $passwordField = "custom4", $hashed = true)
    {
        $data = [
            $passwordField => [$password],
        ];

        return $this->editUser($username, $data);
    }

    /**
     * @param $username
     * @param $newUsername
     * @return bool|mixed
     */
    public function updateUsername($username, $newUsername)
    {
        $data = [
            'username' => [$newUsername],
        ];

        return $this->editUser($username, $data);
    }

    /**
     * @param $username
     * @param $password
     * @return bool|mixed
     */
    public function login($username, $password)
    {
        $data = [
            'authentication' => 'guest',
            'username' => $username,
            'password' => $password,
        ];

        try {
            return $this->get('/rest/15.20/guest/userInformation.json', $data);
        } catch (RequestException $e) {
            return false;
        }
    }

    /**
     * @param $emailAddress
     * @return bool
     */
    public function getEmailExistsAll($emailAddress)
    {
        $response = $this->getUserInformation($emailAddress);

        return ($response['result'] === "success") ? true: false;
    }

    /**
     * @return bool|mixed
     */
    public function getEnrollmentConfig()
    {
        $data = [
            'cardTemplateCode' => 0
        ];

        return $this->get('/rest/15.20/enrollment/enrollmentConfig.json', $data);
    }
}
