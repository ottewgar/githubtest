<?php

namespace AppBundle\Api;

class GeneralApiConfig
{
    private $env;

    /**
     * @var array The array of endpoint config, defined in parameters.yml
     */

    private $config;

    public function __construct($env, $config)
    {
        $this->env = $env;
        $this->config = $config;
    }

    public function getConfig($field)
    {
        return $this->config[$this->env][$field];
    }

}