<?php

namespace AppBundle\Api;

use AppBundle\Util\NandosCardConfig;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ApiService
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AccessTokenManagerInterface
     */
    private $manager;

    /**
     * @var $apiTestMode
     */
    private $apiTestMode;

    /**
     * ApiService constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param AccessTokenManagerInterface $manager
     * @param $apiTestMode
     */
    public function __construct(TokenStorageInterface $tokenStorage, AccessTokenManagerInterface $manager, NandosCardConfig $cardConfig)
    {
        $this->tokenStorage = $tokenStorage;
        $this->manager = $manager;
        $this->apiTestMode = $cardConfig->getApiTestMode();
    }

    /**
     * @param $controllerRoute
     * @return bool
     */
    public function checkIfApiRoute($controllerRoute)
    {
        $apiRoutes = [
//            'fos_oauth_server_token',
            'app_api_',
        ];

        foreach ($apiRoutes as $route) {

            if (strpos($controllerRoute, $route) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getApiTestMode()
    {
        return $this->apiTestMode;
    }

    /**
     * @return bool|null|\Symfony\Component\Security\Core\Authentication\Token\TokenInterface
     */
    public function getToken()
    {
        $token = $this->tokenStorage->getToken();

        if ($token instanceof AnonymousToken) {
            return false;
        } else {
            return $token;
        }
    }

    /**
     * @return \FOS\OAuthServerBundle\Model\TokenInterface|null
     */
    public function getAccessToken()
    {
        if (!$this->getToken()) {
            return false;
        } else {
            $accessToken = $this->manager->findTokenByToken(
                $this->getToken()->getToken()
            );

            return $accessToken;
        }

    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        if (!$this->getAccessToken()) {
            return false;
        } else {
            $client = $this->getAccessToken()->getClient();
            return $client;
        }

    }

    /**
     * @param $token
     */
    public function updateToken($token) {
        $this->manager->updateToken($token);
    }
}