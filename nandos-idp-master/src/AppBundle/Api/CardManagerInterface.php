<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 17/08/16
 * Time: 10:28
 */

namespace AppBundle\Api;


interface CardManagerInterface
{
    public function doesEmailExist($email);

    public function getUserAndStoreLocally($email);

    public function getAuthenticatedUserAndStoreLocally($email, $password);

    public function updateProfilePassword($email, $password, $hashed);
}