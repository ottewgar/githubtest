<?php

namespace AppBundle\Api\Manager;

use AppBundle\Api\NcrRadiant\Manager\NcrRadiantUserManager;
use AppBundle\Api\Paytronix\Manager\PaytronixUserManager;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Util\IdpSettings;
use AppBundle\Util\NandosCardConfig;
use Symfony\Component\HttpFoundation\Request;

class CardUserManagerContainer
{
    /**
     * @var PaytronixUserManager
     */
    private $paytronixUserManager;
    /**
     * @var NcrRadiantUserManager
     */
    private $ncrRadiantUserManager;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var NandosCardConfig
     */
    private $cardConfig;
    /**
     * @var mixed
     */
    private $paytronixEnabled;
    /**
     * @var mixed
     */
    private $ncrEnabled;

    private $enabledUserManagers;

    const PAYTRONIX_USER_MANAGER = 'paytronixUserManager';
    const NCR_RADIANT_USER_MANAGER = 'ncrRadiantUserManager';

    private $triedCardSystem = [];

    /**
     * CardUserManagerContainer constructor.
     * @param PaytronixUserManager $paytronixUserManager
     * @param NcrRadiantUserManager $ncrRadiantUserManager
     * @param UserManager $userManager
     * @param NandosCardConfig $cardConfig
     */
    public function __construct(PaytronixUserManager $paytronixUserManager, NcrRadiantUserManager $ncrRadiantUserManager, UserManager $userManager, NandosCardConfig $cardConfig)
    {
        $this->paytronixUserManager = $paytronixUserManager;
        $this->ncrRadiantUserManager = $ncrRadiantUserManager;
        $this->userManager = $userManager;
        $this->cardConfig = $cardConfig;
        $this->paytronixEnabled = $cardConfig->paytronixEnabled();
        $this->ncrEnabled = $cardConfig->ncrRadiantEnabled();

        $enabledUserManagers = [];

        if ($this->paytronixEnabled) {
            $enabledUserManagers[NandosCardConfig::CARD_PATRONIX] = static::PAYTRONIX_USER_MANAGER;
        }

        if ($this->ncrEnabled) {
            $enabledUserManagers[NandosCardConfig::CARD_NCR] = static::NCR_RADIANT_USER_MANAGER;
        }

        $this->enabledUserManagers = $enabledUserManagers;
    }

    /**
     * @return mixed
     */
    public function getPaytronixEnabled()
    {
        return $this->paytronixEnabled;
    }

    /**
     * @return mixed
     */
    public function getNcrEnabled()
    {
        return $this->ncrEnabled;
    }

    /**
     * @param $email
     * @param $password
     * @return bool
     */
    public function updateProfilePassword($email, $password, $hashed = true)
    {
        $response = false;

        foreach ($this->enabledUserManagers as $um) {
            $response = $this->$um->updateProfilePassword($email, $password, $hashed);
            if ($response != false) {
                break;
            }
        }

        return $response;
    }

    /**
     * @param $username
     * @param $password
     * @return User|bool|mixed|null
     */
    public function getAuthenticatedUserFromCardSystemAndStoreLocally($username, $password)
    {
        $user = null;

        foreach ($this->enabledUserManagers as $um) {

            // this stops the remote card system from being checked more than once on asingle request
            if (!in_array($um, $this->triedCardSystem)) {
                $this->triedCardSystem[] = $um;

                $user = $this->$um->getAuthenticatedUserAndStoreLocally($username, $password);
                if ($user instanceof User) {
                    break;
                }
            }
        }

        return $user;
    }

    /**
     * @param $email
     * @return bool|mixed
     */
    public function getUserFromCardSystemAndStoreLocally($email)
    {
        $user = false;

        foreach ($this->enabledUserManagers as $um) {
            $user = $this->$um->getUserAndStoreLocally($email);
            if ($user != false) {
                break;
            }
        }

        return $user;
    }

    /**
     * @param $email
     * @return bool|string
     */
    public function emailExistsInCardSystem($email)
    {
        $return = false;

        foreach ($this->enabledUserManagers as $key => $um) {
            $return = $this->$um->doesEmailExist($email) ? $key : false;
            if ($return != false) {
                break;
            }
        }

        return $return;
    }

    public function tryLoginOnCardSystem(Request $request)
    {
        if ($request->getMethod() === 'POST') {
            $inputData = $request->request->all();
        } else {
            $inputData = $request->query->all();
        }

        return $this->getAuthenticatedUserFromCardSystemAndStoreLocally($inputData['username'], $inputData['password']);
    }

}