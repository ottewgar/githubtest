<?php

namespace AppBundle\Api\Manager;

use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Log\Traits\LoggerTrait;

abstract class AbstractUserManager
{
    use LoggerTrait;

    /**
     * @param $source
     * @param $userProfile
     * @param null $password
     * @return mixed
     */
    protected function createEnabledUserFromProfile($source, $userProfile, $password = null)
    {
        $this->info("will create user from {$source} profile");

        // create a local user so we can attach a password reset token to it
        $user = $this->createUserFromProfile($userProfile, $password);

        // they have confirmed their email address in NCR so can activate them immediately
        $user->setEnabled(true);

        // Take note of how they registered
        $user->setRegisterSource($source);

        return $user;
    }

    /**
     * @param $email
     * @return mixed
     */
    public function doesEmailExist($email)
    {
        return $this->api->getEmailExistsAll($email);
    }

    /**
     * @param $password
     * @return bool|string
     */
    public function hashPassword($password)
    {
        $options = ['cost' => 12];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }
}