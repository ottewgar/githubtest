<?php

namespace AppBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ExtraDoctrineOrmConfigurationCompilerPass implements CompilerPassInterface
{
    /**
     * The Doctrine Configuration cannot take services by default, so we use a compiler pass to ensure we
     * can get our own Repository Factory inside the
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {

        $definition = $container->getDefinition('doctrine.orm.configuration');

        /**
         *  Set our own RepositoryFactory
         *  This allows us to get our own Entity repository services if they are tagged with "nandos.entity_repository"
         *  Our RepositorFactory is given te service container
         */

        $definition->addMethodCall(
            'setRepositoryFactory', [new Reference('nandos.doctrine.repository_factory')]
        );

        /**
         *  Now tell our custom RepositoryFactory about all the services tagged "nandos.entity_repository"
         *
         */

        $repositoryFactoryDefinition = $container->getDefinition('nandos.doctrine.repository_factory');

        $taggedServices = $container->findTaggedServiceIds('nandos.entity_repository');

        foreach ($taggedServices as $serviceId => $tags) {

            $serviceDefinition = $container->getDefinition($serviceId);
            $repositoryClassName = $serviceDefinition->getClass();

            $repositoryFactoryDefinition->addMethodCall(
                'setEntityManagerRepositoryClassServiceId', array($repositoryClassName, $serviceId)
            );

        }

    }
}