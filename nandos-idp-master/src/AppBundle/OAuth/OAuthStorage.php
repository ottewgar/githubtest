<?php
/**
 * Created by PhpStorm.
 * User: alistairburns
 * Date: 19/02/2016
 * Time: 14:50
 */

namespace AppBundle\OAuth;

use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Api\NcrRadiant\Manager\NcrRadiantUserManager;
use AppBundle\Api\NcrRadiant\Ncr;
use AppBundle\Api\Paytronix\Manager\PaytronixUserManager;
use AppBundle\Api\Paytronix\Paytronix;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Log\Traits\LoggerTrait;
use AppBundle\Util\Sanitizer;
use FOS\OAuthServerBundle\Model\ClientInterface;
use FOS\OAuthServerBundle\Storage\OAuthStorage as BaseOAuthStorage;
use OAuth2\Model\IOAuth2Client;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class OAuthStorage extends BaseOAuthStorage
{
    use LoggerTrait;

    /**
     * @var CardUserManagerContainer
     */

    private $cardUserManagerContainer;

    /**
     * @var UserManager
     */

    private $userManager;

    public function setCardUserManagerContainer(CardUserManagerContainer $cardUserManagerContainer)
    {
        $this->cardUserManagerContainer = $cardUserManagerContainer;
    }

    /**
     * @param UserManager $userManager
     */
    public function setUserManager($userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param IOAuth2Client $client
     * @param string $username
     * @param string $password
     * @return array|bool
     */
    public function checkUserCredentials(IOAuth2Client $client, $username, $password)
    {
        if (!$client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        $sanitizedEmail = Sanitizer::sanitiseEmail($username);
        $this->info("User with email address $sanitizedEmail attempting to OAuth Login...");

        try {
            $user = $this->userProvider->loadUserByUsername($username);
        } catch (AccessDeniedException $e) {

            $this->notice("Access denied for user $sanitizedEmail in Auth login");

            if (!$user = $this->cardUserManagerContainer->getAuthenticatedUserFromCardSystemAndStoreLocally($username, $password)) {
                $this->info("Authenticated user not found in card system for user $sanitizedEmail");
                return false;
            }
        } catch (UsernameNotFoundException $e) {

            $this->notice("Username not found for user $sanitizedEmail in Auth login");

            if (!$user = $this->cardUserManagerContainer->getAuthenticatedUserFromCardSystemAndStoreLocally($username, $password)) {
                $this->info("Authenticated user not found in card system for user $sanitizedEmail");
                return false;
            }
        }

        if (!$user->isEnabled()) {
            $this->notice("User $sanitizedEmail is not enabled");
            throw new AccessDeniedException('User is not enabled');
        }

        if (null !== $user) {
            $encoder = $this->encoderFactory->getEncoder($user);

            if ($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
                $this->info("User with email address {$sanitizedEmail} has successfully logged in");
                return array(
                    'data' => $user,
                );
            } else {
                if ($remoteUser = $this->cardUserManagerContainer->getAuthenticatedUserFromCardSystemAndStoreLocally($username, $password)) {
                    $this->info("User $sanitizedEmail existed in IDP but password did not match. Password did match when compaired to card system login");
                    return array(
                        'data' => $remoteUser,
                    );
                }

            }
        }

        return false;
    }

}