<?php

namespace AppBundle\Entity;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\AttributeOverrides;
use Doctrine\ORM\Mapping\AttributeOverride;
use Lexik\Bundle\MailerBundle\Mapping\Annotation as Mailer;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 *
 * Override a few fields to allow more room for encrypted content
 *
 * NOTE: MySQL UNIQUE indexes only work on fields up to length 255 so we must test the max allowed length
 * of email fields and ensure once encrypted they don't exceed 255 (including the <ENC> suffix)
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="nandos_user",
 *     indexes={
 *         @ORM\Index(name="nandos_card_number_idx", columns={"nandos_card_number"}),
 *         @ORM\Index(name="takeaway_customer_id_idx", columns={"takeaway_customer_id"})
 *     }, uniqueConstraints={
 *         @ORM\UniqueConstraint(name="username_canonical_uniq", columns={"username_canonical"}),
 *         @ORM\UniqueConstraint(name="email_canonical_uniq", columns={"email_canonical"})
 *     })
 *
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="email", column=@ORM\Column(length = 255)),
 *      @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(length = 255)),
 *      @ORM\AttributeOverride(name="username", column=@ORM\Column(length = 255)),
 *      @ORM\AttributeOverride(name="usernameCanonical", column=@ORM\Column(length = 255)),
 *      @ORM\AttributeOverride(name="firstname", column=@ORM\Column(length = 255)),
 *      @ORM\AttributeOverride(name="lastname", column=@ORM\Column(length = 255))
 * })
 *
 */

class User extends BaseUser
{

    /**
     * Error message (code) used when user already exists in the IDP
     *
     * To be kept in sync with the validator defined in validation.yml (until we create a custom user exists validator
     * that is able to use a class constant, or move the validation as an annotation in this class)
     */
    const ERROR_EMAIL_EXISTS_IN_IDP = 'email_exists_in_idp';

    /**
     * Error message (code) used when user already exists in NCR
     */

    const ERROR_EMAIL_EXISTS_IN_NCR = 'email_exists_in_ncr';

    /**
     * Error message (code) used when user already exists in Paytronix
     */

    const ERROR_EMAIL_EXISTS_IN_PAYTRONIX = 'email_exists_in_paytronix';

    /**
     * Error message (code) used when user already exists in a Nandoes card system
     */

    const ERROR_EMAIL_EXISTS_IN_NANDOS_CARD_SYSTEM = 'email_exists_in_nandos_card_system';

    /**
     * The user has registered for the first time through the IDP
     */

    const REGISTER_SOURCE_IDP = 'idp';

    /**
     * The user had an account created previously in NCR Radiant (Nando's card) and the account was created when they
     * logged in for the first time using NCR Radiant credentials
     */

    const REGISTER_SOURCE_NCR_RADIANT = 'ncr_radiant';

    /**
     * User had account in Paytronix but not in the IDP. This shouldn't really be possible
     */

    const REGISTER_SOURCE_PAYTRONIX = 'paytronix';

    /**
     * The user was imported from the Altaine takeaway user database (passwords marked as expired)
     */

    const REGISTER_SOURCE_TAKEAWAY = 'takeaway';

    protected static $registerSources = [
        self::REGISTER_SOURCE_IDP         => 'Nando\'s IDP',
        self::REGISTER_SOURCE_NCR_RADIANT => 'NCR Radiant (Nando\'s card)',
        self::REGISTER_SOURCE_PAYTRONIX    => 'Paytronix',
        self::REGISTER_SOURCE_TAKEAWAY    => 'Altaine Takeaway'
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Encrypted
     * @var string
     */
    protected $username;

    /**
     * @Encrypted
     * @var string
     */
    protected $usernameCanonical;

    /**
     * @Encrypted
     * @var string
     */
    protected $email;

    /**
     * @Encrypted
     * @var string
     */
    protected $emailCanonical;


    /**
     * @Encrypted
     * @var string
     */
    protected $firstname;

    /**
     * @Encrypted
     * @var string
     */
    protected $lastname;

    /**
     * @Encrypted
     * @var string
     *@ORM\Column(type="string", name="nandos_card_number", length=255, nullable=true)
     */
    protected $nandosCardNumber;

    /**
     * @Encrypted
     * @var string
     *@ORM\Column(type="string", name="takeaway_customer_id", length=255, nullable=true)
     */
    protected $takeawayCustomerId;

    /**
     * @var string
     * @ORM\Column(type="string", name="register_source", length=32)
     */

    protected $registerSource = self::REGISTER_SOURCE_IDP;

    /**
     * @ORM\OneToMany(targetEntity="UserMetadata", mappedBy="user", indexBy="key", cascade={"persist"})
     * @var UserMetadata[]
     */
    private $metadatas;

    /**
     * @var datetime
     * @ORM\Column(type="datetime", name="confirmation_expiry", nullable=true)
     */
    protected $confirmationExpiry;

    public function __construct()
    {
        parent::__construct();
        $this->metadatas = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *  We use the email as the username as well
     *
     * @param string $email
     */

    public function setEmail($email)
    {
        $this->email    = $email;
        $this->username = $email;
    }

    /**
     * @Mailer\Address()
     *
     * @return string
     */

    public function getEmail()
    {
        return parent::getEmail();
    }

    /**
     * @Mailer\Name()
     *
     * @return string
     */

    public function getFullname()
    {
        return parent::getFullname();
    }

    /**
     * The username should only be an email address, so set it as the email as well
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
        $this->email    = $username;
    }

    /**
     * @return string
     */
    public function getNandosCardNumber()
    {
        return $this->nandosCardNumber;
    }

    /**
     * @param string $nandosCardNumber
     */
    public function setNandosCardNumber($nandosCardNumber)
    {
        $this->nandosCardNumber = $nandosCardNumber;
    }

    /**
     * @return string
     */
    public function getTakeawayCustomerId()
    {
        return $this->takeawayCustomerId;
    }

    /**
     * @param string $takeawayCustomerId
     */
    public function setTakeawayCustomerId($takeawayCustomerId)
    {
        $this->takeawayCustomerId = $takeawayCustomerId;
    }

    /**
     * @return string
     */
    public function getRegisterSource()
    {
        return $this->registerSource;
    }

    /**
     * @param string $registerSource
     */
    public function setRegisterSource($registerSource)
    {
        $this->registerSource = $registerSource;
    }

    /**
     * @return array
     */
    public static function getRegisterSources()
    {
        return self::$registerSources;
    }

    /**
     * @param UserMetadata[] $metadatas
     */
    public function setMetadatas($metadatas)
    {
        foreach ($metadatas as $metadata) {
            $this->addMetadata($metadata);
        }
    }

    /**
     * @param UserMetadata $metadata
     */

    public function addMetadata($metadata)
    {
        $metadata->setUser($this);
        $this->metadatas[$metadata->getMetadataKey()] = $metadata;
    }

    /**
     * @param $key
     * @return UserMetadata|null
     */

    public function getMetadata($key)
    {
        if (!isset($this->metadatas[$key])) {
            return null;
        }

        return $this->metadatas[$key];
    }

    /**
     * @return UserMetadata[]
     */

    public function getMetadatas()
    {
        return $this->metadatas->toArray();
    }

    /**
     * @return mixed
     */
    public function getConfirmationExpiry()
    {
        return $this->confirmationExpiry;
    }

    /**
     * @param mixed $confirmationExpiry
     */
    public function setConfirmationExpiry($confirmationExpiry)
    {
        $this->confirmationExpiry = $confirmationExpiry;
    }

    /**
     * @param int $hours
     */
    public function renewConfirmationExpiry($hours = 48)
    {
        $datetime = new \DateTime;

        $interval = new \DateInterval("PT{$hours}H");
        $datetime->add($interval);

        $this->setConfirmationExpiry($datetime);
    }

    /**
     * @return bool
     */
    public function checkConfirmationCodeInDate()
    {
        $expiry = $this->getConfirmationExpiry();
        $now = new \DateTime();

        return ($expiry < $now) ? false : true;
    }
}
