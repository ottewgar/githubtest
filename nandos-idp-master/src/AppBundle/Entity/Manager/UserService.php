<?php

namespace AppBundle\Entity\Manager;

class UserService
{
    private $userManager;

    public function __construct($userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return mixed
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    /**
     * @param mixed $userManager
     */
    public function setUserManager($userManager)
    {
        $this->userManager = $userManager;
    }

}