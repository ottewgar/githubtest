<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\User;

use FOS\UserBundle\Util\TokenGeneratorInterface;
use Sonata\UserBundle\Entity\UserManager as BaseUserManager;

use RandomLib\Factory as RandomLibFactory;
use SecurityLib\Strength;

/**
 * Class UserManager
 * @package AppBundle\Entity\Manager
 *
 * We can add an extra setter injection of encrypt/decrypt when required
 *
 */


class UserManager extends BaseUserManager
{

    /**
     * Used to generate confirmation code for user registration etc.
     *
     * @var TokenGeneratorInterface
     */

    private $tokenGenerator;

    /**
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function setTokenGenerator(TokenGeneratorInterface $tokenGenerator)
    {
        $this->tokenGenerator = $tokenGenerator;
    }

    public function initialiseUserConfirmationToken(User $user)
    {
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }
        $user->renewConfirmationExpiry();

        return $user->getConfirmationToken();
    }

    /**
     * When a user registers, they only set their password after confirming so create a placeholder one for now
     * to ensure the account has a password. Note it is unusable for login, as it is marked as expired.
     *
     * @param User $user
     */

    public function initialiseExpiredRandomPassword(User $user)
    {
        $user->setCredentialsExpired(true);

        $generator = (new RandomLibFactory)->getGenerator(new Strength(Strength::MEDIUM));
        $user->setPlainPassword($generator->generateString(30));
    }

}