<?php

namespace AppBundle\Entity\Repository;

use Ambta\DoctrineEncryptBundle\Encryptors\EncryptorInterface;
use Ambta\DoctrineEncryptBundle\Services\Encryptor;
use Ambta\DoctrineEncryptBundle\Subscribers\DoctrineEncryptSubscriber;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \ReflectionClass;

abstract class EncryptedEntityRepository extends EntityRepository implements ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EncryptorInterface
     */

    private $encryptor;

    /**
     * @var Reader
     */

    private $annotationReader;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setEncryptor(Encryptor $encryptor)
    {
        $this->encryptor = $encryptor;
    }

    /**
     * @param Reader $annotationReader
     */
    public function setAnnotationReader($annotationReader)
    {
        $this->annotationReader = $annotationReader;
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return parent::findBy($this->getEncryptedCriteria($criteria), $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return parent::findOneBy($this->getEncryptedCriteria($criteria), $orderBy);
    }

    /**
     * Move to trait or service if needed outwith repository (e.g. admin filters)
     *
     * @param $criteria
     * @return mixed
     */

    public function getEncryptedCriteria($criteria)
    {

        $reader = $this->annotationReader;

        $properties = $this->getClassProperties($this->getClassName());

        $encryptedFields = [];

        // check all field names to see if they are encrypted
        foreach ($properties as $refProperty) {

            /**
             * If field contains the Encrypt annotation, encrypt before searching
             */
            if ($reader->getPropertyAnnotation($refProperty, DoctrineEncryptSubscriber::ENCRYPTED_ANN_NAME)) {
                $encryptedFields[] = $refProperty->getName();
            }
        }

        foreach ($criteria as $fieldname => $value) {
            if (in_array($fieldname, $encryptedFields)) {
                $criteria[$fieldname] = $this->encryptor->encrypt($value);
            }
        }

        return $criteria;

    }

    /**
     * Recursive function to get an associative array of class properties
     * including inherited ones from extended classes
     *
     * @param string $className Class name
     *
     * @return array
     */
    public function getClassProperties($className)
    {

        $reflectionClass = new ReflectionClass($className);
        $properties = $reflectionClass->getProperties();
        $propertiesArray = array();

        foreach($properties as $property) {
            $propertyName = $property->getName();
            $propertiesArray[$propertyName] = $property;
        }

        if($parentClass = $reflectionClass->getParentClass()) {
            $parentPropertiesArray = $this->getClassProperties($parentClass->getName());
            if(count($parentPropertiesArray) > 0) {
                $propertiesArray = array_merge($parentPropertiesArray, $propertiesArray);
            }
        }

        return $propertiesArray;
    }

}