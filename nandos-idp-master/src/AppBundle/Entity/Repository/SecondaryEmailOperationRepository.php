<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\SecondaryEmailOperation;
use AppBundle\Entity\User;

class SecondaryEmailOperationRepository extends EncryptedEntityRepository
{

    /**
     * Allow an unused record to be reused, a new token MUST be generated for security as we extend the expiry of the token
     *
     * @param $operation
     * @param User $primaryUser
     * @param User $secondaryUser
     * @return bool|mixed
     */

    public function getPendingOperationForPrimaryUserAndSecondaryUser($operation, User $primaryUser, User $secondaryUser)
    {
        $criteria = [
            'requestedOperation'    => $operation,
            'primaryUser'           => $primaryUser,
            'secondaryUser'         => $secondaryUser,
            'primaryEmailAddress'   => $primaryUser->getEmailCanonical(),
            'secondaryEmailAddress' => $secondaryUser->getEmailCanonical(),
            'operationAttemptedAt'  => null,
            'operationSuccess'      => null
        ];

        $pendingOperations = $this->findBy($criteria, ['id' => 'DESC']);

        if (count($pendingOperations)) {
            $newestOperation = reset($pendingOperations);
            return $newestOperation;
        }

        return false;

    }

    /**
     * Create a new operation
     *
     * @param $operation
     * @param User $primaryUser
     * @param User $secondaryUser
     * @return SecondaryEmailOperation
     */
    public function createPendingOperationForPrimaryUserAndSecondaryUser($operation, User $primaryUser, User $secondaryUser)
    {
        $secondaryEmailOperation = new SecondaryEmailOperation();

        $secondaryEmailOperation
            ->setRequestedOperation($operation)
            ->setPrimaryUser($primaryUser)
            ->setSecondaryUser($secondaryUser);

        return $secondaryEmailOperation;

    }

    /**
     * Save the record to the database
     *
     * @param SecondaryEmailOperation $secondaryEmailOperation
     */
    public function save(SecondaryEmailOperation $secondaryEmailOperation)
    {
        $this->_em->persist($secondaryEmailOperation);
        $this->_em->flush();
    }

    /**
     * @param $requestedOperation
     * @param $primaryUser
     * @param $verificationToken
     * @return bool|SecondaryEmailOperation
     */
    public function getOneByRequestedOperationPrimaryUserAndVerificationToken($requestedOperation, $primaryUser, $verificationToken)
    {
        $criteria = [
            'requestedOperation'    => $requestedOperation,
            'primaryUser'           => $primaryUser,
            'verificationToken'     => $verificationToken
        ];

        $operations = $this->findBy($criteria, ['id' => 'DESC']);

        if (count($operations)) {
            $newestOperation = reset($operations);
            return $newestOperation;
        }

        return false;

    }

  /**
     * @param $requestedOperation
     * @param $primaryUser
     * @param $verificationToken
     * @return bool|SecondaryEmailOperation
     */
    public function getOneByPrimaryUserAndVerificationToken($primaryUser, $verificationToken)
    {
        $criteria = [
            'primaryUser'           => $primaryUser,
            'verificationToken'     => $verificationToken
        ];

        $operations = $this->findBy($criteria, ['id' => 'DESC']);

        if (count($operations)) {
            $newestOperation = reset($operations);
            return $newestOperation;
        }

        return false;

    }

}