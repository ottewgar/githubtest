<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Client extends BaseClient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="name", nullable=false)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="text", name="allowed_routes", nullable=true)
     */
    protected $allowedRoutes;

    public function __construct()
    {
        parent::__construct();
        // Nandos specific constructor logic here...
    }

    /**
     * @return array
     */
    public function getAllowedRoutes()
    {
        return $this->allowedRoutes;
    }

    /**
     * @param array $allowedRoutes
     */
    public function setAllowedRoutes($allowedRoutes)
    {
        $this->allowedRoutes = $allowedRoutes;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}