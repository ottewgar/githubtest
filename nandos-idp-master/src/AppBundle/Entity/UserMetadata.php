<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserMetadataRepository")
 * @ORM\Table(name="nandos_user_metadata",
 *     indexes={
 *         @ORM\Index(name="metadata_key_idx", columns={"metadata_key"}),
 *     }, uniqueConstraints={
 *         @ORM\UniqueConstraint(name="user_metadata_key_uniq", columns={"user_id", "metadata_key"})
 *     })
 *
 */

class UserMetadata
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="metadatas")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(type="integer", name="user_id")
     */

    private $userId;

    /**
     * @ORM\Column(type="string", name="metadata_key", length=128)
     */
    private $metadataKey;

    /**
     * @ORM\Column(type="string", name="metadata_format", length=32)
     */
    private $metadataFormat = 'string';

    /**
     * @ORM\Column(type="string", name="metadata_value", length=1024, nullable=true)
     */
    private $metadataValue;

    /**
     * @Encrypted
     * @ORM\Column(type="string", name="metadata_encrypted_value", length=2048, nullable=true)
     */
    private $metadataEncryptedValue;

    static public function create($key, $value, $isEncrypted = false, $format = null)
    {

        $metadata = new self;

        $metadata->setMetadataKey($key);

        if ($isEncrypted) {
            $metadata->setMetadataEncryptedValue($value);
        } else {
            $metadata->setMetadataValue($value);
        }

        if (null !== $format) {
            $metadata->setMetadataFormat($format);
        }

        return $metadata;

    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Used to create a new entity in the admin without choosing a user
     *
     * @param int $userId
     * @return UserMetadata
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return UserMetadata
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMetadataKey()
    {
        return $this->metadataKey;
    }

    /**
     * @param mixed $metadataKey
     * @return UserMetadata
     */
    public function setMetadataKey($metadataKey)
    {
        $this->metadataKey = $metadataKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMetadataFormat()
    {
        return $this->metadataFormat;
    }

    /**
     * @param mixed $metadataFormat
     * @return UserMetadata
     */
    public function setMetadataFormat($metadataFormat)
    {
        $this->metadataFormat = $metadataFormat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMetadataValue()
    {
        return $this->metadataValue;
    }

    /**
     * @param mixed $metadataValue
     * @return UserMetadata
     */
    public function setMetadataValue($metadataValue)
    {
        $this->metadataValue = $metadataValue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMetadataEncryptedValue()
    {
        return $this->metadataEncryptedValue;
    }

    /**
     * @param mixed $metadataEncryptedValue
     * @return UserMetadata
     */
    public function setMetadataEncryptedValue($metadataEncryptedValue)
    {
        $this->metadataEncryptedValue = $metadataEncryptedValue;
        return $this;
    }

    function __toString()
    {
        return  $this->metadataKey ? 'metadata field: ' . $this->metadataKey : 'new user metadata field';
    }

}