<?php

namespace AppBundle\Entity;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\SecondaryEmailOperationRepository")
 * @ORM\Table(name="secondary_email_operation", indexes={@ORM\Index(name="verification_token_idx", columns={"primary_user_id", "verification_token"})})
 *
 * @ExclusionPolicy("all")
 */
class SecondaryEmailOperation
{

    /**
     * Allow a user to move the Takeaway customer ID from another IDP user account into their own one
     */
    const OPERATION_MERGE_OLD_TAKEAWAY_ACCOUNT = 'merge_old_takeaway_account';

    protected static $requestedOperations = [
        self::OPERATION_MERGE_OLD_TAKEAWAY_ACCOUNT => 'Merge old takeaway account'
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="verification_token", length=64)
     */
    private $verificationToken;

    /**
     * @ORM\Column(type="datetime", name="expires_at")
     */
    private $expiresAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="primary_user_id", referencedColumnName="id", onDelete="CASCADE")
     * @var User
     */
    private $primaryUser;

    /**
     * @Encrypted
     * @ORM\Column(type="string", name="primary_email_address", length=512)
     * @Expose
     * @Groups({"basic_details"})
     */
    private $primaryEmailAddress;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="secondary_user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var User
     */
    private $secondaryUser;

    /**
     * @Encrypted
     * @ORM\Column(type="string", name="secondary_email_address", length=512)
     * @Expose
     * @Groups({"basic_details"})
     */
    private $secondaryEmailAddress;

    /**
     * @ORM\Column(type="string", name="requested_operation", length=64)
     * @Expose
     * @Groups({"basic_details"})
     */
    private $requestedOperation;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", name="operation_attempted_at", nullable=true)
     */
    private $operationAttemptedAt;

    /**
     * @ORM\Column(type="boolean", name="operation_success", nullable=true)
     */
    private $operationSuccess = null;

    /**
     * @ORM\Column(type="text", name="operation_log", nullable=true)
     */
    private $operationLog;

    public function __construct()
    {
        $this->refreshExpiresAt();
        $this->createdAt = new \DateTime();
    }

    /**
     * To reuse an existing record, refresh the expires at
     */
    public function refreshExpiresAt()
    {
        $this->expiresAt = new \DateTime('+1 year');
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return SecondaryEmailOperation
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerificationToken()
    {
        return $this->verificationToken;
    }

    /**
     * @param mixed $verificationToken
     * @return SecondaryEmailOperation
     */
    public function setVerificationToken($verificationToken)
    {
        $this->verificationToken = $verificationToken;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @param mixed $expiresAt
     * @return SecondaryEmailOperation
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;
        return $this;
    }

    /**
     * @return User
     */
    public function getPrimaryUser()
    {
        return $this->primaryUser;
    }

    /**
     * @param User $primaryUser
     * @return SecondaryEmailOperation
     */
    public function setPrimaryUser($primaryUser)
    {
        $this->primaryUser = $primaryUser;
        $this->primaryEmailAddress = $primaryUser->getEmailCanonical();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrimaryEmailAddress()
    {
        return $this->primaryEmailAddress;
    }

    /**
     * @param mixed $primaryEmailAddress
     * @return SecondaryEmailOperation
     */
    public function setPrimaryEmailAddress($primaryEmailAddress)
    {
        $this->primaryEmailAddress = $primaryEmailAddress;
        return $this;
    }

    /**
     * @return User
     */
    public function getSecondaryUser()
    {
        return $this->secondaryUser;
    }

    /**
     * @param User $secondaryUser
     * @return SecondaryEmailOperation
     */
    public function setSecondaryUser($secondaryUser)
    {
        $this->secondaryUser = $secondaryUser;
        $this->secondaryEmailAddress = $secondaryUser->getEmailCanonical();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSecondaryEmailAddress()
    {
        return $this->secondaryEmailAddress;
    }

    /**
     * @param mixed $secondaryEmailAddress
     * @return SecondaryEmailOperation
     */
    public function setSecondaryEmailAddress($secondaryEmailAddress)
    {
        $this->secondaryEmailAddress = $secondaryEmailAddress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestedOperation()
    {
        return $this->requestedOperation;
    }

    /**
     * @param mixed $requestedOperation
     * @return SecondaryEmailOperation
     */
    public function setRequestedOperation($requestedOperation)
    {
        $this->requestedOperation = $requestedOperation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return SecondaryEmailOperation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperationAttemptedAt()
    {
        return $this->operationAttemptedAt;
    }

    /**
     * @param mixed $operationAttemptedAt
     * @return SecondaryEmailOperation
     */
    public function setOperationAttemptedAt($operationAttemptedAt)
    {
        $this->operationAttemptedAt = $operationAttemptedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperationSuccess()
    {
        return $this->operationSuccess;
    }

    /**
     * @param mixed $operationSuccess
     * @return SecondaryEmailOperation
     */
    public function setOperationSuccess($operationSuccess)
    {
        $this->operationSuccess = $operationSuccess;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperationLog()
    {
        return $this->operationLog;
    }

    /**
     * @param mixed $operationLog
     * @return SecondaryEmailOperation
     */
    public function setOperationLog($operationLog)
    {
        $this->operationLog = $operationLog;
        return $this;
    }

    /**
     * @return array
     */
    public static function getRequestedOperations()
    {
        return self::$requestedOperations;
    }

    /**
     * @param \DateTime $currentDateTime
     * @return bool
     *
     * @VirtualProperty
     * @Groups({"basic_details"})
     *
     */

    public function isExpired($currentDateTime = null)
    {

        if (null === $currentDateTime) {
            $currentDateTime = new \DateTime();
        }

        if (!$this->expiresAt) {
            return false;
        }

        return $this->expiresAt < $currentDateTime;

    }


}