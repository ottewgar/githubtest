<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="idp_setting",
 * indexes={
 *         @ORM\Index(name="setting_key_idx", columns={"setting_key"})
 * })
 */
class IdpSetting
{
    const TYPE_TEXT = 'text';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_DATE = 'date';
    const TYPE_DATETIME = 'datetime';

    static $types = [
        self::TYPE_TEXT => 'Text',
        self::TYPE_BOOLEAN => 'Boolean',
        self::TYPE_DATE => 'Date',
        self::TYPE_DATETIME => 'Date with time',
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="setting_key", length=128)
     */
    private $settingKey;

    /**
     * @ORM\Column(type="text", name="setting_value", nullable=true)
     */
    private $settingValue;

    /**
     * @ORM\Column(type="string", name="setting_type", length=64)
     */
    private $settingType;

    /**
     * @ORM\Column(type="string", name="setting_help", length=512, nullable=true)
     */
    private $settingHelp;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSettingKey()
    {
        return $this->settingKey;
    }

    /**
     * @param mixed $settingKey
     */
    public function setSettingKey($settingKey)
    {
        $this->settingKey = $settingKey;
    }

    /**
     * @return mixed
     */
    public function getSettingValue()
    {
        return $this->settingValue;
    }

    /**
     * @param mixed $settingValue
     */
    public function setSettingValue($settingValue)
    {
        $this->settingValue = $settingValue;
    }

    /**
     * @return mixed
     */
    public function getSettingType()
    {
        return $this->settingType;
    }

    /**
     * @param mixed $settingType
     */
    public function setSettingType($settingType)
    {
        $this->settingType = $settingType;
    }

    /**
     * @return mixed
     */
    public function getSettingHelp()
    {
        return $this->settingHelp;
    }

    /**
     * @param mixed $settingHelp
     */
    public function setSettingHelp($settingHelp)
    {
        $this->settingHelp = $settingHelp;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return self::$types;
    }

    /**
     * @param array $types
     */
    public static function setTypes($types)
    {
        self::$types = $types;
    }
}
