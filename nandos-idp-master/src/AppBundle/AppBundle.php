<?php

namespace AppBundle;

use AppBundle\DependencyInjection\Compiler\ExtraDoctrineOrmConfigurationCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    /**
     * Add our own compiler pass to give Doctrine our own Repository Factory
     *
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new ExtraDoctrineOrmConfigurationCompilerPass());

    }


}
