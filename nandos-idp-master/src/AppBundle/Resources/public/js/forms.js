$(document).ready(function() {
    var $container = $('#api_result');
    var $form = $('.ajax_form');

    $form.each(function() {

        $this = $(this);
        $this.on('submit', function(e) {
            $form = $(this);
            e.preventDefault();

            submitForm($form)
        });
    });

    function submitForm($form)
    {
        var action = $form.attr('action');
        var data = $form.serialize();

        $.ajax( {
            type: $form.attr('method'),
            url: action,
            data: data,
            success: function( response ) {

                $container.html("Success");

                $container.removeClass('alert alert-danger');
                $container.addClass('alert alert-success');

                appendResponse(response);
            },
            error: function(response) {

                response = response.responseJSON;
                $container.html("Error");
                $container.removeClass('alert alert-success');
                $container.addClass('alert alert-danger');
                appendResponse(response);
            },
            beforeSend: function(xhr, settings) {
                var token = $('#token').val();

                if (token != "") {
                    xhr.setRequestHeader('Authorization','Bearer ' + token); }
                }

        } );

    }

    function appendResponse(response)
    {
        for (var key in response) {
            $container.append( "<p> " + key + ": " + response[key] + "</p>" );
        }
    }

});
