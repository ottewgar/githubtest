Confirmation Token
==================


###Account Confirmation

The confirmation token is generated when a new account is created

The user must get this token from their email. It then needs to be submitted to the following endpoint

```
/api/v1/token/{confirmationToken}/confirm
```

This will enabled the account.

###Password Reset

The reset password also uses the confirmation token.

When Password reset is requested with 

/api/v1/send-password-reset-email

An email is sent with the confirmation token

This must be sent to the endpoint - 
/api/v1/token/{confirmationToken}/reset-password

along with the new password
