OAuth
=====


The login endpoint used is /oauth/v2/token


To login you need


client_id       - id string of the client
client_secret   - secret id string
grant_type      - mainly used as password or refresh_token

username        - IDP username
password        - IDP password

refresh_token   - only used if you are using the token refresh



###Client Restriction

By default a client has access to every endpoint.

This can be restricted by chaning the "allowed_routes" in the IDP admin.

These are possible routes which can be added in the allowed routes

- app_api_registerapi_register                                               
- app_api_registerapi_confirmuseraccount
- app_api_registerapi_resetpassword
- app_api_registerapi_resendconfirmationemail
- app_api_registerapi_sendpasswordresetemail
- app_api_userapi_userdetails
- app_api_userapi_updateuserprofile
- app_api_userapi_changepassword
- app_api_userapi_mergeoldtakeawayaccount
- app_api_userapi_confirmmergeoldtakeawayaccount
- app_api_userapi_secondaryemailoperationdetails