API Errors
==========

##Test mode paramter
The api_test_mode parameter in the IDP YML config will allow additional actions to be tested.

``` yml
api_test_mode: true
```

##Too Many logins

Currently the rate limit on login is 10 within 300 seconds.

If you use account "ratelimit@blonde.net" (password is "password") you will see the rate limit exception.

Any other account can be forced to show this error also by making more then 10 login attempts in 300 seconds.

The rate limit will reset once the time expires.


##Broken DB

If site is in production mode a broken DB will cause a 500 error which will be a 500 response 
with no output in the body.

To see a 500 error login with user "break@blonde.net". Password is "password".

Then request the user info.


##Token and refresh token times

If the account "token@blonde.net" (password is "password") is used this will automatically set the token to expire in 10 seconds time.

A request for a new token can then be requested using the refresh token
Send grant_type - refresh_token and pass the refresh token as refresh_token

