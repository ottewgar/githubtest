Settings
=======


Required settings are:

AT LEAST ONE OF THE APIS SHOULD BE ACTIVE

ncr_radiant_enabled: 1 or 0 depending whether the ncr api should be active

paytronix_enabled: 1 or 0 depending on whether the Paytronix API should be active.