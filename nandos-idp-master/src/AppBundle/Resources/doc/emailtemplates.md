Email Templates
===============

###confirm_merge_old_takeaway_account



	
###email_address_changed

	
###password_reset

Args that can be used in template
```
user (object)
confirmation_token
```
	
###resend_confirmation

Args that can be used in template
```
user (object)
confirmation_token
```


	
###user_register

Args that can be used in template
```
user (object)
confirmation_token
```

Sent when user registers for a new account. The token is emailed to allow the user to confirm their account.

An issue with this is that if a user registers and confirms their account they still cannot login as they then have to reset their
password.

A way around this is to use the reset-password call but pass a param for a different email template.
Then when the user confirms their account they can reset their password at the same time.

	
