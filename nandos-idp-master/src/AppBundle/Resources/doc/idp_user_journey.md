IDP User Journey
================

###Login

User will try and log into IDP REST API with the associated client_id and secret.

IDP will first check for user in IDP database. If the user exists the REST API with authenticate and
send back the access token.

If no user exists in the IDP the IDP will look for the requested username and password in all enabled
card Systems.

First Paytronix and then NCR.

If a username and password match is found in the first check then the second card system will not be
checked.

If a user is found the same user is then created in the IDP so for future logins the card system
will not need to be contacted.

####v1

For the first release IDP. The IDP will check IDP login first. If it fails it will also check enabled card systems to see
if the username and password match the login details in the card system. If there is a successful login for a card system the IDP
passord is changed to the new password and the user is logged in.

####v2

The check mentioned in v1 will be removed.


###Register

When the user tries to register all enabled card systems are checked to see if the email address
exists in the system.

If it does the user will have to login or reset password instead of registering.

Registration will not be done directly with the IDP. Instead a user ill be registered to the card system and when the user tries
to login with the correct credentials the IDP account will be created.


###Reset Password

If the user exists in the IDP a confirmation token is emailed to the user. If the user has their email existing in
one of the card systems but not in the IDP the IDP has to create an account for the user in the IDP first and then send 
the confirmation token.

The user must then submit the confirmation token and a new password.

####v1

In the first release reset password will be handled by the Drupal site by connecting to the enabled card system.

####v2

In version 2 the Drupal site will use the IDP reset functionality instead of directly connecting to the card system
