Rate Limits
===========


###Login
Set to 10 requests in 300 seconds. This is restricted to the username used and not an IP address

- GET POST /oauth/v2/token


###Authentcated
100 request in 3600 seconds - Restriction against the access token

- POST /api/v1/user
- GET /api/v1/user
- POST /api/v1/user/change-password
- POST /api/v1/user/merge-old-takeaway-account
- POST /api/v1/user/merge-old-takeaway-account/confirm
- GET /api/v1/user/secondary-email-operation/{verificationToken}
 
###Registration and Reset
1000 requests in 600 seconds - Only resricted again a specific URL. So this could be 100 requests from all different 
users and clients.
 
- POST /api/v1/register
- POST /api/v1/token/{confirmationToken}/confirm
- POST /api/v1/token/{confirmationToken}/reset-password
- POST /api/v1/resend-confirmation-email
- POST /api/v1/send-password-reset-email
