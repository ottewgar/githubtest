
Account Status
==========

##User Management Fields

Locked - Account can be disabled with the locked option

Enabled - user is enabled. This is set after confirmation code is entered.

Credentials Expired - Password must be reset
