<?php

namespace AppBundle\Tests\Handlers;

use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Api\Paytronix\Manager\PaytronixUserManager;
use AppBundle\Email\NandosMailer;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Form\Handler\Api\ResetPasswordByTokenFormHandler;
use AppBundle\Form\Handler\Api\SendPasswordResetEmailFormHandler;
use FOS\RestBundle\Serializer\ExceptionWrapperNormalizer;
use FOS\RestBundle\Util\ExceptionWrapper;
use FOS\RestBundle\View\ExceptionWrapperHandler;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormRegistry;
use Symfony\Component\Form\ResolvedFormType;
use Symfony\Component\HttpFoundation\Request;

class SendPasswordResetEmailFormHandlerTest extends \PHPUnit_Framework_TestCase
{
    protected $userManager;
    protected $sendPasswordResetFormHandler;
    protected $cardUserManagerContainer;
    protected $formFactory;
    protected $form;
    protected $exceptionWrapperHandler;
    protected $exceptionWrapper;
    protected $exceptionWrappernormalizer;
    protected $mailer;

    protected function setUp()
    {
        $this->userManager = $this->getMockBuilder(UserManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->cardUserManagerContainer = $this->getMockBuilder(CardUserManagerContainer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->formFactory = $this->getMockBuilder(FormFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->form = $this->getMockBuilder(Form::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->exceptionWrapperHandler = $this->getMockBuilder(ExceptionWrapperHandler::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->exceptionWrapper = $this->getMockBuilder(ExceptionWrapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->exceptionWrappernormalizer = $this->getMockBuilder(ExceptionWrapperNormalizer::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mailer = $this->getMockBuilder(NandosMailer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->sendPasswordResetFormHandler = new SendPasswordResetEmailFormHandler($this->userManager, $this->cardUserManagerContainer, $this->mailer);
        $this->sendPasswordResetFormHandler->setFormFactory($this->formFactory);
        $this->sendPasswordResetFormHandler->setExceptionWrapperHandler($this->exceptionWrapperHandler);
        $this->sendPasswordResetFormHandler->setExceptionWrapperNormalizer($this->exceptionWrappernormalizer);
    }

    /**
     * Tests a successful form submission
     */
    public function testHandleSuccess()
    {
        $request = new Request();

        $this->formFactory->expects($this->once())->method('createNamed')->will($this->returnValue($this->form));
        $this->form->expects($this->once())->method('handleRequest')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isSubmitted')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isValid')->will($this->returnValue(true));

        $user = new User();
        $user->setEmail('scottp99@gmail.com');

        $this->form->method('getData')->will($this->returnValue($user));

        $this->userManager->expects($this->once())->method('findUserByEmail')->will($this->returnValue($user));
        $this->userManager->method('initialiseExpiredRandomPassword')->will($this->returnValue(true));
        $this->userManager->method('initialiseUserConfirmationToken')->will($this->returnValue(true));
        $this->userManager->method('updateUser')->will($this->returnValue(true));
        $this->mailer->expects($this->once())->method('sendPasswordResetEmail')->will($this->returnValue(true));

        $return = $this->sendPasswordResetFormHandler->handle($request);

        $data = $return->getData();

        $this->assertEquals(200, $data['code']);
        $this->assertEquals('Password reset email resent', $data['message']);
    }

    public function testHandleUserInCS()
    {
        $request = new Request();

        $this->formFactory->expects($this->once())->method('createNamed')->will($this->returnValue($this->form));
        $this->form->expects($this->once())->method('handleRequest')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isSubmitted')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isValid')->will($this->returnValue(true));

        $user = new User();
        $user->setEmail('scottp99@gmail.com');

        $this->form->method('getData')->will($this->returnValue($user));

        $this->userManager->expects($this->once())->method('findUserByEmail')->will($this->returnValue(null));

        $this->cardUserManagerContainer->expects($this->once())->method('getUserFromCardSystemAndStoreLocally')->will($this->returnValue($user));

        $this->userManager->method('initialiseExpiredRandomPassword')->will($this->returnValue(true));
        $this->userManager->method('initialiseUserConfirmationToken')->will($this->returnValue(true));
        $this->userManager->method('updateUser')->will($this->returnValue(true));
        $this->mailer->expects($this->once())->method('sendPasswordResetEmail')->will($this->returnValue(true));

        $return = $this->sendPasswordResetFormHandler->handle($request);

        $data = $return->getData();

        $this->assertEquals(200, $data['code']);
        $this->assertEquals('Password reset email resent', $data['message']);
    }

    public function testHandleNoUser()
    {
        $request = new Request();

        $this->formFactory->expects($this->once())->method('createNamed')->will($this->returnValue($this->form));
        $this->form->expects($this->once())->method('handleRequest')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isSubmitted')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isValid')->will($this->returnValue(true));

        $user = new User();
        $user->setEmail('scottp99@gmail.com');

        $this->form->method('getData')->will($this->returnValue($user));

        $this->userManager->expects($this->once())->method('findUserByEmail')->will($this->returnValue(null));

        $this->cardUserManagerContainer->expects($this->once())->method('getUserFromCardSystemAndStoreLocally')->will($this->returnValue(null));

        $return = $this->sendPasswordResetFormHandler->handle($request);

        $data = $return->getData();

        $this->assertEquals(404, $data['code']);
        $this->assertEquals('user_not_found', $data['message']);
    }
}
