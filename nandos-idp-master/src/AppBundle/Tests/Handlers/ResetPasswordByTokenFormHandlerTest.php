<?php

namespace AppBundle\Tests\Handlers;

use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Api\Paytronix\Manager\PaytronixUserManager;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Form\Handler\Api\ResetPasswordByTokenFormHandler;
use FOS\RestBundle\Serializer\ExceptionWrapperNormalizer;
use FOS\RestBundle\Util\ExceptionWrapper;
use FOS\RestBundle\View\ExceptionWrapperHandler;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormRegistry;
use Symfony\Component\Form\ResolvedFormType;
use Symfony\Component\HttpFoundation\Request;

class ResetPasswordByTokenFormHandlerTest extends \PHPUnit_Framework_TestCase
{
    protected $userManager;
    protected $resetPasswordFormHandler;
    protected $cardUserManagerContainer;
    protected $formFactory;
    protected $form;
    protected $exceptionWrapperHandler;
    protected $exceptionWrapper;
    protected $exceptionWrappernormalizer;

    protected function setUp()
    {
        $this->userManager = $this->getMockBuilder(UserManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->cardUserManagerContainer = $this->getMockBuilder(CardUserManagerContainer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->formFactory = $this->getMockBuilder(FormFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->form = $this->getMockBuilder(Form::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->exceptionWrapperHandler = $this->getMockBuilder(ExceptionWrapperHandler::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->exceptionWrapper = $this->getMockBuilder(ExceptionWrapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->exceptionWrappernormalizer = $this->getMockBuilder(ExceptionWrapperNormalizer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resetPasswordFormHandler = new ResetPasswordByTokenFormHandler($this->userManager, $this->cardUserManagerContainer);
        $this->resetPasswordFormHandler->setFormFactory($this->formFactory);
        $this->resetPasswordFormHandler->setExceptionWrapperHandler($this->exceptionWrapperHandler);
        $this->resetPasswordFormHandler->setExceptionWrapperNormalizer($this->exceptionWrappernormalizer);
    }

    /**
     * Tests a successful form submission
     */
    public function testHandleSuccess()
    {
        $confirmationToken = 'test token';

        $request = new Request();

        $user = new User();
        $user->renewConfirmationExpiry();

        $this->userManager->expects($this->once())->method('findUserByConfirmationToken')->will($this->returnValue($user));
        $this->formFactory->expects($this->once())->method('createNamed')->will($this->returnValue($this->form));
        $this->form->expects($this->once())->method('handleRequest')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isSubmitted')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isValid')->will($this->returnValue(true));

        $this->userManager->expects($this->once())->method('updateUser')->will($this->returnValue(true));
        $this->cardUserManagerContainer->expects($this->once())->method('updateProfilePassword')->will($this->returnValue(true));

        $return = $this->resetPasswordFormHandler->handle($confirmationToken, $request);

        $data = $return->getData();

        $this->assertEquals(200, $data['code']);
        $this->assertEquals('Password updated', $data['message']);
    }

    /**
     * Test when form fails
     */
    public function testHandleFail()
    {
        $confirmationToken = 'test token';

        $request = new Request();

        $user = new User();
        $user->renewConfirmationExpiry();

        $this->userManager->expects($this->once())->method('findUserByConfirmationToken')->will($this->returnValue($user));
        $this->formFactory->expects($this->once())->method('createNamed')->will($this->returnValue($this->form));
        $this->form->expects($this->once())->method('handleRequest')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isSubmitted')->will($this->returnValue(true));
        $this->form->expects($this->once())->method('isValid')->will($this->returnValue(false));

        $this->exceptionWrapperHandler->method('wrap')->will($this->returnValue($this->exceptionWrapper));

        $this->exceptionWrappernormalizer->method('normalize')->will($this->returnValue([]));

        $return = $this->resetPasswordFormHandler->handle($confirmationToken, $request);

        $statusCode = $return->getResponse()->getStatusCode();

        $this->assertEquals(400, $statusCode);
    }

    /**
     * Test when the user has an expired confirmation code
     */
    public function testHandleExpiredCode()
    {
        $confirmationToken = 'test token';

        $request = new Request();

        $user = new User();


        $datetime = new \DateTime;

        $interval = new \DateInterval("PT5H");
        $datetime->sub($interval);

        $user->setConfirmationExpiry($datetime);

        $this->userManager->expects($this->once())->method('findUserByConfirmationToken')->will($this->returnValue($user));

        $return = $this->resetPasswordFormHandler->handle($confirmationToken, $request);

        $data = $return->getData();

        $this->assertEquals(200, $data['code']);
        $this->assertEquals('Confirmation Code Expired', $data['message']);

    }

    /**
     * Test when no user is found from the token lookup
     */
    public function testHandleUserNotFound()
    {
        $confirmationToken = 'test token';

        $request = new Request();

        $this->userManager->expects($this->once())->method('findUserByConfirmationToken')->will($this->returnValue(null));

        $return = $this->resetPasswordFormHandler->handle($confirmationToken, $request);

        $data = $return->getData();
        $this->assertEquals(404, $data['code']);
        $this->assertEquals('user_not_found', $data['message']);
    }
}
