<?php

namespace AppBundle\Tests\Api\NcrRadiant\Manager;

use Ambta\DoctrineEncryptBundle\Encryptors\Rijndael256Encryptor;
use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Api\NcrRadiant\Manager\NcrRadiantUserManager;
use AppBundle\Api\NcrRadiant\NcrRadiantApi;
use AppBundle\Api\NcrRadiant\NcrRadiantConfig;
use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Api\Paytronix\Manager\PaytronixUserManager;
use AppBundle\Api\Paytronix\Paytronix;
use AppBundle\Api\Paytronix\PaytronixApi;
use AppBundle\Api\Paytronix\PaytronixConfig;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\OAuth\OAuthStorage;
use AppBundle\Util\NandosCardConfig;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use FOS\OAuthServerBundle\Model\AuthCodeManagerInterface;
use FOS\OAuthServerBundle\Model\Client;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenManagerInterface;
use FOS\UserBundle\Security\UserProvider;
use FOS\UserBundle\Util\CanonicalizerInterface;
use OAuth2\Model\IOAuth2Client;
use OAuth2\OAuth2Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class NcrRadiantUserManagerMockTest extends \PHPUnit_Framework_TestCase
{
    protected $api;
    protected $userManager;
    protected $config;
    protected $ncrRadiantUserManager;

    protected function setUp()
    {
        $this->api = $this->getMockBuilder(NcrRadiantApi::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->userManager = $this->getMockBuilder(UserManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->config = $this->getMockBuilder(NcrRadiantConfig::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->ncrRadiantUserManager = new NcrRadiantUserManager($this->api, $this->userManager, $this->config);
    }

    public function testPasswordMatchesProfilePassword()
    {
        $profile = $this->getMemberProfile();

        $return = $this->ncrRadiantUserManager->passwordMatchesProfilePassword($profile, 'passwordABC');

        $this->assertEquals(true, $return);
    }

    public function testPasswordMatchesProfilePasswordMd5()
    {
        $profile = $this->getMemberProfile();

        $profile->updatePassword(md5('passwordABC'));

        $this->api->expects($this->once())->method('addMemberProfile')->will($this->returnValue(false));

        $return = $this->ncrRadiantUserManager->passwordMatchesProfilePassword($profile, 'passwordABC');

        $this->assertEquals(true, $return);
    }

    public function testGetAuthenticatedUserAndStoreLocally()
    {
        $this->api
            ->method('getProfileByEmail')
            ->will($this->returnValue(
                $this->getMemberProfile()
            ));

        $this->userManager->expects($this->once())->method('updateUser')->will($this->returnValue(true));

        $return = $this->ncrRadiantUserManager->getAuthenticatedUserAndStoreLocally('scottp99@gmail.com', 'passwordABC');

        $this->assertInstanceOf(User::class, $return);
    }

    public function testGetUserAndStoreLocally()
    {
        $this->api
            ->expects($this->once())
            ->method('getProfileByEmail')
            ->will($this->returnValue(
                $this->getMemberProfile()
            ));

        $return = $this->ncrRadiantUserManager->getUserAndStoreLocally('scottp99@gmail.com');

        $this->assertInstanceOf(User::class, $return);
    }

    public function encrypt($password)
    {
        $options = ['cost' => 12];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    public function getMemberProfile()
    {
        $memberProfile = new MemberProfile();

        $data = [
            'cardNumber'        => '18329100294751',
            'dateOfBirth'       => '19/06/1985',
            'emailAddress'      => 'scottp99@gmail.com',
            'firstName'         => 'Scott',
            'lastName'          => 'Pringle',
            'memberAccountId'   => '11579012',
            'otherPhoneNumber'  => '',
            'phoneNumber'       => null,
            'postalCode'        => 'EH18 1DE',
            'profileExists'     => '1',
        ];

        $memberProfile->setData($data);

        $memberProfile->updatePassword('$2y$12$Q8aEur80noumy60ivlIm9eE/0aeUsAnQSCSAWTgcN6UwkM7Z6AjVe');

        return $memberProfile;
    }
}
