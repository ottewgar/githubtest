<?php

namespace AppBundle\Tests\Api\NcrRadiantUserManager\Manager;

use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Api\Paytronix\Paytronix;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class NcrRadiantUserManagerTest extends KernelTestCase
{
    private $userManager;

    const DEFAULT_USER = "scottp99@gmail.com";

    protected function setUp()
    {
        self::bootKernel();
        $this->userManager = static::$kernel->getContainer()->get('nandos.ncr_radiant.user_manager');

        $this->userManager->updateProfilePassword(self::DEFAULT_USER, $this->userManager->hashPassword('passwordABC'), true);

    }

    public function testGetProfileByEmail()
    {
        $response = $this->userManager->getProfileByEmail(self::DEFAULT_USER);

        $this->assertInstanceOf(MemberProfile::class, $response);
        $this->assertEquals(self::DEFAULT_USER, $response->getEmailAddress());
    }

    public function testPasswordMatchesProfilePassword()
    {
        $profile = $this->userManager->getProfileByEmail(self::DEFAULT_USER);
        $response = $this->userManager->passwordMatchesProfilePassword($profile, '12345');

        $this->assertEquals(false, $response);

        $response = $this->userManager->passwordMatchesProfilePassword($profile, 'passwordABC');
        $this->assertEquals(true, $response);

        // could not test MD5 because MD5 will not save
//        $response = $this->userManager->updateProfilePassword(self::DEFAULT_USER, md5('md5password'), true);
//        $profile = $this->userManager->getProfileByEmail(self::DEFAULT_USER);
//        $response = $this->userManager->passwordMatchesProfilePassword($profile, 'md5password');
//        $this->assertEquals(true, $response);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->userManager = null; // avoid memory leaks
    }
}
