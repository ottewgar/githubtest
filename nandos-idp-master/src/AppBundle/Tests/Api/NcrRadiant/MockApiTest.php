<?php

namespace AppBundle\Tests\Api\NcrRadiant;

use AppBundle\Api\NcrRadiant\NcrRadiantApi;
use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Api\Paytronix\PaytronixApi;
use AppBundle\Entity\User;
use TestBundle\Test\CustomWebTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MockApiTest extends CustomWebTestCase
{
    protected $client;
    const DEFAULT_EMAIL = "ncrtest@gmail.com";

    protected function setUp()
    {
        $this->init();
        $this->client = $this->createClient();
        $this->afterInit();
    }


    /**
     * Delete the user if it exists in IDP then tries to login to API
     * This will recreate the user as the login credentials are a match to the NCR user
     */
    public function testLoginWithUserOnlyInNCR()
    {
        $this->deleteUser(self::DEFAULT_EMAIL);

        $this->ncrApi->expects($this->exactly(2))->method('getProfileByEmail')->will($this->returnValue($this->getMemberProfile()));

        $params = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'rightpassword',
        ];

        $params = array_merge($this->getClientParams(), $params);

        $this->client->request('POST', '/oauth/v2/token', $params);

        $response = $this->client->getResponse();

        $json = json_decode($response->getContent(), true);

        $this->assertEquals(true, array_key_exists('access_token', $json), 'Access token key should exist in the array');

    }

    /**
     * Test when the IDP password entered is incorrect but it matches what is stored in the card system
     * This should update the IDP user's password and log the user in
     */
    public function testLoginApiFallbackNcr()
    {
        $this->getUser(self::DEFAULT_EMAIL, 'wrongpassword');

        $this->ncrApi->expects($this->exactly(2))->method('getProfileByEmail')->will($this->returnValue($this->getMemberProfile()));

        $params = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'rightpassword',
        ];

        $params = array_merge($this->getClientParams(), $params);

        $this->client->request('POST', '/oauth/v2/token', $params);

        $response = $this->client->getResponse();

        $json = json_decode($response->getContent(), true);

        $this->assertEquals(true, array_key_exists('access_token', $json), 'access token key should exist in the array');

    }

    /**
     * Tests resetting password when the user is in the card system but not IDP
     */
    public function testRequestPasswordResetWhenInCS()
    {
        $this->deleteUser(self::DEFAULT_EMAIL);

        $resetParams = [
            'email' => self::DEFAULT_EMAIL,
        ];

        $this->ncrApi->expects($this->once())->method('getProfileByEmail')->will($this->returnValue($this->getMemberProfile()));

        $this->client->request('POST', '/api/v1/send-password-reset-email', $resetParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $user = $this->getUser(self::DEFAULT_EMAIL, 'rightpassword');

        $resetConfirmParams = [
            'password' => "newpassword",
        ];

        $this->client->request('POST', "/api/v1/token/{$user->getConfirmationToken()}/reset-password", $resetConfirmParams);
        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals("Password updated", $responseArray['message'], 'access token key should exist in the array');

        $loginParams = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'newpassword',
        ];

        $loginParams = array_merge($this->getClientParams(), $loginParams);
        $this->client->request('POST', '/oauth/v2/token', $loginParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);
        $this->assertEquals(true, array_key_exists('access_token', $responseArray), 'access token key should exist in the array');

    }

    /**
     * Tests password reset when the email doesnt exist
     */
    public function testResetPasswordEmailDoesntExist()
    {
        $this->deleteUser(self::DEFAULT_EMAIL);

        $resetParams = [
            'email' => self::DEFAULT_EMAIL,
        ];

        $this->ncrApi->expects($this->once())->method('getProfileByEmail')->will($this->returnValue(null));

        $this->client->request('POST', '/api/v1/send-password-reset-email', $resetParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals("user_not_found", $responseArray['message'], 'check user doesnt exist');

    }

    /**
     * @return MemberProfile
     */
    public function getMemberProfile()
    {
        $memberProfile = new MemberProfile();

        $data = [
            'cardNumber'        => '18329100294751',
            'dateOfBirth'       => '19/06/1985',
            'emailAddress'      => self::DEFAULT_EMAIL,
            'firstName'         => 'Scott',
            'lastName'          => 'Pringle',
            'memberAccountId'   => '11579012',
            'otherPhoneNumber'  => '',
            'phoneNumber'       => null,
            'postalCode'        => 'EH18 1DE',
            'profileExists'     => '1',
        ];

        $memberProfile->setData($data);

        $memberProfile->updatePassword($this->encrypt("rightpassword"));

        return $memberProfile;
    }
}