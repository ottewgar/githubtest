<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 05/08/16
 * Time: 11:06
 */

namespace AppBundle\Tests\Api\Paytronix\Manager;

use AppBundle\Api\Paytronix\Paytronix;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PaytronixUserManagerTest extends KernelTestCase
{
    private $userManager;

    const DEFAULT_USER = "adapticd@gmail.com";
    const DEFAULT_USER2 = "scott@adapticdesign.com";

    protected function setUp()
    {
        self::bootKernel();
        $this->userManager = static::$kernel->getContainer()->get('nandos.paytronix.user_manager');

        $response = $this->userManager->updateProfilePassword(self::DEFAULT_USER, $this->userManager->hashPassword('12345678'), true);
    }

    public function testGetPaytronixAccountStatus()
    {
        $response = $this->userManager->getPaytronixAccountStatus('restTest11');
        $this->assertEquals(Paytronix::ACCOUNT_STATUS_NONE, $response);

        $response = $this->userManager->getPaytronixAccountStatus(self::DEFAULT_USER);
        $this->assertEquals(Paytronix::ACCOUNT_STATUS_ACTIVE, $response);
    }

    public function testPasswordMatchesProfilePassword()
    {
        $profile = $this->userManager->getPaytronixProfile(self::DEFAULT_USER);
        $response = $this->userManager->passwordMatchesProfilePassword($profile, '12345');
        $this->assertEquals(false, $response);

        $response = $this->userManager->passwordMatchesProfilePassword($profile, '12345678');
        $this->assertEquals(true, $response);

        // test MD5 password
        $response = $this->userManager->updateProfilePassword(self::DEFAULT_USER, md5('md5password'), true);
        $profile = $this->userManager->getPaytronixProfile(self::DEFAULT_USER);
        $response = $this->userManager->passwordMatchesProfilePassword($profile, 'md5password');
        $this->assertEquals(true, $response);
    }

    public function testUpdateProfilePassword()
    {
        $response = $this->userManager->updateProfilePassword(self::DEFAULT_USER, $this->userManager->hashPassword('password'), true);
        $this->assertEquals("success", $response['result']);

        // test password change has worked on Paytronix
        $profile = $this->userManager->getPaytronixProfile(self::DEFAULT_USER);
        $response = $this->userManager->passwordMatchesProfilePassword($profile, 'password');
        $this->assertEquals(true, $response);


        // change password back to starting password and retest
        $response = $this->userManager->updateProfilePassword(self::DEFAULT_USER, $this->userManager->hashPassword('12345678'), true);
        $this->assertEquals("success", $response['result']);

        $profile = $this->userManager->getPaytronixProfile(self::DEFAULT_USER);
        $response = $this->userManager->passwordMatchesProfilePassword($profile, '12345678');
        $this->assertEquals(true, $response);
    }

    public function testUpdateUsername()
    {
        // change username to email
        $response = $this->userManager->updateUsername(self::DEFAULT_USER, self::DEFAULT_USER2);
//        die(var_dump($response));
        $this->assertEquals("success", $response['result']);

        // check old username is incorrect
        $response = $this->userManager->getPaytronixAccountStatus(self::DEFAULT_USER);
        $this->assertEquals(Paytronix::ACCOUNT_STATUS_NONE, $response);

        // check new username is correct
        $response = $this->userManager->getPaytronixAccountStatus(self::DEFAULT_USER2);
        $this->assertEquals(Paytronix::ACCOUNT_STATUS_ACTIVE, $response);

        // change username back to original and test
        $response = $this->userManager->updateUsername(self::DEFAULT_USER2, self::DEFAULT_USER);
        $this->assertEquals("success", $response['result']);

        $response = $this->userManager->getPaytronixAccountStatus(self::DEFAULT_USER2);
        $this->assertEquals(Paytronix::ACCOUNT_STATUS_NONE, $response);

        $response = $this->userManager->getPaytronixAccountStatus(self::DEFAULT_USER);
        $this->assertEquals(Paytronix::ACCOUNT_STATUS_ACTIVE, $response);

    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->userManager = null; // avoid memory leaks
    }
}