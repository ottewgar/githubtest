<?php

namespace AppBundle\Tests\Api\Paytronix\Manager;

use Ambta\DoctrineEncryptBundle\Encryptors\Rijndael256Encryptor;
use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Api\NcrRadiant\Manager\NcrRadiantUserManager;
use AppBundle\Api\Paytronix\Manager\PaytronixUserManager;
use AppBundle\Api\Paytronix\Paytronix;
use AppBundle\Api\Paytronix\PaytronixApi;
use AppBundle\Api\Paytronix\PaytronixConfig;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\OAuth\OAuthStorage;
use AppBundle\Util\NandosCardConfig;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use FOS\OAuthServerBundle\Model\AuthCodeManagerInterface;
use FOS\OAuthServerBundle\Model\Client;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenManagerInterface;
use FOS\UserBundle\Security\UserProvider;
use FOS\UserBundle\Util\CanonicalizerInterface;
use OAuth2\Model\IOAuth2Client;
use OAuth2\OAuth2Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class PaytronixUserManagerMockTest extends \PHPUnit_Framework_TestCase
{
    protected $api;
    protected $userManager;
    protected $config;
    protected $paytronixUserManager;

    protected function setUp()
    {
        $this->api = $this->getMockBuilder(PaytronixApi::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->userManager = $this->getMockBuilder(UserManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->config = $this->getMockBuilder(PaytronixConfig::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->paytronixUserManager = new PaytronixUserManager($this->api, $this->userManager, $this->config);
    }

    public function testPasswordMatchesProfilePassword()
    {
        $this->config->method('getPasswordField')->will($this->returnValue('custom4'));

        $encryptedPassword = $this->encrypt('password');

        $profile = [
            'fields' => [
                'custom4' => $encryptedPassword,
            ]
        ];

        $return = $this->paytronixUserManager->passwordMatchesProfilePassword($profile, 'password');

        $this->assertEquals(true, $return);
    }

    public function testPasswordMatchesProfilePasswordMd5()
    {
        $this->config->method('getPasswordField')->will($this->returnValue('custom4'));

        $encryptedPassword = md5('password');

        $profile = [
            'fields' => [
                'custom4' => $encryptedPassword,
                'username' => 'scott@quizdigital.com',
            ]
        ];

        $this->api->expects($this->once())->method('updatePassword')->will($this->returnValue(false));

        $return = $this->paytronixUserManager->passwordMatchesProfilePassword($profile, 'password');

        $this->assertEquals(true, $return);
    }

    public function testGetAuthenticatedUserAndStoreLocally()
    {
        $this->config->method('getPasswordField')->will($this->returnValue('custom4'));

        $this->api
            ->expects($this->exactly(2))
            ->method('getUserInformation')
            ->will($this->returnValue($this->getTestProfile()));

        $this->userManager->expects($this->once())->method('updateUser')->will($this->returnValue(true));

        $return = $this->paytronixUserManager->getAuthenticatedUserAndStoreLocally('scott@quizdigital.com', 'password');

        $this->assertInstanceOf(User::class, $return);
    }

    public function testGetUserAndStoreLocally()
    {
        $this->api
            ->expects($this->once())
            ->method('getUserInformation')
            ->will($this->returnValue($this->getTestProfile()));

        $return = $this->paytronixUserManager->getUserAndStoreLocally('scott@quizdigital.com');

        $this->assertInstanceOf(User::class, $return);
    }

    public function encrypt($password)
    {
        $options = ['cost' => 12];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    public function getTestProfile()
    {
        return [
            'result' => 'success',
            'fields' => [
                'firstName' => 'Scott',
                'lastName'  => 'Pringle',
                'username'  => 'scott@quizdigital.com',
                'custom4'   => $this->encrypt('password'),
            ]
        ];
    }
}
