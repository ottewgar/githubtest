<?php

namespace AppBundle\Tests\Api\Paytronix;

use AppBundle\Api\NcrRadiant\NcrRadiantApi;
use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Api\Paytronix\PaytronixApi;
use AppBundle\Entity\User;
use TestBundle\Test\CustomWebTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MockApiTest extends CustomWebTestCase
{
    protected $client;

    const DEFAULT_EMAIL = "paytronixtest@gmail.com";

    protected function setUp()
    {
        $this->init();
        $this->client = $this->createClient();
        $this->afterInit();
    }

    /**
     * Delete the user if it exists in IDP then tries to login to API
     * This will recreate the user as the login credentials are a match to the NCR user
     */
    public function testLoginWithUserOnlyInPaytronix()
    {
        $this->deleteUser(self::DEFAULT_EMAIL);

        $this->paytronixApi->expects($this->exactly(2))->method('getUserInformation')->will($this->returnValue($this->getTestProfile()));

        $params = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'rightpassword',
        ];

        $params = array_merge($this->getClientParams(), $params);

        $this->client->request('POST', '/oauth/v2/token', $params);

        $response = $this->client->getResponse();

        $json = json_decode($response->getContent(), true);

        $this->assertEquals(true, array_key_exists('access_token', $json));

    }

    /**
     * Test when login credentials entered to not match IDP but do work for
     * Paytronix API
     */
    public function testLoginApiFallbackPaytronix()
    {
        $this->getUser(self::DEFAULT_EMAIL, 'wrongpassword');

        $this->paytronixApi->expects($this->exactly(2))->method('getUserInformation')->will($this->returnValue($this->getTestProfile()));

        $params = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'rightpassword',
        ];

        $params = array_merge($this->getClientParams(), $params);

        $this->client->request('POST', '/oauth/v2/token', $params);

        $response = $this->client->getResponse();

        $json = json_decode($response->getContent(), true);

        $this->assertEquals(true, array_key_exists('access_token', $json));

    }

    /**
     * Tests the password reset when the user only exists in a card system
     */
    public function testRequestPasswordResetWhenInCS()
    {
        $this->deleteUser(self::DEFAULT_EMAIL);

        $resetParams = [
            'email' => self::DEFAULT_EMAIL,
        ];

        $this->paytronixApi->expects($this->once())->method('getUserInformation')->will($this->returnValue($this->getTestProfile()));

        $this->client->request('POST', '/api/v1/send-password-reset-email', $resetParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $user = $this->getUser(self::DEFAULT_EMAIL, 'rightpassword');

        $resetConfirmParams = [
            'password' => "newpassword",
        ];

        $this->client->request('POST', "/api/v1/token/{$user->getConfirmationToken()}/reset-password", $resetConfirmParams);
        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals("Password updated", $responseArray['message'], 'access token key should exist in the array');

        $loginParams = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'newpassword',
        ];

        $loginParams = array_merge($this->getClientParams(), $loginParams);
        $this->client->request('POST', '/oauth/v2/token', $loginParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);
        $this->assertEquals(true, array_key_exists('access_token', $responseArray), 'access token key should exist in the array');

    }

    /**
     * Tests password reset when the email doesnt exist in card system or IDP
     */
    public function testPasswordResetWhenEmailDoesntExist()
    {
        $this->deleteUser(self::DEFAULT_EMAIL);

        $resetParams = [
            'email' => self::DEFAULT_EMAIL,
        ];

        $this->paytronixApi->expects($this->once())->method('getUserInformation')->will($this->returnValue(null));

        $this->client->request('POST', '/api/v1/send-password-reset-email', $resetParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals("user_not_found", $responseArray['message'], 'check user doesnt exist');

    }

    /**
     * @return array
     */
    public function getTestProfile()
    {
        return [
            'result' => 'success',
            'fields' => [
                'firstName' => 'Scott',
                'lastName'  => 'Pringle',
                'username'  => self::DEFAULT_EMAIL,
                'custom4'   => $this->encrypt('rightpassword'),
            ]
        ];
    }
}