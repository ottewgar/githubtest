<?php

namespace AppBundle\Tests\Api\Idp;

use AppBundle\Api\NcrRadiant\NcrRadiantApi;
use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Api\Paytronix\PaytronixApi;
use AppBundle\Entity\User;
use TestBundle\Test\CustomWebTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MockApiTest extends CustomWebTestCase
{
    protected $client;
    const DEFAULT_EMAIL = "idptest@gmail.com";

    protected function setUp()
    {
        $this->init();
        $this->client = $this->createClient();
        $this->afterInit();
    }

    /**
     * This will testing a user logging into the API and then using the given access token to get the user info
     */
    public function testLoginOnIDPWithAccessToken()
    {
        $this->getUser('scott@quizdigital.com', 'password');
        $this->getUser(self::DEFAULT_EMAIL, 'rightpassword');

        $params = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'rightpassword',
        ];

        $params = array_merge($this->getClientParams(), $params);

        $this->client->request('POST', '/oauth/v2/token', $params);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);
        $this->assertEquals(true, array_key_exists('access_token', $responseArray), 'access token key should exist in the array');

        $headers = array(
            'HTTP_AUTHORIZATION' => "Bearer {$responseArray['access_token']}",
            'CONTENT_TYPE' => 'application/json',
        );

        $this->client->request('GET', '/api/v1/user', $params, [], $headers);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals(self::DEFAULT_EMAIL, $responseArray['email'], 'Check the returned email address is the same as the original user');

    }

    /**
     * Tests if login credentials are not correct
     */
    public function testLoginFail()
    {
        $this->getUser(self::DEFAULT_EMAIL, 'rightpassword');

        $params = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'wrongpassword',
        ];

        $params = array_merge($this->getClientParams(), $params);

        $this->client->request('POST', '/oauth/v2/token', $params);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals('invalid_grant', $responseArray['error'], 'access token key should exist in the array');

    }

    /**
     * Logs into APi and changes the logged in users password
     */
    public function testLoginAndChangePasswordIDP()
    {
        $this->getUser(self::DEFAULT_EMAIL, 'rightpassword');

        $loginParams = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'rightpassword',
        ];

        $loginParams = array_merge($this->getClientParams(), $loginParams);

        // log in to get access token
        $this->client->request('POST', '/oauth/v2/token', $loginParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);
        $this->assertEquals(true, array_key_exists('access_token', $responseArray), 'access token key should exist in the array');

        $changePasswordParams = [
            'current_password' => 'rightpassword',
            'new_password' => 'newpassword',
        ];

        $headers = array(
            'HTTP_AUTHORIZATION' => "Bearer {$responseArray['access_token']}",
            'CONTENT_TYPE' => 'application/json',
        );

        // change the password
        $this->client->request('POST', '/api/v1/user/change-password', $changePasswordParams, [], $headers);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals('Password updated', $responseArray['message'], 'Check the password has changed');

        // try login again with new password
        $loginParams['password'] = 'newpassword';
        $this->client->request('POST', '/oauth/v2/token', $loginParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals(true, array_key_exists('access_token', $responseArray), 'access token key should exist in the array');
    }

    /**
     * Tests requesting a reset password and then changing the password
     */
    public function testRequestPasswordResetToken()
    {
        $this->getUser(self::DEFAULT_EMAIL, 'rightpassword');

        $resetParams = [
            'email' => self::DEFAULT_EMAIL,
        ];

        $this->client->request('POST', '/api/v1/send-password-reset-email', $resetParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $user = $this->getUser(self::DEFAULT_EMAIL, 'rightpassword');

        $resetConfirmParams = [
            'password' => "newpassword",
        ];

        $this->client->request('POST', "/api/v1/token/{$user->getConfirmationToken()}/reset-password", $resetConfirmParams);
        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals("Password updated", $responseArray['message'], 'access token key should exist in the array');

        $loginParams = [
            'grant_type' => "password",
            'username' => self::DEFAULT_EMAIL,
            'password' => 'newpassword',
        ];

        $loginParams = array_merge($this->getClientParams(), $loginParams);
        $this->client->request('POST', '/oauth/v2/token', $loginParams);

        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);
        $this->assertEquals(true, array_key_exists('access_token', $responseArray), 'access token key should exist in the array');

    }

    /**
     * Tests when the confirmation code is incorrect
     */
    public function testWrongConfirmationCode()
    {
        $user = $this->getUser(self::DEFAULT_EMAIL, 'rightpassword');

        $resetConfirmParams = [
            'password' => "newpassword",
        ];

        $this->client->request('POST', "/api/v1/token/wrongcodehere/reset-password", $resetConfirmParams);
        $response = $this->client->getResponse();
        $responseArray = json_decode($response->getContent(), true);

        $this->assertEquals("user_not_found", $responseArray['message'], 'test a wrong confirmation code will not find a user');

    }
}