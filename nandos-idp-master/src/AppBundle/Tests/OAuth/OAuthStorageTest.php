<?php

namespace AppBundle\Tests\OAuth;

use Ambta\DoctrineEncryptBundle\Encryptors\Rijndael256Encryptor;
use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Api\NcrRadiant\Manager\NcrRadiantUserManager;
use AppBundle\Api\Paytronix\Manager\PaytronixUserManager;
use AppBundle\Api\Paytronix\Paytronix;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\OAuth\OAuthStorage;
use AppBundle\Test\FixturesTrait;
use AppBundle\Util\NandosCardConfig;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use FOS\OAuthServerBundle\Model\AuthCodeManagerInterface;
use FOS\OAuthServerBundle\Model\Client;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenManagerInterface;
use FOS\UserBundle\Security\UserProvider;
use FOS\UserBundle\Util\CanonicalizerInterface;
use OAuth2\Model\IOAuth2Client;
use OAuth2\OAuth2Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class OAuthStorageTest extends \PHPUnit_Framework_TestCase
{
    protected $om;

    protected $repository;

    protected $userManager;

    protected $oAuthStorage;

    protected $cardUserManagerContainer;

    protected $userProvider;

    protected $ec;

    protected $encoder;

    protected $oAuthClient;


    protected function setUp()
    {
        $this->userManager = $this->getMockBuilder(UserManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->cardUserManagerContainer = $this->getMockBuilder(CardUserManagerContainer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->oAuthClient = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([1])
            ->getMock();

        $cm = $this->getMock(ClientManagerInterface::class);
        $atm = $this->getMock(AccessTokenManagerInterface::class);
        $rtm = $this->getMock(RefreshTokenManagerInterface::class);
        $acm = $this->getMock(AuthCodeManagerInterface::class);

        $this->userProvider = $this->getMockBuilder(UserProvider::class)
            ->setConstructorArgs([$this->userManager])
            ->getMock();

        $this->ec = $this->getMockBuilder(EncoderFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->encoder = $this->getMockBuilder(BCryptPasswordEncoder::class)
            ->disableOriginalConstructor()
            ->getMock();


        $this->oAuthStorage = new OAuthStorage($cm, $atm, $rtm, $acm, $this->userProvider, $this->ec);
        $this->oAuthStorage->setCardUserManagerContainer($this->cardUserManagerContainer);

    }

    /**
     * This will test the checkUserCredentials method when the user exists and the password is correct
     */
    public function testCheckUserCredentials()
    {
        $user = new User();
        $user->setEnabled(1);

        $this->userProvider->method('loadUserByUsername')->will($this->returnValue($user));

        $this->encoder->method('isPasswordValid')->will($this->returnValue(true));
        $this->ec->method('getEncoder')->will($this->returnValue($this->encoder));

        $return = $this->oAuthStorage->checkUserCredentials($this->oAuthClient, "scott@quizdigital.com", 'password');

        $this->assertInstanceOf(User::class, $return['data']);
    }


    /**
     * Test output when user is not found in IDP or card system
     */
    public function testCheckUserCredentialsNoUser()
    {
        $this->cardUserManagerContainer->method('getAuthenticatedUserFromCardSystemAndStoreLocally')->will($this->returnValue(false));

        $this->userProvider->method('loadUserByUsername')->will($this->throwException(new AccessDeniedException('Access Denied')));

        $return = $this->oAuthStorage->checkUserCredentials($this->oAuthClient, "scott@quizdigital.com", 'password');

        $this->assertEquals(false, $return);
    }

    /**
     * Tests when user exists in card system but not in IDP
     */
    public function testCheckUserCredentialsWhenNotInIDPButInCS()
    {
        $this->cardUserManagerContainer->method('getAuthenticatedUserFromCardSystemAndStoreLocally')->will($this->returnValue(false));

        $this->userProvider->method('loadUserByUsername')->will($this->throwException(new UsernameNotFoundException('User not found')));

        $return = $this->oAuthStorage->checkUserCredentials($this->oAuthClient, "scott@quizdigital.com", 'password');

        $this->assertEquals(false, $return);
    }
}
