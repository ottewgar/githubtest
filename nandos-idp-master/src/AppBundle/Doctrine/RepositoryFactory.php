<?php

namespace AppBundle\Doctrine;

use Doctrine\ORM\Repository\RepositoryFactory as RepositoryFactoryInterface;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RepositoryFactory implements RepositoryFactoryInterface {


    private $container;

    /**
     * @param Container $container
     */

    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * The list of EntityRepository instances.
     *
     * @var \Doctrine\Common\Persistence\ObjectRepository[]
     */
    private $repositoryList = [];

    /**
     * A mapping of FQCN => service name
     * @var array
     */

    private $entityManagerRepositoryClassServiceId = [];

    public function setEntityManagerRepositoryClassServiceId($entityManagerRepositoryClass, $serviceId)
    {
        $this->entityManagerRepositoryClassServiceId[$entityManagerRepositoryClass] = $serviceId;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository(EntityManagerInterface $entityManager, $entityName)
    {
        $repositoryHash = $entityManager->getClassMetadata($entityName)->getName() . spl_object_hash($entityManager);

        if (isset($this->repositoryList[$repositoryHash])) {
            return $this->repositoryList[$repositoryHash];
        }

        return $this->repositoryList[$repositoryHash] = $this->createRepository($entityManager, $entityName);
    }

    /**
     * Create a new repository instance for an entity class.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager The EntityManager instance.
     * @param string                               $entityName    The name of the entity.
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function createRepository(EntityManagerInterface $entityManager, $entityName)
    {

        /* @var $metadata \Doctrine\ORM\Mapping\ClassMetadata */
        $metadata  = $entityManager->getClassMetadata($entityName);
        $repositoryClassName = $metadata->customRepositoryClassName
            ?: $entityManager->getConfiguration()->getDefaultRepositoryClassName();

        if (isset($this->entityManagerRepositoryClassServiceId[$repositoryClassName])) {

            $result = $this->container->get($this->entityManagerRepositoryClassServiceId[$repositoryClassName]);

        } else {

            $result = new $repositoryClassName($entityManager, $metadata);
        }

        if ($result instanceof ContainerAwareInterface) {
            $result->setContainer($this->container);
        }

        return $result;

    }

}