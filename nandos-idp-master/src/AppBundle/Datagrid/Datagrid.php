<?php

/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Datagrid;

use Sonata\AdminBundle\Admin\FieldDescriptionCollection;
use Sonata\AdminBundle\Admin\FieldDescriptionInterface;
use Sonata\AdminBundle\Datagrid\DatagridInterface;
use Sonata\AdminBundle\Filter\FilterInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormBuilder;

class Datagrid extends \Sonata\AdminBundle\Datagrid\Datagrid implements DatagridInterface
{
    protected $encryptor;

    public function setEncryptor($encryptor)
    {
        $this->encryptor = $encryptor;
    }
    /**
     * {@inheritdoc}
     */
    public function buildPager()
    {
        if ($this->bound) {
            return;
        }

        foreach ($this->getFilters() as $name => $filter) {
            list($type, $options) = $filter->getRenderSettings();

            $this->formBuilder->add($filter->getFormName(), $type, $options);
        }

        $this->formBuilder->add('_sort_by', 'hidden');
        $this->formBuilder->get('_sort_by')->addViewTransformer(new CallbackTransformer(
            function ($value) { return $value; },
            function ($value) { return $value instanceof FieldDescriptionInterface ? $value->getName() : $value; }
        ));

        $this->formBuilder->add('_sort_order', 'hidden');
        $this->formBuilder->add('_page', 'hidden');
        $this->formBuilder->add('_per_page', 'hidden');

        $this->form = $this->formBuilder->getForm();
        $this->form->submit($this->values);

        $data = $this->form->getData();

        $fields = [];
        foreach ($this->getColumns()->getElements() as $column) {
            if ($class = $column->getAdmin()->getClass()) {
                $fields = $this->encryptor->getClassProperties($class);
                continue;
            }
        }

        foreach ($this->getFilters() as $name => $filter) {
            $this->values[$name] = isset($this->values[$name]) ? $this->values[$name] : null;

            if (in_array($name, $fields)) {
                // this is the custom code to encrypt certain fields
                $data[$name]['value'] = $this->encryptor->encrypt($data[$name]['value']);
            }

            $filter->apply($this->query, $data[$name]);
        }

        if (isset($this->values['_sort_by'])) {
            if (!$this->values['_sort_by'] instanceof FieldDescriptionInterface) {
                throw new UnexpectedTypeException($this->values['_sort_by'], 'FieldDescriptionInterface');
            }

            if ($this->values['_sort_by']->isSortable()) {
                $this->query->setSortBy($this->values['_sort_by']->getSortParentAssociationMapping(), $this->values['_sort_by']->getSortFieldMapping());
                $this->query->setSortOrder(isset($this->values['_sort_order']) ? $this->values['_sort_order'] : null);
            }
        }

        $maxPerPage = 25;
        if (isset($this->values['_per_page'])) {
            // check for `is_array` can be safely removed if php 5.3 support will be dropped
            if (is_array($this->values['_per_page'])) {
                if (isset($this->values['_per_page']['value'])) {
                    $maxPerPage = $this->values['_per_page']['value'];
                }
            } else {
                $maxPerPage = $this->values['_per_page'];
            }
        }
        $this->pager->setMaxPerPage($maxPerPage);

        $page = 1;
        if (isset($this->values['_page'])) {
            // check for `is_array` can be safely removed if php 5.3 support will be dropped
            if (is_array($this->values['_page'])) {
                if (isset($this->values['_page']['value'])) {
                    $page = $this->values['_page']['value'];
                }
            } else {
                $page = $this->values['_page'];
            }
        }

        $this->pager->setPage($page);

        $this->pager->setQuery($this->query);
        $this->pager->init();

        $this->bound = true;
    }
}
