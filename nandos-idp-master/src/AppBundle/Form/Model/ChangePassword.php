<?php

namespace AppBundle\Form\Model;

class ChangePassword
{
    /**
     * @var string
     */
    private $newPassword;

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     * @return ChangePassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
        return $this;
    }

}

