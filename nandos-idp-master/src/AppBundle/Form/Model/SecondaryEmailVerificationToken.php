<?php

namespace AppBundle\Form\Model;


class SecondaryEmailVerificationToken
{
    /**
     * @var string
     */
    private $verificationToken;

    /**
     * @return string
     */
    public function getVerificationToken()
    {
        return $this->verificationToken;
    }

    /**
     * @param string $verificationToken
     * @return SecondaryEmailVerificationToken
     */
    public function setVerificationToken($verificationToken)
    {
        $this->verificationToken = $verificationToken;
        return $this;
    }

}