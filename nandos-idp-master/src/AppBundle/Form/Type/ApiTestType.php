<?php
namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApiTestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('api', 'choice', [
                'attr' => ['class' => 'api_choice'],
                'mapped' => false,
                'choices' => [
                    'paytronix' => 'paytronix',
                    'ncr' => 'ncr'
                ]])
            ->add('Test', 'button', [
                'attr' => ['class' => 'api_test_button',]
            ]);
    }

    public function getName()
    {
        return "apitest_formtype";
    }
}