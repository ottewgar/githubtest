<?php

namespace AppBundle\Form\Type\Api;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('current_password', 'text',
            [
                'label'       => 'Current password',
                'mapped'      => false,
                'constraints' => new UserPassword()
            ]
        );

        $builder->add('new_password', 'text',
            [
                'label'         => 'New password',
                'property_path' => 'newPassword'
            ]
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'AppBundle\Form\Model\ChangePassword',
            'intention'       => 'change_password',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'change_password';
    }
}
