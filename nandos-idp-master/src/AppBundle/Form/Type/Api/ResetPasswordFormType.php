<?php

namespace AppBundle\Form\Type\Api;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResetPasswordFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', null, ['property_path' => 'plainPassword', 'description' => 'Unencrypted password. Will be strongly encrypted.'])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\User',
            'validation_groups'  => ['api_reset_password'],
            'csrf_protection'    => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'reset_password';
    }
}
