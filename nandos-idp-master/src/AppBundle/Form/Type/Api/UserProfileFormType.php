<?php

namespace AppBundle\Form\Type\Api;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Regex;

class UserProfileFormType extends AbstractType
{

    const NAME = 'user';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null, ['label' => 'First name'])
            ->add('lastname', null, ['label' => 'Last name'])
            ->add('email', 'email', ['label' => 'Email address']);

        $builder->get('email')
            ->addModelTransformer(new CallbackTransformer(
                function ($originalEmail) {
                    return strtolower($originalEmail);
                },
                function ($originalEmail) {
                    return strtolower($originalEmail);
                }
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\User',
            'validation_groups'  => ['api_user_profile'],
            'csrf_protection'    => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return static::NAME;
    }
}
