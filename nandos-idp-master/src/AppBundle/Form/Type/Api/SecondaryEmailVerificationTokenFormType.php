<?php

namespace AppBundle\Form\Type\Api;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

/**
 * Class SecondaryEmailOperationFormType
 * @package AppBundle\Form\Type\Api
 */

class SecondaryEmailVerificationTokenFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('verification_token', null, ['label' => 'Verification token']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'AppBundle\Form\Model\SecondaryEmailVerificationToken',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'secondary_email_verification_token';
    }
}
