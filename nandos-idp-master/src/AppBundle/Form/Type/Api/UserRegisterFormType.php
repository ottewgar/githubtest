<?php

namespace AppBundle\Form\Type\Api;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Regex;

class UserRegisterFormType extends AbstractType
{

    const NAME = 'user';

    /**
     *
     * These are handled in UserRegisterFormHandler
     *
     * Potentially these could be added as a custom UserMetafield Form Type to the form below
     * but for now the register form only has one such field
     * @var array
     */
    private static $metadataFields = [
        'nandos_card_number' => ['key' => 'unconfirmed_nandos_card_number', 'format' => 'string', 'encrypted' => true]
    ];

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null, ['label' => 'First name'])
            ->add('lastname', null, ['label' => 'Last name'])
            ->add('email', 'email', ['label' => 'Email address'])
            ->add('email_template', 'text', [
                'mapped'      => false,
                'required'    => false,
            ])
            ->add('nandos_card_number', 'text', [
                'mapped'      => false,
                'required'    => false,
                'label'       => "Nando's card Number",
                'constraints' => [new Regex(['pattern' => '#^[0-9 ]+$#', 'groups' => ['api_register']])]
            ]);

        $builder->get('email')
            ->addModelTransformer(new CallbackTransformer(
                function ($originalEmail) {
                    return strtolower($originalEmail);
                },
                function ($originalEmail) {
                    return strtolower($originalEmail);
                }
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\User',
            'validation_groups'  => ['api_register'],
            'csrf_protection'    => false
        ));
    }

    /**
     * @return array
     */
    public static function getMetadataFields()
    {
        return static::$metadataFields;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return static::NAME;
    }
}
