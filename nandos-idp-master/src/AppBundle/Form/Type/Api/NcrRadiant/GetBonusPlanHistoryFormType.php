<?php

namespace AppBundle\Form\Type\Api\NcrRadiant;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

class GetBonusPlanHistoryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('start_date',            'text',    [ 'label' => 'Start date', 'property_path' => 'startDate' ])
            ->add('end_date',              'text',    [ 'label' => 'End date',   'property_path' => 'endDate' ])
            ->add('number_of_assignments', 'integer', [ 'label' => 'Number of assignments', 'property_path' => 'numberOfAssignments' ])
            ->add('number_of_days',        'integer', [ 'label' => 'Number of days', 'property_path' => 'numberOfDays' ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'AppBundle\Api\NcrRadiant\Request\GetBonusPlanHistoryRequest',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'get_bonus_plan_history';
    }

}