<?php

namespace AppBundle\Form\Type\Api;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

/**
 * General form type to capture a correctly formatted address.
 * It does not check if the email address exists in the database or any system, this should be done by the specific form handler.
 * Do not extend this class any further. If your form needs additional functionality then create a new Form Type.
 *
 * Class EmailFormType
 * @package AppBundle\Form\Type\Api
 */

class EmailFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, ['label' => 'Email address']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'AppBundle\Form\Model\Email',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'email';
    }
}
