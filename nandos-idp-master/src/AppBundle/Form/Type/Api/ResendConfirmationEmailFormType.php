<?php

namespace AppBundle\Form\Type\Api;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class ResendConfirmationEmailFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, ['label' => 'Email address'])
            ->add('email_template', 'text', [
                'mapped'      => false,
                'required'    => false,
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\User',
            'validation_groups'  => ['api_resend_confirmation_email'],
            'csrf_protection'    => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'resend_confirmation_email';
    }

}