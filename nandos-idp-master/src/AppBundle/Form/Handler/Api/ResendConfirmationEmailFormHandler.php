<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Email\NandosMailer;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;

use AppBundle\Form\Type\Api\ResendConfirmationEmailFormType;
use AppBundle\Util\NandosCardConfig;
use AppBundle\Util\Sanitizer;
use FOS\RestBundle\View\View;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ResendConfirmationEmailFormHandler extends AbstractFormHandler
{

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var NandosMailer
     */
    private $mailer;

    private $cardConfig;

    /**
     * Create a new ResendConfirmationEmailFormHandler
     *
     * @param UserManagerInterface $userManager
     */
    function __construct(UserManagerInterface $userManager, NandosMailer $mailer, NandosCardConfig $cardConfig)
    {
        $this->userManager = $userManager;
        $this->mailer      = $mailer;
        $this->cardConfig = $cardConfig;
    }

    /**
     * @param Request $request
     * @return View
     */

    public function handle(Request $request)
    {

        $form = $this->getForm();

        // remove format from the request or it will be treated as an extra form field
        $request->request->remove('_format');

        $form->handleRequest($request);

        // an empty POST is treated as not submitted, so set an empty value that will force full validation
        if (!$form->isSubmitted()) {
            $form->submit(array('email' => ''));
        }

        // Most likely an invalid email address format
        if (!$form->isValid()) {
            return $this->getBasicFormErrorsResponse($form);

        }

        $user = $this->userManager->findUserByEmail($form->getData()->getEmail());

        $emailTemplate = ($form->get('email_template')) ? $form->get('email_template')->getData(): "";

        if (!$user) {
            $this->info("User not found", ['form_handler' => 'resendconfirmationemail']);
            return $this->getUserNotFoundResponse();
        }

        $sanitisedEmail = Sanitizer::sanitiseEmail($user->getUsername());

        // the user may already be activated, if so, should we send them a different email to to tell them?
        // e.g. maybe they forgot their password? Currently this call will always end up creating a confirmation_token

        $this->info("Resend confirmation token for user {$sanitisedEmail}", ['form_handler' => 'resendconfirmationemail']);
        
        $this->userManager->initialiseUserConfirmationToken($user);
        $this->userManager->updateUser($user);

        $this->mailer->sendResendConfirmationEmail($user, $emailTemplate);

        return new View([
            'code'              => Response::HTTP_OK,
            'message'           => 'Confirmation email resent',
            'required_action'   => 'check_confirmation_email',
        ], Response::HTTP_OK);

    }

    /**
     * @return Form
     */
    private function getForm()
    {
        return $this->formFactory->createNamed('', new ResendConfirmationEmailFormType(), new User());
    }

    /**
     * @return View
     */

    private function getUserNotFoundResponse()
    {
        return new View([
            'code'            => Response::HTTP_NOT_FOUND,
            'message'         => 'user_not_found',
            'required_action' => 'check_email_address',
        ], Response::HTTP_NOT_FOUND);
    }

}