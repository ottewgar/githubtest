<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Email\NandosMailer;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;

use AppBundle\Form\Type\Api\SendPasswordResetEmailFormType;
use AppBundle\Util\Sanitizer;
use FOS\RestBundle\View\View;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SendPasswordResetEmailFormHandler extends AbstractFormHandler
{

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var CardUserManagerContainer
     */
    private $cardUserManagerContainer;

    /**
     * @var NandosMailer
     */
    private $mailer;

    /**
     * SendPasswordResetEmailFormHandler constructor.
     * @param UserManagerInterface $userManager
     * @param CardUserManagerContainer $cardUserManagerContainer
     * @param NandosMailer $mailer
     */
    function __construct(UserManagerInterface $userManager, CardUserManagerContainer $cardUserManagerContainer, NandosMailer $mailer)
    {
        $this->userManager              = $userManager;
        $this->cardUserManagerContainer = $cardUserManagerContainer;
        $this->mailer                   = $mailer;
    }

    /**
     * @param Request $request
     * @return View
     */

    public function handle(Request $request)
    {

        $form = $this->getForm();

        // remove format from the request or it will be treated as an extra form field
        $request->request->remove('_format');

        $form->handleRequest($request);

        // an empty POST is treated as not submitted, so set an empty value that will force full validation
        if (!$form->isSubmitted()) {
            $form->submit(array('email' => ''));
        }

        // Most likely an invalid email address format
        if (!$form->isValid()) {

            $this->notice("Invalid form", ['form_handler' => 'sendpasswordresetemail']);

            $formErrors = $this->getFormErrorsArray($form);
            return new View($formErrors, Response::HTTP_BAD_REQUEST);

        }

        $email = $form->getData()->getEmail();
        $sanitisedEmail = Sanitizer::sanitiseEmail($email);

        $emailTemplate = ($form->get('email_template')) ? $form->get('email_template')->getData(): "";

        $user = $this->userManager->findUserByEmail($email);

        if (!$user) {

            $this->notice("User {$sanitisedEmail} not found in IDP", ['form_handler' => 'sendpasswordresetemail']);
            if (!$user = $this->cardUserManagerContainer->getUserFromCardSystemAndStoreLocally($email)) {

                $this->notice("User {$sanitisedEmail} not found in IDP or card system", ['form_handler' => 'sendpasswordresetemail']);
                return $this->getUserNotFoundResponse();
            } else {

                $this->notice("User {$sanitisedEmail} found in card system", ['form_handler' => 'sendpasswordresetemail']);
            }
        } else {

            $this->info("User {$sanitisedEmail} found in IDP", ['form_handler' => 'sendpasswordresetemail']);

        }

        // set them an expired, totally random password
        $this->userManager->initialiseExpiredRandomPassword($user);

        $this->userManager->initialiseUserConfirmationToken($user);
        $this->userManager->updateUser($user);

        $this->notice("User {$sanitisedEmail} found - sending password reset email", ['form_handler' => 'sendpasswordresetemail']);

        $this->mailer->sendPasswordResetEmail($user, $emailTemplate);

        return new View([
            'code'              => Response::HTTP_OK,
            'message'           => 'Password reset email resent',
            'required_action'   => 'check_password_reset_email',
        ], Response::HTTP_OK);

    }

    /**
     * @return Form
     */
    private function getForm()
    {
        return $this->formFactory->createNamed('', new SendPasswordResetEmailFormType(), new User());
    }

    /**
     * @return View
     */

    private function getUserNotFoundResponse()
    {
        return new View([
            'code'            => Response::HTTP_NOT_FOUND,
            'message'         => 'user_not_found',
            'required_action' => 'check_email_address',
        ], Response::HTTP_NOT_FOUND);
    }

}