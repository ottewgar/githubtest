<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Entity\Repository\SecondaryEmailOperationRepository;
use AppBundle\Entity\SecondaryEmailOperation;
use AppBundle\Entity\User;
use AppBundle\Form\Model\SecondaryEmailVerificationToken;
use AppBundle\Form\Type\Api\SecondaryEmailVerificationTokenFormType;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Extend this class to make a handler responsible for a single type of requestedOperation
 * Override various response variables as required
 *
 * Class AbstractSecondaryEmailVerificationTokenFormHandler
 * @package AppBundle\Form\Handler\Api
 */
abstract class AbstractSecondaryEmailVerificationTokenFormHandler extends AbstractFormHandler
{
    /**
     * @var string The type of operation the form handler is responsible for
     */
    protected $requestedOperation = null;

    /**
     * @var string The error message to use when secondary email operation not found
     */

    private $secondaryEmailOperationNotFoundResponseErrorMessage = 'record_not_found';

    /**
     * @var string The required action when secondary email operation not found
     */

    private $secondaryEmailOperationNotFoundResponseRequiredAction = 'check_logged_in_as_correct_user_or_request_again';

    /**
     * @var string The error message to use when secondary email operation has expired
     */

    private $secondaryEmailOperationExpiredResponseErrorMessage = 'verification_token_expired';

    /**
     * @var string The required action when secondary email operation has expired
     */

    private $secondaryEmailOperationExpiredResponseRequiredAction = 'request_operation_again';

    /**
     * @var string The success message to use when the secondary email operation was performed
     */

    private $secondaryEmailOperationSuccessfulResponseMessage = 'operation_successful';

    /**
     * @var string The required action to use when the secondary email operation was performed
     */

    private $secondaryEmailOperationSuccessfulResponseRequiredAction = null;

    /**
     * @var string The success message to use when the secondary email operation was performed
     */

    private $secondaryEmailOperationFailedResponseErrorMessage = 'operation_failed';

    /**
     * @var string The required action to use when the secondary email operation was performed
     */

    private $secondaryEmailOperationFailedResponseRequiredAction = 'retry_operation';

    /**
     * @var SecondaryEmailOperationRepository
     */
    private $secondaryEmailOperationRepository;

    /**
     * @param SecondaryEmailOperationRepository $secondaryEmailOperationRepository
     */
    public function setSecondaryEmailOperationRepository($secondaryEmailOperationRepository)
    {
        $this->secondaryEmailOperationRepository = $secondaryEmailOperationRepository;
    }

    /**
     * @param User $primaryUser
     * @param Request $request
     * @return View
     */

    public final function handle(User $primaryUser, Request $request)
    {

        $this->initialiseDefaultResponseMessages();

        $form = $this->getForm();

        // remove format from the request or it will be treated as an extra form field
        $request->request->remove('_format');

        $form->handleRequest($request);

        // an empty POST is treated as not submitted, so set an empty value that will force full validation
        if (!$form->isSubmitted()) {
            $form->submit(array('verification_token' => ''));
        }

        if (!$form->isValid()) {
            // really just no token specified
            return $this->getBasicFormErrorsResponse($form);

        }

        $verificationToken = $form->getData()->getVerificationToken();

        $secondaryEmailOperation = $this->secondaryEmailOperationRepository
            ->getOneByRequestedOperationPrimaryUserAndVerificationToken(
                $this->requestedOperation, $primaryUser, $verificationToken);

        if (!$secondaryEmailOperation) {
            return $this->getSecondaryEmailVerificationNotFoundResponse();

        }

        if ($secondaryEmailOperation->isExpired(new \DateTime())) {
            return $this->getSecondaryEmailVerificationExpiredResponse();

        }

        $success = $this->performSecondaryEmailVerificationOperation($secondaryEmailOperation);

        if ($success) {
            return $this->getSecondaryEmailVerificationOperationSuccessfulResponse();

        } else {
            return $this->getSecondaryEmailVerificationFailedResponse();
        }

    }

    protected function initialiseDefaultResponseMessages()
    {
        // override to customise response messages using setters
    }

    /**
     * @param SecondaryEmailOperation $secondaryEmailOperation
     * @return bool
     */
    abstract protected function performSecondaryEmailVerificationOperation(SecondaryEmailOperation $secondaryEmailOperation);

    /**
     * @return Form
     */

    private final function getForm()
    {
        return $this->formFactory->createNamed('', new SecondaryEmailVerificationTokenFormType(), new SecondaryEmailVerificationToken());
    }

    /**
     * @return string
     */
    public function getSecondaryEmailOperationNotFoundResponseErrorMessage()
    {
        return $this->secondaryEmailOperationNotFoundResponseErrorMessage;
    }

    /**
     * @param string $secondaryEmailOperationNotFoundResponseErrorMessage
     * @return AbstractSecondaryEmailVerificationTokenFormHandler
     */
    public function setSecondaryEmailOperationNotFoundResponseErrorMessage($secondaryEmailOperationNotFoundResponseErrorMessage)
    {
        $this->secondaryEmailOperationNotFoundResponseErrorMessage = $secondaryEmailOperationNotFoundResponseErrorMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryEmailOperationNotFoundResponseRequiredAction()
    {
        return $this->secondaryEmailOperationNotFoundResponseRequiredAction;
    }

    /**
     * @param string $secondaryEmailOperationNotFoundResponseRequiredAction
     * @return AbstractSecondaryEmailVerificationTokenFormHandler
     */
    public function setSecondaryEmailOperationNotFoundResponseRequiredAction($secondaryEmailOperationNotFoundResponseRequiredAction)
    {
        $this->secondaryEmailOperationNotFoundResponseRequiredAction = $secondaryEmailOperationNotFoundResponseRequiredAction;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryEmailOperationExpiredResponseErrorMessage()
    {
        return $this->secondaryEmailOperationExpiredResponseErrorMessage;
    }

    /**
     * @param string $secondaryEmailOperationExpiredResponseErrorMessage
     * @return AbstractSecondaryEmailVerificationTokenFormHandler
     */
    public function setSecondaryEmailOperationExpiredResponseErrorMessage($secondaryEmailOperationExpiredResponseErrorMessage)
    {
        $this->secondaryEmailOperationExpiredResponseErrorMessage = $secondaryEmailOperationExpiredResponseErrorMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryEmailOperationExpiredResponseRequiredAction()
    {
        return $this->secondaryEmailOperationExpiredResponseRequiredAction;
    }

    /**
     * @param string $secondaryEmailOperationExpiredResponseRequiredAction
     * @return AbstractSecondaryEmailVerificationTokenFormHandler
     */
    public function setSecondaryEmailOperationExpiredResponseRequiredAction($secondaryEmailOperationExpiredResponseRequiredAction)
    {
        $this->secondaryEmailOperationExpiredResponseRequiredAction = $secondaryEmailOperationExpiredResponseRequiredAction;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryEmailOperationSuccessfulResponseMessage()
    {
        return $this->secondaryEmailOperationSuccessfulResponseMessage;
    }

    /**
     * @param string $secondaryEmailOperationSuccessfulResponseMessage
     * @return AbstractSecondaryEmailVerificationTokenFormHandler
     */
    public function setSecondaryEmailOperationSuccessfulResponseMessage($secondaryEmailOperationSuccessfulResponseMessage)
    {
        $this->secondaryEmailOperationSuccessfulResponseMessage = $secondaryEmailOperationSuccessfulResponseMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryEmailOperationSuccessfulResponseRequiredAction()
    {
        return $this->secondaryEmailOperationSuccessfulResponseRequiredAction;
    }

    /**
     * @param string $secondaryEmailOperationSuccessfulResponseRequiredAction
     * @return AbstractSecondaryEmailVerificationTokenFormHandler
     */
    public function setSecondaryEmailOperationSuccessfulResponseRequiredAction($secondaryEmailOperationSuccessfulResponseRequiredAction)
    {
        $this->secondaryEmailOperationSuccessfulResponseRequiredAction = $secondaryEmailOperationSuccessfulResponseRequiredAction;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryEmailOperationFailedResponseErrorMessage()
    {
        return $this->secondaryEmailOperationFailedResponseErrorMessage;
    }

    /**
     * @param string $secondaryEmailOperationFailedResponseErrorMessage
     * @return AbstractSecondaryEmailVerificationTokenFormHandler
     */
    public function setSecondaryEmailOperationFailedResponseErrorMessage($secondaryEmailOperationFailedResponseErrorMessage)
    {
        $this->secondaryEmailOperationFailedResponseErrorMessage = $secondaryEmailOperationFailedResponseErrorMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryEmailOperationFailedResponseRequiredAction()
    {
        return $this->secondaryEmailOperationFailedResponseRequiredAction;
    }

    /**
     * @param string $secondaryEmailOperationFailedResponseRequiredAction
     * @return AbstractSecondaryEmailVerificationTokenFormHandler
     */
    public function setSecondaryEmailOperationFailedResponseRequiredAction($secondaryEmailOperationFailedResponseRequiredAction)
    {
        $this->secondaryEmailOperationFailedResponseRequiredAction = $secondaryEmailOperationFailedResponseRequiredAction;
        return $this;
    }

    /**
     * @return View
     */

    protected function getSecondaryEmailVerificationNotFoundResponse()
    {
        return new View([
            'code'              => Response::HTTP_NOT_FOUND,
            'message'           => $this->secondaryEmailOperationNotFoundResponseErrorMessage,
            'required_action'   => $this->secondaryEmailOperationNotFoundResponseRequiredAction,
        ], Response::HTTP_NOT_FOUND);

    }

    /**
     * @return View
     */
    protected function getSecondaryEmailVerificationExpiredResponse()
    {
        return new View([
            'code'              => Response::HTTP_BAD_REQUEST,
            'message'           => $this->secondaryEmailOperationExpiredResponseErrorMessage,
            'required_action'   => $this->secondaryEmailOperationExpiredResponseRequiredAction,
        ], Response::HTTP_BAD_REQUEST);

    }

    /**
     * @return View
     */
    protected function getSecondaryEmailVerificationOperationSuccessfulResponse()
    {
        return new View([
            'code'              => Response::HTTP_OK,
            'message'           => $this->secondaryEmailOperationSuccessfulResponseMessage,
            'required_action'   => $this->secondaryEmailOperationSuccessfulResponseRequiredAction,
        ], Response::HTTP_OK);

    }

    /**
     * @return View
     */
    protected function getSecondaryEmailVerificationFailedResponse()
    {
        return new View([
            'code'              => Response::HTTP_BAD_REQUEST,
            'message'           => $this->secondaryEmailOperationFailedResponseErrorMessage,
            'required_action'   => $this->secondaryEmailOperationFailedResponseRequiredAction,
        ], Response::HTTP_BAD_REQUEST);

    }

}