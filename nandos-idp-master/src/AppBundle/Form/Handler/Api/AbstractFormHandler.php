<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Log\Traits\LoggerTrait;
use AppBundle\Util\ApiTest\ApiTestRequestValidator;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Serializer\ExceptionWrapperNormalizer;
use FOS\RestBundle\View\ExceptionWrapperHandler;
use Symfony\Component\Form\FormFactory;

/**
 * Class BaseFormHandler
 *
 * This provides a means of building forms, and customising form error output
 *
 */

abstract class AbstractFormHandler
{
    use LoggerTrait;

    /**
     * @var ExceptionWrapperHandler
     */

    protected $exceptionWrapperHandler;

    /**
     * @var ExceptionWrapperNormalizer
     */

    protected $exceptionWrapperNormalizer;

    /**
     * @var FormFactory
     */

    protected $formFactory;

    /**
     * @var ApiTestRequestValidator
     */

    protected $apiTestRequestValidator;

    /**
     * @param ExceptionWrapperHandler $exceptionWrapperHandler
     */
    public function setExceptionWrapperHandler($exceptionWrapperHandler)
    {
        $this->exceptionWrapperHandler = $exceptionWrapperHandler;
    }

    /**
     * @param ExceptionWrapperNormalizer $exceptionWrapperNormalizer
     */
    public function setExceptionWrapperNormalizer($exceptionWrapperNormalizer)
    {
        $this->exceptionWrapperNormalizer = $exceptionWrapperNormalizer;
    }

    /**
     * @param FormFactory $formFactory
     */
    public function setFormFactory($formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param ApiTestRequestValidator $apiTestRequestValidator
     */
    public function setApiTestRequestValidator($apiTestRequestValidator)
    {
        $this->apiTestRequestValidator = $apiTestRequestValidator;
    }

    /**
     * Version 1
     * manually create the exception and normalize it (converts the form errors to an array, and then adjust output)
     * We can in the future make our own ExceptionWrapperHandler than makes a new ExceptionWrapper that DOES NOT
     * extend ExceptionWrapper, and add our own tagged service "serializer.normalizer" that will do the same as
     * fos_rest.serializer.exception_wrapper_normalizer but will take an optional callable in the array of options,
     * that will be triggered in the normalizer and perform extra steps. (most likely a callable to the form handler
     * triggering the form error)
     *
     * @param $form
     * @param string $message
     * @param int $statusCode
     * @return array
     */

    public function getFormErrorsArray($form, $message = 'Validation Failed', $statusCode = Response::HTTP_BAD_REQUEST)
    {

        $formException = $this->exceptionWrapperHandler->wrap([
            'status_code' => $statusCode,
            'message'     => $message,
            'errors'      => $form,
        ]);

        // This normalizer converts form errors to a nested array structure
        $formErrorsArray = $this->exceptionWrapperNormalizer->normalize($formException);

        $formErrorsArray['required_action'] = null;

        return $formErrorsArray;

    }

    /**
     * To be used when no manipulation needs to be done to the form errors array structure
     *
     * @param Form $form
     * @param string $message
     * @param int $statusCode
     * @return View
     */
    public function getBasicFormErrorsResponse(Form $form, $message = 'Validation Failed', $statusCode = Response::HTTP_BAD_REQUEST)
    {
        $formErrors = $this->getFormErrorsArray($form, $message, $statusCode);
        return new View($formErrors, Response::HTTP_BAD_REQUEST);
    }

    /**
     * For testing, we are allowed to return extra information e.g. the confirmation code for account activation.
     * For security purposes, this is done through a secret token. This functionality will never be enabled on the production
     * servers, as it is controlled by parameters.yml
     *
     * @return bool
     */

    public function canAddExtendedResponseInformation()
    {
        return $this->apiTestRequestValidator->isValidatedApiTestRequest();
    }

}