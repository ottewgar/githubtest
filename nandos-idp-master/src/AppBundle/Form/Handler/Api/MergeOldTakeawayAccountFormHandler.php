<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\SecondaryEmailOperation;

class MergeOldTakeawayAccountFormHandler extends AbstractSecondaryEmailVerificationTokenFormHandler
{

    /**
     * @var UserManager
     */
    private $userManager;

    protected $requestedOperation = SecondaryEmailOperation::OPERATION_MERGE_OLD_TAKEAWAY_ACCOUNT;

    /**
     * Create a new UserProfileFormHandler
     *
     * @param UserManagerInterface $userManager
     */

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * Can set failure messages using $this->setSecondaryEmailOperationFailedResponseErrorMessage('reason');
     * @param SecondaryEmailOperation $secondaryEmailOperation
     * @return bool
     */
    protected function performSecondaryEmailVerificationOperation(SecondaryEmailOperation $secondaryEmailOperation)
    {

        if ($takeawayCustomerId = $secondaryEmailOperation->getSecondaryUser()->getTakeawayCustomerId()) {

            $primaryUser = $secondaryEmailOperation->getPrimaryUser();

            $primaryUser->setTakeawayCustomerId($takeawayCustomerId);

            // TBC - null value from secondary user? mark account as locked?

            $this->userManager->updateUser($primaryUser);

        }

        return true;

    }
}