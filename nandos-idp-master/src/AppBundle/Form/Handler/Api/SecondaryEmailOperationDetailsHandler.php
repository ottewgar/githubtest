<?php
/**
 * Created by PhpStorm.
 * User: alistairburns
 * Date: 06/04/2016
 * Time: 13:16
 */

namespace AppBundle\Form\Handler\Api;


use AppBundle\Entity\Repository\SecondaryEmailOperationRepository;
use AppBundle\Entity\User;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

class SecondaryEmailOperationDetailsHandler
{

    /**
     * @var SecondaryEmailOperationRepository
     */

    private $secondaryEmailOperationRepository;

    /**
     * SecondaryEmailOperationDetailsHandler constructor.
     * @param SecondaryEmailOperationRepository $secondaryEmailOperationRepository
     */
    public function __construct(SecondaryEmailOperationRepository $secondaryEmailOperationRepository)
    {
        $this->secondaryEmailOperationRepository = $secondaryEmailOperationRepository;
    }

    public function getSecondaryEmailOperationDetailsResponse(User $primaryUser, $verificationCode)
    {

        $secondaryEmailOperation = $this->secondaryEmailOperationRepository->getOneByPrimaryUserAndVerificationToken($primaryUser, $verificationCode);

        if ($secondaryEmailOperation) {
            return $secondaryEmailOperation;

        } else {
            return new View([
                'code'              => Response::HTTP_NOT_FOUND,
                'message'           => 'record_not_found',
                'required_action'   => 'check_logged_in_as_correct_user_or_request_again',
            ], Response::HTTP_NOT_FOUND);

        }

    }

}