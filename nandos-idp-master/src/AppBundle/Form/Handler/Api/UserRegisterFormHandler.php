<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Email\NandosMailer;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Entity\UserMetadata;
use AppBundle\Form\Type\Api\UserRegisterFormType;
use AppBundle\Util\NandosCardConfig;
use AppBundle\Util\Sanitizer;
use FOS\RestBundle\View\View;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserRegisterFormHandler extends AbstractFormHandler
{

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var NandosMailer
     */
    private $mailer;

    private $cardConfig;

    /**
     * Create a new UserRegisterFormHandler
     *
     * @param UserManagerInterface $userManager
     * @param NandosMailer $mailer
     */

    function __construct(UserManagerInterface $userManager, NandosMailer $mailer, NandosCardConfig $cardConfig)
    {
        $this->userManager = $userManager;
        $this->mailer      = $mailer;
        $this->cardConfig = $cardConfig;
    }

    /**
     * @param Request $request
     * @return View
     */

    public function handle(Request $request)
    {
        $form = $this->getForm();

        $form->handleRequest($request);

        // an empty user{...} POST is treated as not submitted, so set an empty value that will force full validation
        if (!$form->isSubmitted()) {
            $form->submit(array('firstname' => ''));
        }

        return $this->processForm($form);

    }

    /**
     * The form should already be bound with values using handleRequest
     *
     * @param Form $form
     * @return View
     */

    private function processForm(FormInterface $form)
    {
        // if user exists, need to send back the confirmation token if not active

        /** @var User $user */
        $user = $form->getData();

        if ($form->isValid()) {

            $this->initializeNewUser($user, $form);

            $sanitisedEmail = Sanitizer::sanitiseEmail($user->getUsername());

            $this->info("Create new user with email {$sanitisedEmail}", ['form_handler' => 'userregister']);

            // save non activated user to database, with a confirmation code
            // they have no account in NCR/Paytronix
            $this->userManager->updateUser($user);

            $emailTemplate = $form->get('email_template')->getData();

            // send them a confirmation email
            $this->mailer->sendUserRegisterEmail($user, $emailTemplate);

            $responseVars = [
                'code'              => Response::HTTP_CREATED,
                'message'           => 'Registration successful',
                'required_action'   => 'check_confirmation_email'
            ];

            if ($this->canAddExtendedResponseInformation()) {
                $responseVars['confirmation_code'] = $user->getConfirmationToken();
            }

            return new View($responseVars, Response::HTTP_CREATED);

        } else {

            $this->info("Register usr form not valid", ['form_handler' => 'userregister']);

            $formErrorsArray = $this->getFormErrorsArray($form, 'Registration failed');

            if (isset($formErrorsArray['errors']['children']['email']['errors'])) {

                $emailErrors = $formErrorsArray['errors']['children']['email']['errors'];

                // if the user exists in the IDP, we need to take specific actions depending on the status of the user
                if (in_array(User::ERROR_EMAIL_EXISTS_IN_IDP, $emailErrors)) {

                    // remove error code, add human readable message, and update required_action
                    //$emailErrors = array_merge(array_diff($emailErrors, array(User::ERROR_EMAIL_EXISTS_IN_IDP)));
                    //$emailErrors[] = 'User already exists';

                    // required action depends on status of user record
                    $idpUser = $this->userManager->findUserByEmail($user->getEmail());

                    if (!$idpUser) {
                        throw new \Exception(sprintf("We identified %s as being in the IDP but were unable to retrieve their user details", $user->getEmail()));
                    }

                    // user needs to confirm by email? // user needs to reset their password?

                    $formErrorsArray['required_action'] = 'login_or_password_reset';

                } elseif (in_array(User::ERROR_EMAIL_EXISTS_IN_NCR, $emailErrors) || in_array(User::ERROR_EMAIL_EXISTS_IN_PAYTRONIX, $emailErrors)) {

                    // exist in ncr, they have account, tell them to login or password reset
                    $formErrorsArray['required_action'] = 'login_or_password_reset';

                }

                // add back the updated email errors
                //$formExceptionArray['errors']['children']['email']['errors'] = $emailErrors;

            }

            return new View($formErrorsArray, Response::HTTP_BAD_REQUEST);

        }

    }

    private function initializeNewUser(User $user, $form)
    {
        $user->setEnabled(false);

        // they set password after confirming, so set them an expired, totally random password
        $this->userManager->initialiseExpiredRandomPassword($user);

        // and ensure they have a confirmation token to set their password
        $this->userManager->initialiseUserConfirmationToken($user);

        // store any user metadata
        $metadataFields = UserRegisterFormType::getMetadataFields();

        foreach ($metadataFields as $fieldName => $fieldOptions)
        {

            $fieldValue = trim($form->get($fieldName)->getData());

            if (strlen($fieldValue)) {

                $key         = isset($fieldOptions['key'])       ? $fieldOptions['key']       : $fieldName;
                $format      = isset($fieldOptions['format'])    ? $fieldOptions['format']    : null;
                $isEncrypted = isset($fieldOptions['encrypted']) ? $fieldOptions['encrypted'] : false;

                $metadata = UserMetadata::create($key, $fieldValue, $isEncrypted, $format);
                $user->addMetadata($metadata);

            }
        }

    }

    /**
     * @return Form
     */
    private function getForm()
    {
        return $this->formFactory->create(new UserRegisterFormType(), new User());
    }

}