<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Email\NandosMailer;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Form\Model\ChangePassword;
use AppBundle\Form\Type\Api\ChangePasswordFormType;
use AppBundle\Form\Type\Api\UserProfileFormType;
use FOS\RestBundle\View\View;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserProfileFormHandler extends AbstractFormHandler
{

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var NandosMailer
     */
    private $mailer;

    /**
     * Create a new UserProfileFormHandler
     *
     * @param UserManagerInterface $userManager
     */

    public function __construct(UserManagerInterface $userManager, NandosMailer $mailer)
    {
        $this->userManager = $userManager;
        $this->mailer      = $mailer;
    }

    /**
     *
     * @param Form $form
     * @return View
     */

    public function handle(User $user, Request $request)
    {

        $currentEmailAddress = strtolower(trim($user->getEmail()));

        $form = $this->getForm($user);

        // remove format from the request or it will be treated as an extra form field
        $request->request->remove('_format');

        $form->handleRequest($request);

        // an empty POST is treated as not submitted, so set an empty value that will force full validation
        if (!$form->isSubmitted()) {
            $form->submit(array('firstname' => ''));
        }

        if (!$form->isValid()) {

            $formErrors = $this->getFormErrorsArray($form, 'Changes not saved');
            return new View($formErrors, Response::HTTP_BAD_REQUEST);

        } else {

            $requiredAction = null;

            $newEmailAddress = strtolower(trim($user->getEmail()));

            // check to see if email address changed
            if ($newEmailAddress != $currentEmailAddress) {

                // Important, don't update the user record yet, send an email asking them to confirm the change
                $user->setEmail($currentEmailAddress);

                // change to a non mapped field? e.g. new Email - need to define the same validation constraints

//                $this->userManager->initialiseUserConfirmationToken($user);
//                $user->setEnabled(false);
//
//                $this->mailer->sendEmailAddressChangedEmail($user);
                $requiredAction = 'check_confirmation_email';
            }

            $this->userManager->updateUser($user);

            return new View([
                'code'              => Response::HTTP_OK,
                'message'           => 'Details updated',
                'required_action'   => $requiredAction
            ], Response::HTTP_OK);

        }

    }

    /**
     * @param User $user
     * @return Form
     */

    private function getForm($user)
    {
        return $this->formFactory->create(new UserProfileFormType(), $user);
    }
}