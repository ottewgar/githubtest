<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Email\NandosMailer;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\Repository\SecondaryEmailOperationRepository;
use AppBundle\Entity\SecondaryEmailOperation;
use AppBundle\Entity\User;

use AppBundle\Form\Model\Email;
use AppBundle\Form\Type\Api\EmailFormType;
use FOS\RestBundle\View\View;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RequestMergeOldTakeawayAccountFormHandler extends AbstractFormHandler
{

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var NandosMailer
     */
    private $mailer;

    /**
     * @var SecondaryEmailOperationRepository
     */

    private $secondaryEmailOperationRepository;

    /**
     * @var TokenGeneratorInterface
     */

    private $secondaryEmailOperationTokenGenerator;

    /**
     * Create a new ResendConfirmationEmailFormHandler
     *
     * @param UserManagerInterface $userManager
     */
    function __construct(
        UserManagerInterface $userManager,
        NandosMailer $mailer,
        SecondaryEmailOperationRepository $secondaryEmailOperationRepository,
        TokenGeneratorInterface $secondaryEmailOperationTokenGenerator)
    {
        $this->userManager                           = $userManager;
        $this->mailer                                = $mailer;
        $this->secondaryEmailOperationRepository     = $secondaryEmailOperationRepository;
        $this->secondaryEmailOperationTokenGenerator = $secondaryEmailOperationTokenGenerator;

    }

    /**
     * @param Request $request
     * @return View
     */

    public function handle(User $primaryUser, Request $request)
    {

        $form = $this->getForm();

        // remove format from the request or it will be treated as an extra form field
        $request->request->remove('_format');

        $form->handleRequest($request);

        // an empty POST is treated as not submitted, so set an empty value that will force full validation
        if (!$form->isSubmitted()) {
            $form->submit(array('email' => ''));
        }

        // Most likely an invalid email address format
        if (!$form->isValid()) {
            return $this->getBasicFormErrorsResponse($form);
        }

        $secondaryEmailAddress = $form->getData()->getEmail();
        /**
         * @var User $secondaryUser
         */
        $secondaryUser = $this->userManager->findUserByEmail($secondaryEmailAddress);

        if (!$secondaryUser) {
            return $this->getUserNotFoundResponse();
        }

        // does the user have a takeaway customer id?
        if (!$secondaryUser->getTakeawayCustomerId()) {
            return $this->getInvalidLagacyTakeawayAccountResponse();
        }

        // create or update a SecondaryEmailOperation record (refresh token and expiry)

        // get pending record for user
        $secondaryEmailOperation = $this->secondaryEmailOperationRepository->getPendingOperationForPrimaryUserAndSecondaryUser(
            SecondaryEmailOperation::OPERATION_MERGE_OLD_TAKEAWAY_ACCOUNT,
            $primaryUser,
            $secondaryUser
        );

        if (!$secondaryEmailOperation) {
            $secondaryEmailOperation = $this->secondaryEmailOperationRepository->createPendingOperationForPrimaryUserAndSecondaryUser(
                SecondaryEmailOperation::OPERATION_MERGE_OLD_TAKEAWAY_ACCOUNT,
                $primaryUser,
                $secondaryUser
            );
        }

        $secondaryEmailOperation->refreshExpiresAt();
        $secondaryEmailOperation->setVerificationToken($this->secondaryEmailOperationTokenGenerator->generateToken());

        $this->secondaryEmailOperationRepository->save($secondaryEmailOperation);

        $this->mailer->sendConfirmMergeOldTakeawayAccountEmail($secondaryEmailOperation);

        return new View([
            'code'              => Response::HTTP_OK,
            'message'           => 'Confirm old Takeaway email sent',
            'required_action'   => 'check_confirmation_email',
        ], Response::HTTP_OK);

    }

    /**
     * @return Form
     */
    private function getForm()
    {
        return $this->formFactory->createNamed('', new EmailFormType(), new Email());
    }

    /**
     * @return View
     */

    private function getUserNotFoundResponse()
    {
        return new View([
            'code'            => Response::HTTP_NOT_FOUND,
            'message'         => 'account_not_found',
            'required_action' => 'check_email_address',
        ], Response::HTTP_NOT_FOUND);
    }

    private function getInvalidLagacyTakeawayAccountResponse()
    {
        return new View([
            'code'            => Response::HTTP_BAD_REQUEST,
            'message'         => 'not_old_takeaway_account',
            'required_action' => null,
        ], Response::HTTP_BAD_REQUEST);
    }

}