<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Form\Type\Api\ResetPasswordFormType;
use AppBundle\Util\Sanitizer;
use FOS\RestBundle\View\View;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ResetPasswordByTokenFormHandler extends AbstractFormHandler
{

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var CardUserManagerContainer
     */
    private $cardUserManagerContainer;

    /**
     * Create a new ResetPasswordByTokenFormHandler
     *
     * @param UserManagerInterface $userManager
     * @param CardUserManagerContainer $cardUserManagerContainer
     */
    function __construct(UserManagerInterface $userManager, CardUserManagerContainer $cardUserManagerContainer)
    {
        $this->userManager = $userManager;
        $this->cardUserManagerContainer = $cardUserManagerContainer;
    }

    /**
     *
     * @param Form $form
     * @return View
     */
    public function handle($confirmationToken, Request $request)
    {

        $user = $this->userManager->findUserByConfirmationToken($confirmationToken);

        if (!$user) {
            $this->notice("User not found with token", ['form_handler' => 'resetpasswordbytoken']);
            return $this->getUserNotFoundResponse();
        }

        $sanitisedEmail = Sanitizer::sanitiseEmail($user->getUsername());

        if (!$user->checkConfirmationCodeInDate()) {

            $this->notice("Password reset confirmation code expired for user {$sanitisedEmail}", ['form_handler' => 'resetpasswordbytoken']);

            return new View([
                'code'               => Response::HTTP_OK,
                'message'            => 'Confirmation Code Expired',
                'required_action'    => 'instruct_user_to_request_new_confirmation_code',
            ], Response::HTTP_OK);
        }

        $form = $this->getForm($user);

        // remove format from the request or it will be treated as an extra form field
        $request->request->remove('_format');

        $form->handleRequest($request);

        // an empty POST is treated as not submitted, so set an empty value that will force full validation
        if (!$form->isSubmitted()) {
            $form->submit(array('password' => ''));
        }

        if (!$form->isValid()) {

            $this->info("Password reset for not valid for user {$sanitisedEmail}", ['form_handler' => 'resetpasswordbytoken']);

            $formErrors = $this->getFormErrorsArray($form, 'Password not changed');

            // set a 'required_action' ?

            return new View($formErrors, Response::HTTP_BAD_REQUEST);

        } else {

            // change password/active/reset password expired etc. (do we care about the state the user was in?)

            $this->info("Update password for user {$sanitisedEmail}", ['form_handler' => 'resetpasswordbytoken']);

            $user->setConfirmationToken(null); // clear the token
            $user->setEnabled(true); // enable the account, as they can only get the token in an email, so we know the email is theirs
            $user->setCredentialsExpired(false);
            $user->setCredentialsExpireAt(null);


            $this->userManager->updateUser($user);

            $this->cardUserManagerContainer->updateProfilePassword($user->getUsername(), $user->getPassword(), true);

            return new View([
                'code'              => Response::HTTP_OK,
                'message'           => 'Password updated',
                'required_action'   => 'instruct_user_to_login',
            ], Response::HTTP_OK);

        }

    }

    /**
     * @param User $user
     * @return Form
     */
    private function getForm($user)
    {
        return $this->formFactory->createNamed('', new ResetPasswordFormType(), $user);
    }

    /**
     * @return View
     */
    private function getUserNotFoundResponse()
    {
        return new View([
            'code'            => Response::HTTP_NOT_FOUND,
            'message'         => 'user_not_found',
            'required_action' => 'request_new_password_reset_or_attempt_login',
        ], Response::HTTP_NOT_FOUND);
    }

}