<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Entity\Manager\UserManager;
use AppBundle\Log\Traits\LoggerTrait;
use AppBundle\Util\Sanitizer;
use FOS\RestBundle\View\View;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class ConfirmUserAccountByTokenHandler
{
    use LoggerTrait;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * Create a new ConfirmUserAccountByTokenHandler
     *
     * @param UserManagerInterface $userManager
     */

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param $confirmationToken
     * @return View
     */

    public function handle($confirmationToken)
    {
        return $this->confirmUserByToken($confirmationToken);
    }

    /**
     * @param $confirmationToken
     * @return View
     */

    private function confirmUserByToken($confirmationToken)
    {

        $user = $this->userManager->findUserByConfirmationToken($confirmationToken);

        if (!$user) {

            $this->notice("No user found when trying to confirm account", ['form_handler' => 'confirmuseraccountbytoken']);

            return new View([
                'code'              => Response::HTTP_NOT_FOUND,
                'message'           => 'user_not_found',
                'required_action'   => 'request_new_confirmation_token_or_attempt_login',
            ], Response::HTTP_NOT_FOUND);
        }

        $sanitisedEmail = Sanitizer::sanitiseEmail($user->getUsername());

        if (!$user->checkConfirmationCodeInDate()) {

            $this->notice("Confirmation token expired for account confirmation", ['form_handler' => 'confirmuseraccountbytoken']);

            return new View([
                'code'               => Response::HTTP_OK,
                'message'            => 'Confirmation Code Expired',
                'required_action'    => 'instruct_user_to_request_new_confirmation_code',
            ], Response::HTTP_OK);
        }

        $user->setEnabled(true);

        $requiredAction = 'instruct_user_to_login';

        if ($user->isCredentialsNonExpired()) {
            // clear token, as they don't need it to set password, they are ready to log in
            $user->setConfirmationToken(null);
        } else {
            $requiredAction = 'instruct_user_to_set_password';
        }

        $this->info("Confirm user account {$sanitisedEmail}", ['form_handler' => 'confirmuseraccountbytoken']);

        $this->userManager->updateUser($user);

        return new View([
            'code'               => Response::HTTP_OK,
            'message'            => 'Confirmation successful',
            'required_action'    => $requiredAction,
            'confirmation_token' => $confirmationToken
        ], Response::HTTP_OK);


    }

}