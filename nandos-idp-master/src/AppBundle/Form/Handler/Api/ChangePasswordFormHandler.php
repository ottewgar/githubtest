<?php

namespace AppBundle\Form\Handler\Api;

use AppBundle\Api\Manager\CardUserManagerContainer;
use AppBundle\Entity\Manager\UserManager;
use AppBundle\Entity\User;
use AppBundle\Form\Model\ChangePassword;
use AppBundle\Form\Type\Api\ChangePasswordFormType;
use AppBundle\Util\Sanitizer;
use FOS\RestBundle\View\View;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChangePasswordFormHandler extends AbstractFormHandler
{

    /**
     * @var UserManager
     */
    private $userManager;

    private $cardUserManagerContainer;

    /**
     * Create a new ChangePasswordFormHandler
     *
     * @param UserManagerInterface $userManager
     */

    public function __construct(UserManagerInterface $userManager, CardUserManagerContainer $cardUserManagerContainer)
    {
        $this->userManager = $userManager;
        $this->cardUserManagerContainer = $cardUserManagerContainer;
    }

    /**
     *
     * @param Form $form
     * @return View
     */

    public function handle(User $user, Request $request)
    {

        $form = $this->getForm();

        // remove format from the request or it will be treated as an extra form field
        $request->request->remove('_format');

        $form->handleRequest($request);

        // an empty POST is treated as not submitted, so set an empty value that will force full validation
        if (!$form->isSubmitted()) {
            $form->submit(array('current_password' => ''));
        }

        if (!$form->isValid()) {

            $this->info("Change password handler form error", ['form_handler' => 'changepassword']);

            $formErrors = $this->getFormErrorsArray($form, 'Password not changed');
            return new View($formErrors, Response::HTTP_BAD_REQUEST);

        } else {

            $newPassword = $form->get('new_password')->getData();

            $sanitisedEmail = Sanitizer::sanitiseEmail($user->getUsername());
            $this->info("Update password for user {$sanitisedEmail} using change password", ['form_handler' => 'changepassword']);

            $user->setPlainPassword($newPassword);

            $this->userManager->updateUser($user);

            $this->cardUserManagerContainer->updateProfilePassword($user->getUsername(), $user->getPassword(), true);

            return new View([
                'code'              => Response::HTTP_OK,
                'message'           => 'Password updated',
            ], Response::HTTP_OK);

        }

    }

    /**
     * @param User $user
     * @return Form
     */

    private function getForm()
    {
        return $this->formFactory->createNamed('', new ChangePasswordFormType(), new ChangePassword());
    }
}