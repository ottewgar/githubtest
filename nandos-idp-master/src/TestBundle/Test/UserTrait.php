<?php

namespace TestBundle\Test;

use AppBundle\Entity\User;

trait UserTrait
{
    protected $userManager;

    public function setUserManager($userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param $username
     * @param $password
     */
    public function getUser($username, $password)
    {
        // change the IDP password
        $user = $this->userManager->findUserByUsername($username);

        if ($user instanceof User) {
            $user->setPlainPassword($password);
            $user->setEnabled(1);
        } else {
            $user = new User();
            $user->setPlainPassword($password);
            $user->setUsername($username);
            $user->setFirstname('Test');
            $user->setLastname('Test');
            $user->setEnabled(1);
        }

        $this->userManager->updateUser($user);

        return $user;
    }

    /**
     * @param $username
     */
    public function deleteUser($username)
    {
        $user = $this->userManager->findUserByUsername($username);

        if ($user instanceof User) {
            $this->userManager->deleteUser($user);
        }

    }

}