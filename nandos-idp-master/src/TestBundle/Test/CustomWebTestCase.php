<?php

namespace TestBundle\Test;

use AppBundle\Api\NcrRadiant\NcrRadiantApi;
use AppBundle\Api\Paytronix\PaytronixApi;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * WebTestCase is the base class for functional tests.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
abstract class CustomWebTestCase extends KernelTestCase
{
    use UserTrait;

    /**
     * @var null
     */
    private $kernelModifier = null;

    /**
     * @var
     */
    protected $paytronixApi;

    /**
     * @var
     */
    protected $ncrApi;

    /**
     *
     */
    protected function init()
    {
        $this->paytronixApi = $this->getMockBuilder(PaytronixApi::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->ncrApi = $this->getMockBuilder(NcrRadiantApi::class)
            ->disableOriginalConstructor()
            ->getMock();

        $paytronixApi = $this->paytronixApi;
        $ncrApi = $this->ncrApi;

        $this->setKernelModifier(function($kernel) use ($paytronixApi, $ncrApi) {
            $kernel->getContainer()->set('nandos.paytronix.api', $paytronixApi);
            $kernel->getContainer()->set('nandos.ncr_radiant.api', $ncrApi);
        });

    }

    public function afterInit()
    {
        $settings = static::$kernel->getContainer()->get('nandos.settings');

        $settings->setSetting('ncr_radiant_enabled', 1, 'boolean');
        $settings->setSetting('paytronix_enabled', 1, 'boolean');

        $userService = static::$kernel->getContainer()->get('user_service');
        $userManager = $userService->getUserManager();

        $this->setUserManager($userManager);

    }

    /**
     * @return array
     */
    public function getClientParams()
    {
        $params = [
            'client_id' => "1_ursij638cq8s88skscsk044kg4ggoo04skggskcc84g084400",
            'client_secret' => "2a5c9iium534cwsk8o8ogksssgkk84gg8ccoco80s0k4kgckg0",
        ];

        return $params;
    }

    /**
     * @param $password
     * @return bool|string
     */
    public function encrypt($password)
    {
        $options = ['cost' => 12];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    /**
     * Creates a Client.
     *
     * @param array $options An array of options to pass to the createKernel class
     * @param array $server  An array of server parameters
     *
     * @return Client A Client instance
     */
    protected function createClient(array $options = array(), array $server = array())
    {
        static::bootKernel($options);
        if ($kernelModifier = $this->kernelModifier) {
            $kernelModifier(static::$kernel);
            $this->kernelModifier = null;
        };

        $client = static::$kernel->getContainer()->get('test.client');
        $client->setServerParameters($server);

        return $client;
    }

    /**
     * @param \Closure $kernelModifier
     */
    public function setKernelModifier(\Closure $kernelModifier)
    {
        $this->kernelModifier = $kernelModifier;

        // We force the kernel to shutdown to be sure the next request will boot it
//        static::$kernel->shutdown();
    }
}
