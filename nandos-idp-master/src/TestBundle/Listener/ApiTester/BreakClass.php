<?php

namespace TestBundle\Listener\ApiTester;

class BreakClass
{
    /**
     * BreakClass constructor.
     *
     * causes 500 error for api test
     */
    public function __construct()
    {
        throw new \Exception('Simulated 500 error');
    }
}