<?php

namespace TestBundle\Listener;

use AppBundle\Api\ApiService;
use AppBundle\Listener\ApiTester\BreakClass;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ApiTesterListener
{
    /**
     * @var ApiService
     */
    private $apiService;

    /**
     * CheckRouteAllowedListener constructor.
     * @param ApiService $apiService
     */
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param FilterControllerEvent $event
     * @return bool
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        if (!$this->apiService->getApiTestMode()) {
            return false;
        }

        $controllerRoute = $event->getRequest()->get('_route');

        $token = $this->apiService->getToken();

        if (!$token) {
            return false;
        }

        $user = $token->getUser();

        switch ($user->getUsername()) {
            case "token@blonde.net":
                // change token expiry to 10 seconds time
                $accessToken = $this->apiService->getAccessToken();
                $accessToken->setExpiresAt(time() + (10));
                $this->apiService->updateToken($accessToken);
                break;
            case "ratelimit@blonde.net":
                // simulate rate limit being hit
                // this is not used as RateLimitBreakListener was used instead
                break;
            case "break@blonde.net":
                $break = new BreakClass();
                break;
            default:

        }

    }

}
