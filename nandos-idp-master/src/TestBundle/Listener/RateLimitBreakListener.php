<?php

namespace TestBundle\Listener;

use Noxlogic\RateLimitBundle\Annotation\RateLimit;
use Noxlogic\RateLimitBundle\EventListener\BaseListener;
use Noxlogic\RateLimitBundle\EventListener\RateLimitAnnotationListener;
use Noxlogic\RateLimitBundle\Events\GenerateKeyEvent;
use Noxlogic\RateLimitBundle\Events\RateLimitEvents;
use Noxlogic\RateLimitBundle\Service\RateLimitService;
use Noxlogic\RateLimitBundle\Util\PathLimitProcessor;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class RateLimitBreakListener extends RateLimitAnnotationListener
{
    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $rateUsername = "ratelimit@blonde.net";

        $getUsername = $event->getRequest()->get('username');
        $postUsername = $event->getRequest()->request->get('username');

        // if the username is the ratelimit user name then...
        if ($getUsername == $rateUsername || $postUsername == $rateUsername) {

            // Skip if we aren't the main request
            if ($event->getRequestType() != HttpKernelInterface::MASTER_REQUEST) {
                return;
            }

            // Skip if we are a closure
            if (! is_array($controller = $event->getController())) {
                return;
            }

            // Find the best match
            $annotations = $event->getRequest()->attributes->get('_x-rate-limit', array());
            $rateLimit = $this->findBestMethodMatch($event->getRequest(), $annotations);

            // No matching annotation found
            if (! $rateLimit) {
                return;
            };

            // This will loop through and add 20 calls to the rate limit key
            for ($i  = 0; $i < 20; $i++) {
                $key = $this->getKey($event, $rateLimit, $annotations);
                $rateLimitInfo = $this->rateLimitService->limitRate($key);
            }

            $request = $event->getRequest();
            $request->attributes->set('rate_limit_info', $rateLimitInfo);

        }
    }

    /**
     * This had to be copied over from the parent because it is private
     *
     * @param FilterControllerEvent $event
     * @param RateLimit $rateLimit
     * @param array $annotations
     * @return string
     */
    private function getKey(FilterControllerEvent $event, RateLimit $rateLimit, array $annotations)
    {
        $request = $event->getRequest();
        $controller = $event->getController();

        $rateLimitMethods = join("", $rateLimit->getMethods());
        $rateLimitAlias = count($annotations) === 0
            ? $this->pathLimitProcessor->getMatchedPath($request)
            : get_class($controller[0]) . ':' . $controller[1];

        // Create an initial key by joining the methods and the alias
        $key = $rateLimitMethods . ':' . $rateLimitAlias ;

        // Let listeners manipulate the key
        $keyEvent = new GenerateKeyEvent($event->getRequest(), $key);
        $this->eventDispatcher->dispatch(RateLimitEvents::GENERATE_KEY, $keyEvent);

        return $keyEvent->getKey();
    }
}
