<?php

namespace TestBundle\Controller;


use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/testfrontend")
 */
class TestFrontendController extends Controller
{
    static $options = [
        'csrf_protection' => false,
        'attr' => ['class' => 'ajax_form']
    ];

    /**
     * @Route("/resetpassword", name="testfrontend_resetpassword")
     */
    public function indexAction(Request $request)
    {
        $data = array();
        $emailForm = $this->getPasswordResetEmailForm($data);

        return $this->render(
            'TestBundle:TestFrontend:form.html.twig',
            [
                'form' => $emailForm->createView(),
                'title' => "Reset Password Email"
            ]);
    }

    /**
     * @Route("/resetpassword-submit/{token}", name="testfrontend_resetpassword-submit")
     */
    public function resetpasswordsubmitAction(Request $request, $token)
    {
        $data = array();
        $form = $this->getPasswordResetForm($data, $token);

        return $this->render(
            'TestBundle:TestFrontend:form.html.twig',
            [
                'form' => $form->createView(),
                'title' => "Reset Password"
            ]);
    }

    /**
     * @Route("/register", name="testfrontend_register")
     */
    public function registerAction(Request $request)
    {
        $data = array();
        $form = $this->getRegisterForm($data);

        return $this->render(
            'TestBundle:TestFrontend:form.html.twig',
            [
                'form' => $form->createView(),
                'title' => "Register"
            ]);
    }

    /**
     * @Route("/confirmaccount/{token}", name="testfrontend_confirmaccount")
     */
    public function confirmAccountAction(Request $request, $token)
    {
        $data = array();
        $form = $this->getConfirmAccountForm($data, $token);

        return $this->render(
            'TestBundle:TestFrontend:form.html.twig',
            [
                'form' => $form->createView(),
                'title' => "Confirm Account"
            ]);
    }

    /**
     * @Route("/login", name="testfrontend_login")
     */
    public function loginAction(Request $request)
    {
        $data = array();
        $form = $this->getLoginForm($data);

        return $this->render(
            'TestBundle:TestFrontend:form.html.twig',
            [
                'form' => $form->createView(),
                'title' => "Login"
            ]);
    }

    /**
     * @Route("/userinfo", name="testfrontend_userinfo")
     */
    public function userinfoAction(Request $request)
    {
        $data = array();
        $form = $this->getUserinfoForm($data);

        return $this->render(
            'TestBundle:TestFrontend:userinfo.html.twig',
            [
                'form' => $form->createView(),
                'title' => "User Info"
            ]);
    }

    public function getPasswordResetEmailForm($data)
    {
        $form = $this->container->get('form.factory')->createNamedBuilder(null, 'form', $data, self::$options)
            ->setMethod('POST')
            ->setAction('/api/v1/send-password-reset-email')
            ->add('email', 'text', [
                'attr' => ['class' => 'password'],
            ])
            ->add('Submit', 'submit', [
                'attr' => ['class' => 'form_submit_button btn',]
            ])
            ->getForm();

        return $form;
    }

    public function getPasswordResetForm($data, $token, $label = "Password")
    {
        $form = $this->container->get('form.factory')->createNamedBuilder(null, 'form', $data, self::$options)
            ->setMethod('POST')
            ->setAction("/api/v1/token/{$token}/reset-password")
            ->add('password', 'text', [
                'attr' => ['class' => 'password'],
                'label' => $label,
            ])
            ->add('Submit', 'submit', [
                'attr' => ['class' => 'form_submit_button btn',]
            ])
            ->getForm();

        return $form;
    }

    public function getConfirmAccountForm($data, $token)
    {
        // user needs to set a password to password reset is used
        return $this->getPasswordResetForm($data, $token, "Set Password");

//        $form = $this->container->get('form.factory')->createNamedBuilder(null, 'form', $data, self::$options)
//            ->setMethod('POST')
//            ->setAction("/api/v1/token/{$token}/confirm")
//            ->add('Submit', 'submit', [
//                'attr' => ['class' => 'form_submit_button btn',]
//            ])
//            ->getForm();
//
//        return $form;
    }

    public function getRegisterForm($data)
    {
        $form = $this->container->get('form.factory')->createNamedBuilder('user', 'form', $data, self::$options)
            ->setMethod('POST')
            ->setAction("/api/v1/register")
            ->add('firstname', 'text')
            ->add('lastname', 'text')
            ->add('email', 'text')
            ->add('nandos_card_number', 'text')
            ->add('Submit', 'submit', [
                'attr' => ['class' => 'form_submit_button btn',]
            ])
            ->getForm();

        return $form;
    }

    public function getLoginForm($data)
    {
        $options = self::$options;

        $options['attr'] = ['class' => 'ajax_form', 'data-closure' => "true"];

        $form = $this->container->get('form.factory')->createNamedBuilder(null, 'form', $data, $options)
            ->setMethod('POST')
            ->setAction("/oauth/v2/token")
            ->add('client_id', 'text')
            ->add('client_secret', 'text')
            ->add('grant_type', 'text')
            ->add('username', 'text')
            ->add('password', 'text')
            ->add('Submit', 'submit', [
                'attr' => ['class' => 'form_submit_button btn',]
            ])
            ->getForm();

        return $form;
    }

    public function getUserinfoForm($data)
    {
        $form = $this->container->get('form.factory')->createNamedBuilder(null, 'form', $data, self::$options)
            ->setMethod('GET')
            ->setAction("/api/v1/user")
            ->add('Submit', 'submit', [
                'attr' => ['class' => 'form_submit_button btn',]
            ])
            ->getForm();

        return $form;
    }
}
