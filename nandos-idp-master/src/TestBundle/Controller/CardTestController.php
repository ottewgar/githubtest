<?php

namespace TestBundle\Controller;


use AppBundle\Api\NcrRadiant\Result\MemberProfile;
use AppBundle\Form\Type\UserRegisterFormType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CardTestController extends Controller
{
    const NCR_EMAIL = "scottp99@gmail.com";
    const PAYTRONIX_EMAIL = "adapticd@gmail.com";

    public function indexAction(Request $request)
    {
        $apiService = $this->container->get('nandos.api.api_service');

        if (!$apiService->getApiTestMode()) {
            return false;
        }

//        $this->wrapper('testUpdatePaytronixPassword');
        $this->wrapper('testPaytronixProfile');
//        $this->wrapper('testPaytronixChangeUsername');

        return new JsonResponse(['message' => "test"]);

    }

    public function wrapper($input)
    {
        die(var_dump($this->$input()));
    }

    public function testPaytronixStatus()
    {
        $userManager = $this->get('nandos.paytronix.user_manager');
        return $userManager->getPaytronixAccountStatus(self::PAYTRONIX_EMAIL);
    }

    public function testPaytronixProfile()
    {
        $userManager = $this->get('nandos.paytronix.user_manager');
        return $userManager->getPaytronixProfile(self::PAYTRONIX_EMAIL);
    }

    public function testPaytronixChangeUsername()
    {
        $userManager = $this->get('nandos.paytronix.user_manager');
        return $userManager->updateUsername("2".self::PAYTRONIX_EMAIL, self::PAYTRONIX_EMAIL);
    }

    public function testUpdatePaytronixPassword()
    {
        $userManager = $this->get('nandos.paytronix.user_manager');
        $response = $userManager->updateProfilePassword(static::PAYTRONIX_EMAIL, '12345678', false);
    }

    public function testPaytronixPasswordMatch()
    {
        $userManager = $this->get('nandos.paytronix.user_manager');
        $response = $userManager->getPaytronixProfile('adapticd@gmail.com');
        $response = $userManager->passwordMatchesProfilePassword('12345', 'restTest');
        $response = $userManager->passwordMatchesProfilePassword($response, '12345678');
    }

    public function testNcrProfile()
    {
        $api = $this->get('nandos.ncr_radiant.api');
        $result = $api->getProfileByEmail(self::NCR_EMAIL);

        dump($result);

        $cardNumber = $result->getCardNumber();
        $result2 = $api->getBonusPlanHistory($cardNumber);

        dump($result2);
    }

    public function testNcrPasswordMatch()
    {
        $email = self::NCR_EMAIL;
        $userManager = $this->get('nandos.ncr_radiant.user_manager');

        $profile = $userManager->getProfileByEmail($email);

        $result = $userManager->passwordMatchesProfilePassword('password123', $profile);
        $result = $userManager->updateProfilePassword($profile, 'password');
    }

    public function testNcrAddUser()
    {
        $userManager = $this->get('nandos.ncr_radiant.user_manager');
        $profile = new MemberProfile();
        $profile->address1 = "sdfasdf";
        $profile->address2 = "kjldsfg";
        $profile->cardNumber = "34234233444822";
        $profile->firstName = "scott";
        $profile->lastName = "pringle";
        $profile->password = 'password';
        $profile->emailAddress = "adapticd@gmail.com";

        $result = $userManager->addMember($profile);
    }

    public function testNcrChangePassword()
    {
        $userManager = $this->get('nandos.ncr_radiant.user_manager');

        $userManager->updateProfilePassword(self::NCR_EMAIL, 'password123A');
    }
}
