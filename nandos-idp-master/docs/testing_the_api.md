Testing the API
==============

As several API calls e.g. register, require that an action be confirmed by a code received by email, this makes API testing more challenging.
 
We generally cannot return the confirmation code in the API response, otherwise malicious hackers (who have decompiled the mobile app and extracted the client id and secret) can get the code and immediately activate accounts.
  
To assist with API testing, the non-production test sites allow a query parameter to be added to certain API calls.

This has been enabled on the following API calls:

  * [POST] /api/v1/register?api_test_key=[API_TEST_KEY]

For additional security (even though this setting will not be applied to the production server), the api_test_key is not stored in the repo and is available upon request. 

THis can also be provided in the API sandbox at `/api/doc` under the **Filters** section within the **Sandbox** tab