Getting an access token for a user
=================================

POST to: /oauth/v2/token

in body format: x-www-form-urlencoded 

with:

    'grant_type' => 'password',
    'username' => '[users email address]',
    'password' => '[users password]',
    'client_id' => '[client_id]_[client_random_id]', // this forms the client id
    'client_secret' => '[client secret]'

NOTE: The client is not a "normal" user.
It is either the Nando's website, or Nando's mobile app, or another 3rd party allowed to use the IDP

We control the client records in the admin. If a client is deleted, login will fail, so they must be treated very carefully.

Due to the nature of mobile apps, we cannot guarantee the privacy of the client_id and secret, so all API calls related to users must rely on the user access token, which is only available after a user has provided their username/password credentials.

API calls without a logged in user e.g. /api/register should be rate limited from IP addresses other than the website addresses, as a malicious user can extract the client_id and secret from the app and spoof that they are the mobile app.

See: https://stormpath.com/blog/the-ultimate-guide-to-mobile-api-security/
  

Setting up a test frontend site
-------------------------------
See: https://gist.github.com/lologhi/7b6e475a2c03df48bcdd


Accessing the API with an auth_token
------------------------------------

Add the header to all /api requests requiring a logged in user:

Authorization: Bearer [access_token]
