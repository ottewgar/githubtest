#!/usr/bin/env bash

SHORE_ENVIRONMENT=$1

echo "Provisioning Nando's IDP under shore environment: $SHORE_ENVIRONMENT"

# If required do non-interactive composer install when SHORE_ENVIRONMENT = staging

cd /shore_site && composer install --ignore-platform-reqs

