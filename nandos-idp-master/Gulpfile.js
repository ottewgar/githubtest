var gulp = require('gulp');
var sass = require('gulp-sass') ;
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');
var gp_concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');

var resourcesPath = 'src/AppBundle/Resources/';

gulp.task('sass', function() {
    gulp.src(resourcesPath+'public/scss/core.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCSS())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('./web/frontend/css'));
});

  gulp.task('frontend', ['sass']);
  gulp.task('default', ['frontend']);
